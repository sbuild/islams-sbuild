#!/bin/sh

gcat=/mingw/etc/xml/catalog
tcat=/mingw/etc/xml/docbook-xml.xml

if test -f ${tcat}
then
  xmlcatalog --noout --del "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" ${tcat}
  xmlcatalog --noout --del "http://docbook.org/xml/4.4/docbookx.dtd" ${tcat}
  xmlcatalog --noout --del "-//OASIS//ELEMENTS DocBook Information Pool V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//ENTITIES DocBook Additional General Entities V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//ENTITIES DocBook Notations V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//ENTITIES DocBook Character Entities V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//ELEMENTS DocBook XML HTML Tables V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//DTD DocBook XML V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//ELEMENTS DocBook Document Hierarchy V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//DTD DocBook CALS Table Model V4.4//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//DTD XML Exchange Table Model 19990315//EN" ${tcat}
  if test $(cat ${tcat} | wc -l) -eq $(xmlcatalog --create | wc -l)
  then
    rm ${tcat}
  fi
fi

en=$(echo -en "\n\b")

pids=\
"-//OASIS//ELEMENTS DocBook"$en\
"-//OASIS//DTD DocBook XML"$en\
"-//OASIS//DTD XML Exchange Table Model 19990315"$en\
"-//OASIS//ENTITIES DocBook"$en\
"-//OASIS//DTD DocBook CALS Table Model"

uris=\
"http://docbook.org/xml/"$en\
"http://www.oasis-open.org/docbook/xml/"

if test -f ${gcat}
then
  normal_IFS=$IFS
  IFS=$en
  for pid in $pids $uris
  do
    IFS=$normal_IFS
    remove_catalog=0
    if test ! -f ${tcat}
    then
      remove_catalog=1
    else
      if test $(cat ${tcat} | grep --regexp=\""${pid}.*"\" | wc -l) == 0
      then
        remove_catalog=1
      fi
    fi
    if test $remove_catalog == 1
    then
      echo "Removing reference \"$pid\" to catalog ${tcat}"
      xmlcatalog --noout --del "$pid" ${gcat}
    fi
    IFS=$en
  done
  IFS=$normal_IFS
  if test $(cat ${gcat} | wc -l) -eq $(xmlcatalog --create | wc -l)
  then
    rm ${gcat}
  fi
fi

