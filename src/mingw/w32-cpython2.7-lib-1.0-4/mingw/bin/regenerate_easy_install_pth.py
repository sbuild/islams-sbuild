#!/mingw/bin/python
# Usage: regenerate_easy_install_pth.py <dir where easy-install.pth is> [dirs to look for egg subdirs in (defaults to easy-install.pth dir)]
# For example: /mingw/bin/regenerate_easy_install_pth.py /mingw/lib/python2.7/site-packages

from __future__ import print_function

import os
import sys

tmpl_h="""import sys; sys.__plen = len(sys.path)"""
tmpl_f="""import sys; new=sys.path[sys.__plen:]; del sys.path[sys.__plen:]; p=getattr(sys,'__egginsert',0); sys.path[p:p]=new; sys.__egginsert = p+len(new)"""

l = []
locs = sys.argv[2:]
if len (locs) == 0:
  locs = [sys.argv[1]]
for a in locs:
  for d in os.listdir (a):
    if d[-4:] == ".egg":
      ei = os.path.join (a, d, "EGG-INFO")
      if os.path.exists (ei):
        l.append ('./' + os.path.relpath (os.path.join (a, d), sys.argv[1]).replace ('\\', '/'))

with open (os.path.join (sys.argv[1], 'easy-install.pth'), 'wb') as w:
  print (tmpl_h, file=w)
  for i in l:
    print (i, file=w)
  print (tmpl_f, file=w)
