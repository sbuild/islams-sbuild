#!/bin/sh
prefix=/mingw
pyprefix=$(/mingw/bin/python -c "import sys;print sys.prefix.replace('\\\\','/')")
pyver2dot=$(/mingw/bin/python -c "import sys;print str(sys.version_info[0]) + '.' + str(sys.version_info[1])")
pyver2=$(/mingw/bin/python -c "import sys;print str(sys.version_info[0]) + str(sys.version_info[1])")
sysroot="$(echo $SYSTEMROOT | sed -e 's!\\!/!')"
system32="${sysroot}/System32"
syswow64="${sysroot}/SysWOW64"
pyarch=$(/mingw/bin/python -c "import platform;print platform.architecture()[0]")
if test "x${pyarch}" == "x32bit" -a -d ${syswow64}
then
  system32="${syswow64}"
fi

if test ! -e "/mingw/lib/libpython${pyver2dot}.dll.a"
then
   echo "pexports '${system32}/python${pyver2}.dll' > '${pyprefix}/libs/libpython${pyver2}.def'"
   pexports "${system32}/python${pyver2}.dll" > "${pyprefix}/libs/libpython${pyver2}.def"
   echo dlltool "${system32}/python${pyver2}.dll" -l "${prefix}/lib/libpython${pyver2dot}.dll.a" -d "${pyprefix}/libs/libpython${pyver2}.def"
   dlltool "${system32}/python${pyver2}.dll" -l "${prefix}/lib/libpython${pyver2dot}.dll.a" -d "${pyprefix}/libs/libpython${pyver2}.def"
fi

if test ! -e "/mingw/lib/libpython${pyver2}.dll.a"
then
   ln -s "libpython${pyver2dot}.dll.a" "${prefix}/lib/libpython${pyver2}.dll.a"
fi

wget --no-check-certificate https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - | /mingw/bin/python
