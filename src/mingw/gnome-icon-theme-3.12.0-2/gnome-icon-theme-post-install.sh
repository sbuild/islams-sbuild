#!/bin/sh

pushd .
  cd /mingw/share/icons/gnome
  for size in 8x8 16x16 22x22 24x24 32x32 48x48 256x256
  do
    cd $size || continue
    echo -e "Going to set up icon mapping for $size"
    for dir in `find . -type d`
    do
      context="`echo $dir | cut -c 3-`"
      if [ $context ]
      then
        `pkg-config --variable=program_path icon-naming-utils`/icon-name-mapping -c $context
      fi
    done
    cd ..
  done
popd
