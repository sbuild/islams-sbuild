#!/bin/sh
# Append msys .pc directories to the path.
# This helps if mingw packages are checking for msys tools via pkg-config
# Obviously, this will bite you if some mingw prerequisites are missing
if test x"$MSYSTEM" == "xMSYS"
then
  export PKG_CONFIG_PATH=${PKG_CONFIG_PATH+$PKG_CONFIG_PATH:}/usr/lib/pkgconfig:/usr/share/pkgconfig
else
  export PKG_CONFIG_PATH=${PKG_CONFIG_PATH+$PKG_CONFIG_PATH:}/mingw/lib/pkgconfig:/mingw/share/pkgconfig:/usr/lib/pkgconfig:/usr/share/pkgconfig
fi
