#!/bin/sh
mkdir -p /mingw/etc/xml

if ! test -f /mingw/etc/xml/catalog
then
  xmlcatalog --noout --create /mingw/etc/xml/catalog
fi
xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/5" "./docbook5-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/5" "./docbook5-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML 5" "./docbook5-xml.xml" /mingw/etc/xml/catalog

if ! test -f /mingw/etc/xml/docbook5-xml.xml
then
  xmlcatalog --noout --create /mingw/etc/xml/docbook5-xml.xml
fi
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML 5//EN" "../../share/xml/docbook/schema/catalog-docbook5.xml" /mingw/etc/xml/docbook5-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML 5.0//EN" "../../share/xml/docbook/schema/catalog-docbook5.xml" /mingw/etc/xml/docbook5-xml.xml
xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/5.0/" "../../share/xml/docbook/schema/catalog-docbook5.xml" /mingw/etc/xml/docbook5-xml.xml
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/5.0/" "../../share/xml/docbook/schema/catalog-docbook5.xml" /mingw/etc/xml/docbook5-xml.xml
