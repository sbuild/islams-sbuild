#!/bin/sh
CONF_LINKS="\
	10-scale-bitmap-fonts.conf \
	10-sub-pixel-rgb.conf \
	11-lcdfilter-default.conf \
	20-unhint-small-vera.conf \
	30-urw-aliases.conf \
	30-metric-aliases.conf \
	40-nonlatin.conf \
	45-latin.conf \
	49-sansserif.conf \
	50-user.conf \
	51-local.conf \
	60-latin.conf \
	65-fonts-persian.conf \
	65-nonlatin.conf \
	69-unifont.conf \
	70-no-bitmaps.conf \
	80-delicious.conf \
	90-synthetic.conf"

cd /mingw/etc/fonts/conf.d
for i in ${CONF_LINKS}; do
  cmd //C del //F //Q $i
done
