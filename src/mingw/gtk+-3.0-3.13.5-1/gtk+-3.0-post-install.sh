#!/bin/sh
mkdir -p /mingw/lib/gtk-3.0/3.0.0
/mingw/bin/gtk-query-immodules-3.0.exe >/mingw/lib/gtk-3.0/3.0.0/immodules.cache
/mingw/bin/gtk-update-icon-cache-3.0 -f -t /mingw/share/icons/hicolor
/mingw/bin/glib-compile-schemas.exe /mingw/share/glib-2.0/schemas

settingsfile=/mingw/etc/gtk-3.0/settings.ini
if test ! -f $settingsfile
then
  echo [Settings]>>$settingsfile
  echo gtk-xft-antialias=1>>$settingsfile
  echo gtk-xft-hinting=1>>$settingsfile
  echo gtk-xft-hintstyle=hintfull>>$settingsfile
  echo gtk-xft-rgba=rgb>>$settingsfile
  echo gtk-primary-button-warps-slider=false>>$settingsfile
  echo gtk-dialogs-use-header=true>>$settingsfile
fi
