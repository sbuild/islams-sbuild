#!/bin/sh
mkdir -p /mingw/etc/xml

if ! test -f /mingw/etc/xml/catalog
then
  xmlcatalog --noout --create /mingw/etc/xml/catalog
fi
xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/" "./docbook-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/" "./docbook-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook" "./docbook-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML" "./docbook-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD XML Exchange Table Model 19990315" "./docbook-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook" "./docbook-xml.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook CALS Table Model" "./docbook-xml.xml" /mingw/etc/xml/catalog

if ! test -f /mingw/etc/xml/docbook-xml.xml
then
  xmlcatalog --noout --create /mingw/etc/xml/docbook-xml.xml
fi
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/4.5/docbookx.dtd" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook Information Pool V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook Additional General Entities V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook Notations V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook Character Entities V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook XML HTML Tables V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook Document Hierarchy V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook CALS Table Model V4.5//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD XML Exchange Table Model 19990315//EN" "../../share/xml/docbook/schema/dtd/4.5/catalog.xml" /mingw/etc/xml/docbook-xml.xml
