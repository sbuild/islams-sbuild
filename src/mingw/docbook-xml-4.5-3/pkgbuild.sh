#! /bin/bash

name=docbook-xml
badname=docbook-xml
ver=4.5
ver1=4
ver2=4.5
dllver=0
rev=3
subsys=mingw
srcname=${name}-${ver}
prefix=/mingw
CFLAGS="-g -O2"
docdir=share/doc/${badname}/${ver}
SOURCE_ARCHIVE_FORMAT=.zip
SOURCE_ARCHIVE=${name}-${ver}${SOURCE_ARCHIVE_FORMAT}
preconf_patches=
docfiles=""
licfiles=""

if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=/etc --localstatedir=/var"; fi

url=http://www.oasis-open.org/docbook/xml/${ver2}/${SOURCE_ARCHIVE}

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw"
then
  echo "You must be in an MINGW shell to build a mingw package"
  exit 5
fi

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/bld
logdir=${pkgbuilddir}/logs
#repodir=${pkgbuilddir}/repo
srcdir=${pkgbuilddir}/${name}-${ver}

echo The source directory is ${srcdir}

srcdirname=$(basename ${srcdir})

capname=Unknown
if test "x${subsys}" == "xmingw"
then
capname=MinGW
elif test "x${subsys}" == "xmsys"
then
capname=MSYS
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}


if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi

p1=${prefix:1}

do_clone=0
do_download=1
do_unpack=1
do_copy=0
do_patch=1
do_preconfigure=0
do_reconfigure=0
do_configure=0
do_make=0
do_check=0
do_install=1
do_fixinstall=1
do_pack=1
do_clean=1

if test ! "x$repodir" = "x"
then
  do_clone=1
  do_download=0
  do_unpack=0
  do_copy=1
  do_reconfigure=1
fi

zeroall() {
      do_clone=0
      do_download=0
      do_unpack=0
      do_copy=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --clone) do_clone=1 ; shift 1 ;;
    --copy) do_copy=1 ; shift 1 ;;
    --download) do_download=1 ; shift 1 ;;
    --unpack) do_unpack=1 ; shift 1 ;;
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --clone-only) zeroall; do_clone=1 ; shift 1 ;;
    --copy-only) zeroall; do_copy=1 ; shift 1 ;;
    --download-only) zeroall; do_download=1 ; shift 1 ;;
    --unpack-only) zeroall; do_unpack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_clone" == "x1"
then
  if test ! -d "${repodir}"; then
    mkdir -p ${repodir} # Don't fail if exists
    git clone ${repo_url} ${repodir} || fail $LINENO
  else
    pushd .
    cd ${repodir}
    git pull  || fail $LINENO
    popd
  fi
fi

if test "x$do_download" == "x1"
then
  rm -f ${logdir}/download.log
  test -f ${pkgbuilddir}/${SOURCE_ARCHIVE} || {
    echo "Downloading ${SOURCE_ARCHIVE} ..." 2>&1 | tee ${logdir}/download.log
    wget -P ${pkgbuilddir} $url 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    b=$(basename $url)
    if test ! "x$b" = "x${SOURCE_ARCHIVE}"
    then
      mv -f $b ${SOURCE_ARCHIVE}
    fi
  }

  if test -f ${SOURCE_ARCHIVE}.sha512
  then
    echo "Verifying sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum -c ${SOURCE_ARCHIVE}.sha512 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  else
    echo "Calculating sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum ${SOURCE_ARCHIVE} >${SOURCE_ARCHIVE}.sha512 2>>${logdir}/download.log || fail $LINENO
  fi
  echo "Done downloading"
fi

if test "x$do_unpack" == "x1"
then
  rm -f ${logdir}/unpack.log
  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/unpack.log
    rm -rf ${srcdir}/* 2>&1 | tee -a ${logdir}/unpack.log
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee ${logdir}/unpack.log
    exit 2
  fi

  echo Cleaning up inst and build directories | tee -a ${logdir}/unpack.log
  rm -rf ${blddir} ${instdir} 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  mkdir -p ${srcdir}
  cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Unpacking $SOURCE_ARCHIVE in `pwd`" | tee -a ${logdir}/unpack.log
  case "$SOURCE_ARCHIVE" in
  *.tar.bz2 ) tar xjf ${pkgbuilddir}/$SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.gz  ) tar xzf ${pkgbuilddir}/$SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.zip     ) miniunzip -x ${pkgbuilddir}/$SOURCE_ARCHIVE  2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.lzma) tar --lzma -xf ${pkgbuilddir}/$SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.xz) tar xJf ${pkgbuilddir}/$SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  esac
  if test $(ls ${srcdir} -l | wc -l) -ge 2
  then
    echo "Unpacked into ${srcdir} successfully!" | tee -a ${logdir}/unpack.log
  else
    echo "${srcdir} is empty - did not unpack into it?" | tee -a ${logdir}/unpack.log
    exit 3
  fi
  echo "Done unpacking"
fi

if test "x$do_copy" == "x1"
then
  rm -f ${logdir}/copy.log

  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/copy.log
    rm -rf ${srcdir}/* 2>&1 | tee -a ${logdir}/copy.log
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee ${logdir}/copy.log
    exit 2
  fi
  
  echo Cleaning up src, inst and build directories | tee -a ${logdir}/copy.log
  rm -rf ${blddir} ${instdir} ${srcdir} 2>&1 | tee -a ${logdir}/copy.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Copying from $repodir to $srcdir" | tee -a ${logdir}/copy.log
  cp -rf $repodir $srcdir | tee -a ${logdir}/copy.log
  rm -rf $srcdir/.git | tee -a ${logdir}/copy.log
  pushd .
  echo cd ${srcdir} && cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  tar cJv --hard-dereference -f ${pkgbuilddir}/${SOURCE_ARCHIVE} * 2>&1 | tee -a ${logdir}/copy.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  popd

  echo "Done copying"
fi

patch_list=
if test "x$do_patch" == "x1"
then
  rm -f ${logdir}/patch.log
  echo cd ${srcdir}
  cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test ! "x${patchfiles}" == "x"
  then
    echo "Patching in `pwd` from patchfiles ${patchfiles}" | tee -a ${logdir}/patch.log
  fi
  for patchfile in ${patchfiles}
  do
    if test ! "x${patchfiles}" == "x"
    then
      echo "Applying ${patchfile}" | tee -a ${logdir}/patch.log
      patch -p1 -i ${patchfile} 2>&1 | tee -a ${logdir}/patch.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    patch_list="$patch_list $patchfile"
  done
  echo "Done patching"
fi

mkdir -p ${blddir} || fail $LINENO

if test "x$do_reconfigure" == "x1"
then
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  #cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Reconfiguring in `pwd` | tee -a ${logdir}/reconfigure.log
  touch ${srcdir}/config.rpath
  mkdir ${srcdir}/doc/html/api
  touch ${srcdir}/doc/html/api/dummy
  ${srcdir}/autogen.sh --prefix=${prefix} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
elif test "x$do_preconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Patching in `pwd` from ${preconf_patches} | tee -a ${logdir}/reconfigure.log
  for patchfile in ${pkgbuilddir}/preconf-patches/*${subsys}.patch
  do
    patch -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  done
fi

if test "x$do_configure" == "x1"
then
  :
  #rm -f ${logdir}/configure.log
  #cd ${blddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  #
  #lndir ${srcdir} . 2>&1 | tee -a ${logdir}/configure.log
  #echo Configuring in `pwd` | tee -a ${logdir}/configure.log
  #../$(basename ${srcdir})/configure --prefix=${prefix} 2>&1 | tee -a ${logdir}/configure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_make" == "x1"
then
  rm -f ${logdir}/make.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Making
  make $MAKEFLAGS 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

install=/bin/install

if test "x$do_install" == "x1"
then
  rm -f ${logdir}/install.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Installing into ${instdir} from `pwd` | tee ${logdir}/install.log
  mkdir -p ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5
  mkdir -p ${instdir}${prefix}/etc/sgml/docbook-xml/4.5

  for f in ${srcdir}/*.dtd ${srcdir}/*.mod
  do
    if test "x$(basename ${f})" == "xdbgenent.mod";
    then
      ${install}  -t ${instdir}${prefix}/etc/sgml/docbook-xml/4.5/ ${srcdir}/$(basename ${f})
    fi
    ${install}  -t ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5 ${srcdir}/$(basename ${f})
  done
  #TODO: create ${instdir}${prefix}/share/xml/entities/xml-iso-entities-8879.1986 as a copy of ent, modify the dbcentx.mod file to point to it
  ${install} -d ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5/
  ${install} -t ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5/ ${srcdir}/ent/*
  ${install} ${srcdir}/catalog.xml ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5/catalog.xml
  ${install} ${srcdir}/docbook.cat ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5/catalog
  ${install} ${srcdir}/docbook.cat ${instdir}${prefix}/share/xml/docbook/schema/dtd/4.5/docbook.cat

  mkdir -p ${instdir}${prefix}/var/lib/mingw-get/scripts
  ${install} ${pkgbuilddir}/${badname}-post-install.sh ${instdir}${prefix}/var/lib/mingw-get/scripts/${badname}-${ver}-${rev}-post-install.sh
  ${install} ${pkgbuilddir}/${badname}-post-remove.sh ${instdir}${prefix}/var/lib/mingw-get/scripts/${badname}-${ver}-${rev}-post-remove.sh

  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver}
  ${install}  ${srcdir}/ChangeLog ${instdir}${prefix}/share/doc/${badname}/${ver}/
  #make install DESTDIR=${instdir} $MAKEFLAGS 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${pkgbuilddir}
  if test ! "x${scriptfiles}" == "x"
  then
    mkdir -p ${instdir}${prefix}/var/lib/mingw-get/scripts
  fi
  for scriptfile in ${scriptfiles}
  do
    if test ! "x${scriptfile}" == "x"
    then
      cp ${scriptfile} ${instdir}${prefix}/var/lib/mingw-get/scripts/$(echo ${scriptfile} | sed -e 's/'${badname}'-/'${badname}'-'${ver}'-'${rev}'-/')
    fi
  done
fi

if test "x$do_check" == "x1"
then
  rm -f ${logdir}/test.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Testing" | tee -a ${logdir}/test.log
  #PATH=${instdir}/${prefix}/bin:$PATH
  make -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi


if test "x$do_fixinstall" == "x1"
then
  rm -f ${logdir}/fixinstall.log
  echo Fixing the installation | tee ${logdir}/fixinstall.log
  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  for f in ${docfiles} ${licfiles}
  do
    if test -f ${instdir}${prefix}/${docdir}/$(basename ${f})
    then
      ${install} ${f} ${instdir}${prefix}/${docdir}/${f}
    else
      ${install} ${f} ${instdir}${prefix}/${docdir}/$(basename ${f})
    fi
  done
  mkdir -p ${instdir}${prefix}/share/doc/${capname} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  ${install} ${pkgbuilddir}/${subsys}-${badname}.RELEASE_NOTES \
    ${instdir}${prefix}/share/doc/${capname}/${badname}-${ver}-${rev}-${subsys}.RELEASE_NOTES.txt 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  cd ${instdir}${prefix}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  echo Packing | tee -a ${logdir}/pack.log
  tar cv --lzma --hard-dereference -f ${pkgbuilddir}/${DEVPKG} ${DEV_CONTENTS} 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  tar cv --lzma --hard-dereference -f ${pkgbuilddir}/${DOCPKG} ${DOC_CONTENTS} 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  tar cv --lzma --hard-dereference -f ${pkgbuilddir}/${BLDPKG} ${BLD_CONTENTS} 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  tar cv --lzma --hard-dereference -f ${pkgbuilddir}/${SRCPKG} ${SRC_CONTENTS} 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

echo "Done"
exit 0

pkgtmp=${badname}-${ver}-${rev}-${subsys}
BINPKG=${pkgtmp}-bin.tar.lzma
DEVPKG=${pkgtmp}-dev.tar.lzma
DLLPKG=lib${badname}-${ver}-${subsys}-dll-${dllver}.tar.lzma
DOCPKG=${pkgtmp}-doc.tar.lzma
LICPKG=${pkgtmp}-lic.tar.lzma
COMPKG=${badname}-${ver}-${rev}-msys-${SYSVER}-completion.tar.lzma
LANGPKG=${pkgtmp}-lang.tar.lzma
BLDPKG=${pkgtmp}-bld.tar.lzma
SRCPKG=${pkgtmp}-src.tar.lzma
BIN_CONTENTS="--exclude=bin/*.dll bin"
DEV_CONTENTS="--exclude=*.dll --exclude=*.la share/xml etc/sgml var"
DLL_CONTENTS="bin/*.dll"
LIC_CONTENTS="$(for i in $licfiles; do echo $docdir/$i; done)"
DOC_CONTENTS="share/doc/${badname}/${ver}"
LIC_CONTENTS="$LIC_CONTENTS $(for i in $docfiles; do echo --exclude=$i; done)"
DOC_CONTENTS="$DOC_CONTENTS $(for i in $licfiles; do echo --exclude=$i; done) share/doc/${capname}"
COM_CONTENTS="etc/bash_completion.d"
LANG_CONTENTS="share/locale"
BLD_CONTENTS="pkgbuild.sh \
${subsys}-${badname}.RELEASE_NOTES \
patches \
${badname}-post-install.sh \
${badname}-post-remove.sh \
${SOURCE_ARCHIVE}.md5"
SRC_CONTENTS="${BLD_CONTENTS} \
${SOURCE_ARCHIVE}"
