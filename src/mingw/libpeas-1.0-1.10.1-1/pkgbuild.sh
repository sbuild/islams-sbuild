#! /bin/bash

name=libpeas
badname=libpeas-1.0
ver=1.10.1
ver1=1
ver2=1.10
dllver=0
rev=1
subsys=mingw
srcname=${name}-${ver}
prefix=/mingw
CFLAGS="-g -O2"
docdir=share/doc/${badname}/${ver}
SOURCE_ARCHIVE_FORMAT=.tar.xz
SOURCE_ARCHIVE=${name}-${ver}${SOURCE_ARCHIVE_FORMAT}
preconf_patches=
docfiles="README \
NEWS"
licfiles="AUTHORS \
COPYING \
COPYING.lib \
MAINTAINERS"

if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=\${prefix}/etc --localstatedir=/var"; fi

reposrc=""
url=https://download.gnome.org/sources/libpeas/${ver2}/${SOURCE_ARCHIVE}

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw"
then
  echo "You must be in an MINGW shell to build a mingw package"
  exit 5
fi

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/bld
logdir=${pkgbuilddir}/logs
srcdir=${pkgbuilddir}/${name}-${ver}

echo The source directory is ${srcdir}

srcdirname=$(basename ${srcdir})

capname=Unknown
if test "x${subsys}" == "xmingw"
then
capname=MinGW
triplet=$(gcc -dumpmachine)
elif test "x${subsys}" == "xmsys"
then
capname=MSYS
triplet=$(gcc -dumpmachine)
elif test "x${subsys}" == "xmingw64"
then
capname=MinGW-W64
triplet=$(gcc -dumpmachine)
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}


if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi

p1=${prefix:1}

do_download=1
do_unpack=1
do_patch=1
do_preconfigure=0
do_reconfigure=1
do_configure=1
do_make=1
do_check=0
do_install=1
do_fixinstall=1
do_pack=1
do_clean=1

if test ! "x$reposrc" = "x"
then
  srcdir="$reposrc"
  do_download=0
  do_unpack=0
  do_reconfigure=1
fi

zeroall() {
      do_download=0
      do_unpack=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --download) do_download=1 ; shift 1 ;;
    --unpack) do_unpack=1 ; shift 1 ;;
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --download-only) zeroall; do_download=1 ; shift 1 ;;
    --unpack-only) zeroall; do_unpack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_download" == "x1"
then
  rm -f ${logdir}/download.log
  test -f ${pkgbuilddir}/${SOURCE_ARCHIVE} || {
    echo "Downloading ${SOURCE_ARCHIVE} ..." 2>&1 | tee ${logdir}/download.log
    wget -P ${pkgbuilddir} $url 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    b=$(basename $url)
    if test ! "x$b" = "x${SOURCE_ARCHIVE}"
    then
      mv -f $b ${SOURCE_ARCHIVE}
    fi
  }

  if test -f ${SOURCE_ARCHIVE}.sha512
  then
    echo "Verifying sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum -c ${SOURCE_ARCHIVE}.sha512 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  else
    echo "Calculating sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum ${SOURCE_ARCHIVE} >${SOURCE_ARCHIVE}.sha512 2>>${logdir}/download.log || fail $LINENO
  fi
  echo "Done downloading"
fi

if test "x$do_unpack" == "x1"
then
  rm -f ${logdir}/unpack.log
  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/unpack.log
    rm -rf ${srcdir}/* 2>&1 | tee -a ${logdir}/unpack.log
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee ${logdir}/unpack.log
    exit 2
  fi

  echo Cleaning up inst and build directories | tee -a ${logdir}/unpack.log
  rm -rf ${blddir} ${instdir} 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Unpacking $SOURCE_ARCHIVE in `pwd`" | tee -a ${logdir}/unpack.log
  case "$SOURCE_ARCHIVE" in
  *.tar.bz2 ) tar xjf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.gz  ) tar xzf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.zip     ) unzip -q $SOURCE_ARCHIVE  2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.lzma) tar --lzma -xf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.xz) tar xJf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  esac
  if test $(ls ${srcdir} -l | wc -l) -ge 2
  then
    echo "Unpacked into ${srcdir} successfully!" | tee -a ${logdir}/unpack.log
  else
    echo "${srcdir} is empty - did not unpack into it?" | tee -a ${logdir}/unpack.log
    exit 3
  fi
  echo "Done unpacking"
fi

patch_list=
if test "x$do_patch" == "x1"
then
  rm -f ${logdir}/patch.log
  echo cd ${srcdir}
  cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test ! "x${patchfiles}" == "x"
  then
    echo "Patching in `pwd` from patchfiles ${patchfiles}" | tee -a ${logdir}/patch.log
  fi
  for patchfile in ${patchfiles}
  do
    if test ! "x${patchfiles}" == "x"
    then
      echo "Applying ${patchfile}" | tee -a ${logdir}/patch.log
      patch -p1 -i ${patchfile} 2>&1 | tee -a ${logdir}/patch.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    patch_list="$patch_list $patchfile"
  done
  echo "Done patching"
fi

if test "x$do_reconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Reconfiguring in `pwd` | tee -a ${logdir}/reconfigure.log
  intltoolize -f 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  autoreconf -i -f 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
elif test "x$do_preconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Patching in `pwd` from ${preconf_patches} | tee -a ${logdir}/reconfigure.log
  for patchfile in ${pkgbuilddir}/preconf-patches/*${subsys}.patch
  do
    patch -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  done
fi

mkdir -p ${blddir} || fail $LINENO

if test "x$do_configure" == "x1"
then
  rm -f ${logdir}/configure.log
  cd ${blddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  build_host_target=
  if test ! "x${buildalias}" == "x"
  then
    build_host_target+=" --build=${buildalias}"
  fi
  if test ! "x${hostalias}" == "x"
  then
    build_host_target+=" --host=${hostalias}"
  fi
  if test ! "x${targetalias}" == "x"
  then
    build_host_target+=" --target=${targetalias}"
  fi

  #lndir ${srcdir} . 2>&1 | tee -a ${logdir}/configure.log
  echo Configuring in `pwd` | tee -a ${logdir}/configure.log
  ../$(basename ${srcdir})/configure \
    --prefix=${prefix} \
    --enable-silent-rules \
    --enable-gtk-doc \
    --enable-glade-catalog \
    ${install_dirs} \
    ${build_host_target} \
    STRIP=true CFLAGS="$CFLAGS" CPPFLAGS="-DWINVER=0x500" \
    2>&1 | tee -a ${logdir}/configure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_make" == "x1"
then
  rm -f ${logdir}/make.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Making
  rm -f ${srcdir}/docs/reference/gtk/gtk3.types
  make $MAKEFLAGS 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_install" == "x1"
then
  rm -f ${logdir}/install.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Installing into ${instdir} from `pwd` | tee ${logdir}/install.log
  make install DESTDIR=${instdir} $MAKEFLAGS 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${pkgbuilddir}
  if test ! "x${scriptfiles}" == "x"
  then
    mkdir -p ${instdir}${prefix}/var/lib/mingw-get/scripts
  fi
  for scriptfile in ${scriptfiles}
  do
    if test ! "x${scriptfile}" == "x"
    then
      sed -e 's/@triplet@/'${triplet}'/g' ${scriptfile} > ${instdir}${prefix}/var/lib/mingw-get/scripts/$(echo ${scriptfile} | sed -e 's/'${badname}'-/'${badname}'-'${ver}'-'${rev}'-/')
    fi
  done
fi

if test "x$do_check" == "x1"
then
  rm -f ${logdir}/test.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Testing" | tee -a ${logdir}/test.log
  PATH=${instdir}/${prefix}/bin:$PATH make -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi


if test "x$do_fixinstall" == "x1"
then
  rm -f ${logdir}/fixinstall.log
  echo Fixing the installation | tee ${logdir}/fixinstall.log
  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  for f in ${docfiles} ${licfiles}
  do
    if test -f ${instdir}${prefix}/${docdir}/$(basename ${f})
    then
      cp -r ${f} ${instdir}${prefix}/${docdir}/${f}
    else
      cp -r ${f} ${instdir}${prefix}/${docdir}/$(basename ${f})
    fi
  done
  mkdir -p ${instdir}${prefix}/share/doc/${capname} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cp ${pkgbuilddir}/${subsys}-${badname}.RELEASE_NOTES \
    ${instdir}${prefix}/share/doc/${capname}/${badname}-${ver}-${rev}-${subsys}.RELEASE_NOTES.txt 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  /mingw/bin/laremove.py $(cd ${instdir}; pwd -W) && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  /mingw/bin/split-debug.py $(cd ${instdir}; pwd -W) --target=$buildalias && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  echo Packing | tee -a ${logdir}/pack.log
  cd ${instdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  pkgtmp=${ver}-${rev}-${subsys}_${arch}

  tar cv --xz -f ${pkgbuilddir}/gir1.2-peas-1.0-${pkgtmp}-bin.tar.xz \
$p1/lib/girepository-1.0 \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libpeas-1.0-0-${pkgtmp}-dll.tar.xz \
$p1/bin/libpeas-1.0-0.dll \
$p1/bin/libpeas-gtk-1.0-0.dll \
$p1/lib/libpeas-1.0/loaders/libpythonloader.dll \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libpeas-1.0-0-${pkgtmp}-dbg.tar.xz \
$p1/bin/libpeas-1.0-0.dll.dbg \
$p1/bin/libpeas-gtk-1.0-0.dll.dbg \
$p1/lib/libpeas-1.0/loaders/libpythonloader.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libpeas-common-${pkgtmp}-bin.tar.xz \
$p1/share/icons \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libpeas-${pkgtmp}-lang.tar.xz \
$p1/share/locale \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libpeas-${pkgtmp}-dev.tar.xz \
$p1/include \
$p1/lib/*.a \
$p1/lib/pkgconfig \
$p1/share/gir-1.0 \
$p1/share/glade \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-doc.tar.xz \
$p1/bin/peas-demo.exe \
$p1/bin/peas-demo.exe.dbg \
$p1/lib/peas-demo \
$p1/share/gtk-doc \
$p1/${docdir} \
$p1/share/doc/${capname} \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  BLD="\
pkgbuild.sh \
${subsys}-${badname}.RELEASE_NOTES \
$(if test -d patches; then echo patches; fi) \
${scriptfiles} \
${SOURCE_ARCHIVE}.sha512 \
"

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bld.tar.xz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-src.tar.xz ${SOURCE_ARCHIVE} $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_clean" == "x1"
then

cd ${pkgbuilddir}
  rm -rf ${blddir} ${instdir}
  if test -d "${srcdir}" -a ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    rm -rf ${srcdir}
  fi
fi

echo "Done"
exit 0
