#! /bin/sh

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

name=gettext
badname=gettext
ver=0.19.2
ver1=0
ver2=0.19
rev=1
subsys=mingw
srcname=${name}-${ver}
prefix=/mingw
CFLAGS="-g -O2"
docdir=share/doc/${badname}/${ver}
SOURCE_ARCHIVE_FORMAT=.tar.xz
SOURCE_ARCHIVE="${name}-${ver}${SOURCE_ARCHIVE_FORMAT}"
preconf_patches=
docfiles="README \
README.win32 \
ChangeLog \
NEWS"
licfiles="AUTHORS \
COPYING"

reposrc=""

# Set to non-empty value to link source directory into the build directory
srctobuild=

url=http://ftpmirror.gnu.org/${name}/${SOURCE_ARCHIVE}

if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=/etc --localstatedir=/var"; fi

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw"
then
  echo "You must be in an MINGW shell to build a mingw package"
  exit 5
fi

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/bld
logdir=${pkgbuilddir}/logs
srcdir=${pkgbuilddir}/${name}-${ver}

echo The source directory is ${srcdir}

srcdirname=$(basename ${srcdir})

capname=Unknown
if test "x${subsys}" == "xmingw"
then
capname=MinGW
elif test "x${subsys}" == "xmsys"
then
capname=MSYS
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}

if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi

p1=${prefix:1}

do_download=1
do_unpack=1
do_patch=1
do_preconfigure=0
do_reconfigure=1
do_configure=1
do_make=1
do_check=0
do_install=1
do_fixinstall=1
do_pack=1
do_clean=1

if test ! "x$reposrc" = "x"
then
  srcdir="$reposrc"
  do_download=0
  do_unpack=0
  do_reconfigure=1
fi

zeroall() {
      do_download=0
      do_unpack=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --download) do_download=1 ; shift 1 ;;
    --unpack) do_unpack=1 ; shift 1 ;;
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --download-only) zeroall; do_download=1 ; shift 1 ;;
    --unpack-only) zeroall; do_unpack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_download" == "x1"
then
  rm -f ${logdir}/download.log
  test -f ${pkgbuilddir}/${SOURCE_ARCHIVE} || {
    echo "Downloading ${SOURCE_ARCHIVE} ..." 2>&1 | tee ${logdir}/download.log
    wget -P ${pkgbuilddir} $url 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    b=$(basename $url)
    if test ! "x$b" = "x${SOURCE_ARCHIVE}"
    then
      mv -f $b ${SOURCE_ARCHIVE}
    fi
  }

  if test -f ${SOURCE_ARCHIVE}.sha512
  then
    echo "Verifying sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum -c ${SOURCE_ARCHIVE}.sha512 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  else
    echo "Calculating sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum ${SOURCE_ARCHIVE} >${SOURCE_ARCHIVE}.sha512 2>>${logdir}/download.log || fail $LINENO
  fi
  echo "Done downloading"
fi

if test "x$do_unpack" == "x1"
then
  rm -f ${logdir}/unpack.log
  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/unpack.log
    rm -rf ${srcdir}/* 2>&1 | tee -a ${logdir}/unpack.log
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee ${logdir}/unpack.log
    exit 2
  fi

  echo Cleaning up inst and build directories | tee -a ${logdir}/unpack.log
  rm -rf ${blddir} ${instdir} 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Unpacking $SOURCE_ARCHIVE in `pwd`" | tee -a ${logdir}/unpack.log
  case "$SOURCE_ARCHIVE" in
  *.tar.bz2 ) tar xjf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.gz  ) tar xzf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.zip     ) unzip -q $SOURCE_ARCHIVE  2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.lzma) tar --lzma -xf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.xz) tar xJf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  esac
  if test $(ls ${srcdir} -l | wc -l) -ge 2
  then
    echo "Unpacked into ${srcdir} successfully!" | tee -a ${logdir}/unpack.log
  else
    echo "${srcdir} is empty - did not unpack into it?" | tee -a ${logdir}/unpack.log
    exit 3
  fi
  echo "#include \"../gnulib-lib/ostream.c\"">${srcdir}/gettext-tools/woe32dll/c++ostream.cc
  echo "Done unpacking"
fi

patch_list=
if test "x$do_patch" == "x1"
then
  rm -f ${logdir}/patch.log
  echo cd ${srcdir}
  cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test ! "x${patchfiles}" == "x"
  then
    echo "Patching in `pwd` from patchfiles ${patchfiles}" | tee -a ${logdir}/patch.log
  fi
  for patchfile in ${patchfiles}
  do
    if test ! "x${patchfiles}" == "x"
    then
      echo "Applying ${patchfile}" | tee -a ${logdir}/patch.log
      echo patch -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/patch.log
      patch -N -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/patch.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    patch_list="$patch_list $patchfile"
  done
  echo "Done patching"
fi

if test "x$do_reconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Reconfiguring in `pwd` | tee -a ${logdir}/reconfigure.log
#  autoreconf -fi -I macros 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
	echo "Bootstrapping. This may take some time..."

	echo "libtoolize"
	libtoolize --automake --copy --force

	./autogen.sh --skip-gnulib

#	echo "gettext-runtime"
#	(cd gettext-runtime
#	 ../build-aux/fixaclocal aclocal -I m4 -I ../m4 -I gnulib-m4
#	 autoconf
#	 autoheader && touch config.h.in
#	 automake
#	)
#
#	echo "gettext-runtime/libasprintf"
#	(cd gettext-runtime/libasprintf
#	 ../../build-aux/fixaclocal aclocal -I ../../m4 -I ../m4 -I gnulib-m4
#	 autoconf
#	 autoheader && touch config.h.in
#	 automake
#	)
#
#	cp -p gettext-runtime/ABOUT-NLS gettext-tools/ABOUT-NLS
#
#	echo "gettext-tools"
#	(cd gettext-tools
#	 ../build-aux/fixaclocal aclocal -I m4 -I ../gettext-runtime/m4 -I ../m4 -I gnulib-m4 -I libgrep/gnulib-m4 -I libgettextpo/gnulib-m4
#	 autoconf
#	 autoheader && touch config.h.in
#	 automake
#	)
#
#	echo "gettext-tools/examples"
#	(cd gettext-tools/examples
#	 ../../build-aux/fixaclocal aclocal -I ../../gettext-runtime/m4 -I ../../m4
#	 autoconf
#	 automake
#	 # Rebuilding the examples PO files is only rarely needed.
#	 #if ! $quick; then
#	 #  ./configure && (cd po && make update-po) && make distclean
#	 #fi
#	)
#
#	echo "topdir"
#	build-aux/fixaclocal aclocal -I m4
#	autoconf
#	automake
#
#	cd ${SRCDIR}
#
   chmod +x build-aux/git-version-gen
   chmod +x gettext-tools/misc/gettextize
   chmod +x gettext-tools/misc/autopoint
#  chmod +x gettext-tools/misc/gettextize.in
#  chmod +x gettext-tools/misc/autopoint.in

elif test "x$do_preconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Patching in `pwd` from ${preconf_patches} | tee -a ${logdir}/reconfigure.log
  for patchfile in ${pkgbuilddir}/preconf-patches/*${subsys}.patch
  do
    patch -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  done
fi

mkdir -p ${blddir} || fail $LINENO

if test "x$do_configure" == "x1"
then
  rm -f ${logdir}/configure.log
  cd ${blddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test "x${srctobuild}" != "x"; then
    lndir ${srcdir} . 2>&1 | tee -a ${logdir}/configure.log
  fi

  eval "install_dirs=\"$install_dirs\""

  build_host_target=
  if test ! "x${buildalias}" == "x"
  then
    build_host_target+=" --build=${buildalias}"
  fi
  if test ! "x${hostalias}" == "x"
  then
    build_host_target+=" --host=${hostalias}"
  fi
  if test ! "x${targetalias}" == "x"
  then
    build_host_target+=" --target=${targetalias}"
  fi

  echo Configuring in `pwd` | tee -a ${logdir}/configure.log
  ../$(basename ${srcdir})/configure \
    --prefix=${prefix} \
    --enable-shared --enable-static --enable-silent-rules \
    --enable-threads=posix \
    --with-included-gettext --with-included-glib --with-included-libcroco \
    --with-included-unistring --with-included-libxml \
    --disable-rpath \
    --disable-openmp \
    --without-emacs --disable-native-java --disable-curses \
    --enable-relocatable \
    ${install_dirs} \
    ${build_host_target} \
    CPPFLAGS="$CPPFLAGS -D__USE_MINGW_ANSI_STDIO=1 -D__USE_MINGW_FSEEK" \
    CFLAGS="$CFLAGS" STRIP=true \
    CXXFLAGS="$CFLAGS" \
    2>&1 | tee -a ${logdir}/configure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

alias_path="${instdir}${prefix}/share/locale\;${instdir}${prefix}/local/share/locale"

if test "x$do_make" == "x1"
then
  rm -f ${logdir}/make.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Making
  make $MAKEFLAGS aliaspath="${alias_path}" EMACS=no 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_install" == "x1"
then
  rm -f ${logdir}/install.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Installing into ${instdir} from `pwd` | tee ${logdir}/install.log
  make install aliaspath="${alias_path}" EMACS=no DESTDIR=${instdir} 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${pkgbuilddir}
  if test ! "x${scriptfiles}" == "x"
  then
    mkdir -p ${instdir}${prefix}/var/lib/mingw-get/scripts
  fi
  for scriptfile in ${scriptfiles}
  do
    if test ! "x${scriptfile}" == "x"
    then
      cp ${scriptfile} ${instdir}${prefix}/var/lib/mingw-get/scripts/$(echo ${scriptfile} | sed -e 's/'${badname}'-/'${badname}'-'${ver}'-'${rev}'-/')
    fi
  done
fi

if test "x$do_check" == "x1"
then
  rm -f ${logdir}/test.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  # Setting PATH isn't enough, we have to give xgettext OUR versions of dlls
  cp -f ${instdir}/${prefix}/bin/*.dll ${blddir}/gettext-tools/src/.libs/

  echo "Testing" | tee -a ${logdir}/test.log

  alias_eval=$(eval echo ${alias_path})
  cd ${blddir}/gettext-runtime
  PATH=${instdir}/${prefix}/bin:$PATH make -j1 aliaspath=${alias_eval} EMACS=no -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  alias_eval=$(eval echo ${alias_path})
  cd ${blddir}/gettext-tools
  PATH=${instdir}/${prefix}/bin:$PATH make -j1 aliaspath=${alias_eval} EMACS=no -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi


if test "x$do_fixinstall" == "x1"
then
  rm -f ${logdir}/fixinstall.log
  echo Fixing the installation | tee ${logdir}/fixinstall.log
  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  for f in ${docfiles} ${licfiles}
  do
    if test -f ${instdir}${prefix}/${docdir}/$(basename ${f})
    then
      cp -r ${f} ${instdir}${prefix}/${docdir}/${f}
    else
      cp -r ${f} ${instdir}${prefix}/${docdir}/$(basename ${f})
    fi
  done
  mkdir -p ${instdir}${prefix}/share/doc/${capname} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cp ${pkgbuilddir}/${subsys}-${badname}.RELEASE_NOTES \
    ${instdir}${prefix}/share/doc/${capname}/${badname}-${ver}-${rev}-${subsys}.RELEASE_NOTES.txt 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  /mingw/bin/laremove.py $(cd ${instdir}; pwd -W) && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  /mingw/bin/split-debug.py $(cd ${instdir}; pwd -W) --target=$buildalias && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  echo Packing | tee -a ${logdir}/pack.log
  cd ${instdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  pkgtmp=${ver}-${rev}-${subsys}_${arch}

  tar cv --xz -f ${pkgbuilddir}/gettext-base-${pkgtmp}-bin.tar.xz \
$p1/bin/envsubst.exe \
$p1/bin/gettext.exe \
$p1/bin/gettext.sh \
$p1/bin/ngettext.exe \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-base-${pkgtmp}-dbg.tar.xz \
$p1/bin/envsubst.exe.dbg \
$p1/bin/gettext.exe.dbg \
$p1/bin/ngettext.exe.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-base-${pkgtmp}-lang.tar.xz \
$p1/share/locale/*/LC_MESSAGES/gettext-runtime.mo \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-${pkgtmp}-bin.tar.xz \
--exclude=*.dbg \
--exclude=$p1/bin/libintl*.dll \
--exclude=$p1/bin/libasprintf*.dll \
--exclude=$p1/bin/libgettextpo*.dll \
--exclude=$p1/bin/envsubst.exe \
--exclude=$p1/bin/gettext.exe \
--exclude=$p1/bin/gettext.sh \
--exclude=$p1/bin/ngettext.exe \
$p1/bin/* \
$p1/lib/gettext \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-${pkgtmp}-dbg.tar.xz \
--exclude=$p1/bin/envsubst.exe.dbg \
--exclude=$p1/bin/gettext.exe.dbg \
--exclude=$p1/bin/ngettext.exe.dbg \
$p1/bin/*.exe.dbg \
$p1/bin/libgettextlib-0-19-2.dll.dbg \
$p1/bin/libgettextsrc-0-19-2.dll.dbg \
$p1/lib/gettext/*.exe.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi


  tar cv --xz -f ${pkgbuilddir}/gettext-${pkgtmp}-dev.tar.xz \
$p1/share/aclocal \
$p1/share/gettext \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-base-${pkgtmp}-doc.tar.xz \
$p1/share/man/man1/envsubst.1 \
$p1/share/man/man1/gettext.1 \
$p1/share/man/man1/ngettext.1 \
$p1/share/man/man3 \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libgettextpo0-${pkgtmp}-dll.tar.xz \
$p1/bin/libgettextpo-0.dll \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libgettextpo0-${pkgtmp}-dbg.tar.xz \
$p1/bin/libgettextpo-0.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libgettextpo-${pkgtmp}-dev.tar.xz \
$p1/include/gettext-po.h \
$p1/lib/libgettextpo.a \
$p1/lib/libgettextpo.dll.a \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libasprintf0-${pkgtmp}-dll.tar.xz \
$p1/bin/libasprintf-0.dll \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libasprintf0-${pkgtmp}-dbg.tar.xz \
$p1/bin/libasprintf-0.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libasprintf-${pkgtmp}-dev.tar.xz \
$p1/include/autosprintf.h \
$p1/lib/libasprintf.a \
$p1/lib/libasprintf.dll.a \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-${pkgtmp}-lang.tar.xz \
$p1/share/locale/*/LC_MESSAGES/gettext-tools.mo \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/gettext-${pkgtmp}-doc.tar.xz \
--exclude=$p1/share/man1/envsubst.1 \
--exclude=$p1/share/man1/gettext.1 \
--exclude=$p1/share/man1/ngettext.1 \
--exclude=$p1/share/info/dir \
$p1/share/man/man1 \
$p1/share/info \
$p1/${docdir} \
$p1/share/doc/${capname} \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libintl8-${pkgtmp}-dll.tar.xz \
$p1/bin/libintl-8.dll \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libintl8-${pkgtmp}-dbg.tar.xz \
$p1/bin/libintl-8.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/libintl-${pkgtmp}-dev.tar.xz \
$p1/include/libintl.h \
$p1/lib/libintl.a \
$p1/lib/libintl.dll.a \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  BLD="\
pkgbuild.sh \
${subsys}-${badname}.RELEASE_NOTES \
$(if test -d patches; then echo patches; fi) \
${scriptfiles} \
${SOURCE_ARCHIVE}.sha512 \
"

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bld.tar.xz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-src.tar.xz ${SOURCE_ARCHIVE} $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_clean" == "x1"
then

cd ${pkgbuilddir}
  rm -rf ${blddir} ${instdir}
  if test -d "${srcdir}" -a ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    rm -rf ${srcdir}
  fi
fi

echo "Done"
exit 0
