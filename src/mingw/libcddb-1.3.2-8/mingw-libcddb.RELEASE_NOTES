mingw libcddb
========================================================================
Libcddb is a C library to access data on a CDDB server (freedb.org).
It allows you to:
  search the database for possible CD matches;
  retrieve detailed information about a specific CD;
  submit new CD entries to the database.
Libcddb supports both the custom CDDB protocol and tunnelling the query and
read operations over plain HTTP. It is also possible to use an HTTP proxy
server. If you want to speed things up, you can make use of the built-in
caching facility provided by the library.

Canonical homepage:
  http://libcddb.sourceforge.net/index.html

Canonical download:
  http://libcddb.sourceforge.net/download.html

License:
  LGPLv2+

Language:
  C

========================================================================

----------  libcddb-1.3.2-8 -- 2014-05-04 -----------
* Re-build with gcc-4.9.0, newer mingw-w64

----------  libcddb-1.3.2-7 -- 2014-02-09 -----------
* Re-build against newer mingw-w64

----------  libcddb-1.3.2-6 -- 2013-09-30 -----------
* Re-build against newer mingw-w64

----------  libcddb-1.3.2-5 -- 2013-08-11 -----------
* Fixed reconfiguration (doesn't put .m4 files in / anymore)

----------  libcddb-1.3.2-4 -- 2013-08-06 -----------
* Fixed the -dev package naming

----------  libcddb-1.3.2-3 -- 2013 Apr 07 -----------
* i686-mingw-w64 release

----------  libcddb-1.3.2-2 -- 2012 Sep 15 -----------
* Recompiled with detached debug info

----------  libcddb-1.3.2-1 -- 2012 Apr 13 -----------
* First release of libcddb for mingw using new packaging standard. 
