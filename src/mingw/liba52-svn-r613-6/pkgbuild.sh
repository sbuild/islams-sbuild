#! /bin/bash

name=liba52
badname=liba52
rev=6
subsys=mingw
d1_srcname="\${name}-\${ver}"
prefix=/mingw
CFLAGS="-g -O2"
LDFLAGS="-g -O2"
d1_docdir="share/doc/\${badname}/\${ver}"
SOURCE_ARCHIVE_FORMAT=.tar.xz
d1_SOURCE_ARCHIVE="\${name}-\${ver}\${SOURCE_ARCHIVE_FORMAT}"
SOURCE_ARCHIVE_PATTERN="${name}-*${SOURCE_ARCHIVE_FORMAT}"
preconf_patches=
docfiles="README \
ChangeLog \
NEWS"
licfiles="AUTHORS \
COPYING"

if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=/etc --localstatedir=/var"; fi

repo_url=svn://svn.videolan.org/liba52/trunk
VCS="svn"
svn_static_rev=613

d1_pkgtmp="\${badname}-\${ver}-\${rev}-\${subsys}"
d1_BINPKG="\${pkgtmp}-bin.tar.lzma"
d1_DEVPKG="\${pkgtmp}-dev.tar.lzma"
d1_DLLPKG="\${badname}-\${ver}-\${rev}-\${subsys}-dll-\${dllver}.tar.lzma"
d1_DOCPKG="\${pkgtmp}-doc.tar.lzma"
d1_LICPKG="\${pkgtmp}-lic.tar.lzma"
d1_COMPKG="\${badname}-\${ver}-\${rev}-msys-\${SYSVER}-completion.tar.lzma"
d1_LANGPKG="\${pkgtmp}-lang.tar.lzma"
d1_DBGPKG="\${pkgtmp}-dbg.tar.lzma"
d1_BLDPKG="\${pkgtmp}-bld.tar.lzma"
d1_SRCPKG="\${pkgtmp}-src.tar.lzma"
d1_BIN_CONTENTS="--exclude=*.dbg --exclude=bin/*.dll bin"
d1_DEV_CONTENTS="--exclude=*.dbg --exclude=*.dll --exclude=*.la lib include"
d1_DLL_CONTENTS="--exclude=*.dbg bin/*.dll"
d1_LIC_CONTENTS="\$(for i in \$licfiles; do echo \$docdir/\$i; done)"
d1_DOC_CONTENTS="share/doc/\${badname}/\${ver}"
d2_LIC_CONTENTS="\$LIC_CONTENTS \$(for i in \$docfiles; do echo --exclude=\$i; done)"
d2_DOC_CONTENTS="\$DOC_CONTENTS \$(for i in \$licfiles; do echo --exclude=\$i; done) share/doc/\${capname} share/man"
d1_COM_CONTENTS="etc/bash_completion.d"
d1_LANG_CONTENTS="share/locale"
d1_DBG_CONTENTS=-name\ "*.dbg"
d1_BLD_CONTENTS="pkgbuild.sh \
\${subsys}-\${badname}.RELEASE_NOTES \
patches \
\${SOURCE_ARCHIVE}.md5"
d1_SRC_CONTENTS="${d1_BLD_CONTENTS} \
\${SOURCE_ARCHIVE}"

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw"
then
  echo "You must be in an MINGW shell to build a mingw package"
  exit 5
fi

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/bld
logdir=${pkgbuilddir}/logs
repodir=${pkgbuilddir}/repo
d1_srcdir="\${pkgbuilddir}/\${name}-\${ver}"
srcdir_pattern="${pkgbuilddir}/${name}-*"

d1_srcdirname="\$(basename \${srcdir})"

capname=Unknown
if test "x${subsys}" == "xmingw"
then
capname=MinGW
elif test "x${subsys}" == "xmsys"
then
capname=MSYS
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}

if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi

p1=${prefix:1}

do_clone=1
do_copy=1
do_patch=1
do_preconfigure=0
do_reconfigure=1
do_configure=1
do_make=1
do_check=0
do_install=1
do_fixinstall=1
do_pack=1
do_clean=1

if test ! "x$repodir" = "x"
then
  do_clone=1
  do_copy=1
  do_reconfigure=1
fi

zeroall() {
      do_clone=0
      do_download=0
      do_unpack=0
      do_copy=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_clone" == "x1"
then
  if test "x$VCS" == "xgit"
  then
    if test ! -d "${repodir}"; then
      mkdir -p ${repodir} # Don't fail if exists
      git clone ${repo_url} ${repodir} || fail $LINENO
      cd ${repodir}
      git submodule init
      git submodule update
    else
      pushd .
      cd ${repodir}
      git fetch || fail $LINENO
      git rebase || fail $LINENO
      popd
    fi
  elif test "x$VCS" == "xsvn"
  then
    if test ! -d "${repodir}"; then
      mkdir -p ${repodir} # Don't fail if exists
      svn checkout ${repo_url} ${repodir} || fail $LINENO
    fi
    pushd .
    cd ${repodir}
    if test -n "${svn_static_rev}"
    then
      svn up -r ${svn_static_rev} || fail $LINENO
    else
      svn up || fail $LINENO
    fi
    popd
  fi
fi

if test "x$VCS" == "xgit"
then
  commit=$(cd ${repodir} && git log --no-color -1 | grep -e '^commit [0-9a-f]\{40\}$' | sed -e 's/commit \([0-9a-f]\{40\}\)/\1/')
  echo commit = $commit

  if [ ! -n "$commit" ]; then
    exit 1
  fi

  #Check if this commit matches a tag
  tag=$(cd ${repodir} && git tag --contains $commit 2>/dev/null)
  echo tag = \'$tag\'

  if [ -n "$tag" ]; then
    #Matches a tag, use tag as version
    version=$tag
    eval $(echo -e "v=\"$tag\"\nv1=v\nv2=v\ndot1=v.find('.')\nif dot1 > 0:\n  v1=v1[:dot1]\n  minus=v1.rfind('-')\n  if minus > 0:\n    v1=v1[minus+1:]\n  dot2=v2[dot1+1:].find('.')\n  if dot2 > 0:\n    v2=v2[:dot1+1+dot2]\nprint ('ver={}\\\nver1={}\\\nver2={}'.format (v, v1, v2))" | python)
  else
    #No tag for this commit, use commit hash as version
    version=git-$commit
    ver=git-$commit
    ver1=git-$commit
    ver2=git-$commit
  fi
elif test "x$VCS" == "xsvn"
then
  svnrev=$(cd ${repodir} && LC_ALL=C svn info | grep "Revision: " | sed -e 's/Revision: \([0-9]\+\)/\1/')
  if [ ! -n "$svnrev" ]
  then
    exit 1
  fi
  ver="svn-r$svnrev"
  ver1="svn-r$svnrev"
  ver2="svn-r$svnrev"
fi

docdir=$(eval echo $d1_docdir)
SOURCE_ARCHIVE=$(eval echo $d1_SOURCE_ARCHIVE)

pkgtmp=$(eval echo $d1_pkgtmp)
BINPKG=$(eval echo $d1_BINPKG)
DEVPKG=$(eval echo $d1_DEVPKG)
DLLPKG=$(eval echo $d1_DLLPKG)
DOCPKG=$(eval echo $d1_DOCPKG)
LICPKG=$(eval echo $d1_LICPKG)
COMPKG=$(eval echo $d1_COMPKG)
LANGPKG=$(eval echo $d1_LANGPKG)
DBGPKG=$(eval echo $d1_DBGPKG)
BLDPKG=$(eval echo $d1_BLDPKG)
SRCPKG=$(eval echo $d1_SRCPKG)
BIN_CONTENTS=$(eval echo $d1_BIN_CONTENTS)
DEV_CONTENTS=$(eval echo $d1_DEV_CONTENTS)
DLL_CONTENTS=$(eval echo $d1_DLL_CONTENTS)
LIC_CONTENTS=$(eval echo $d1_LIC_CONTENTS)
DOC_CONTENTS=$(eval echo $d1_DOC_CONTENTS)
LIC_CONTENTS=$(eval echo $d2_LIC_CONTENTS)
DOC_CONTENTS=$(eval echo $d2_DOC_CONTENTS)
COM_CONTENTS=$(eval echo $d1_COM_CONTENTS)
LANG_CONTENTS=$(eval echo $d1_LANG_CONTENTS)
DBG_CONTENTS=$(eval echo $d1_DBG_CONTENTS)
BLD_CONTENTS=$(eval echo $d1_BLD_CONTENTS)
SRC_CONTENTS=$(eval echo $d1_SRC_CONTENTS)
srcdir=$(eval echo $d1_srcdir)
srcdirname=$(eval echo $d1_srcdirname)

echo "Source directory is $srcdir"

if test "x$do_copy" == "x1"
then
  rm -f "${logdir}/copy.log"

  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/copy.log
    rm -rf "${srcdir}" 2>&1 | tee -a ${logdir}/copy.log
    echo "Deleting directories matching ${srcdir_pattern}" | tee ${logdir}/copy.log
    for d in ${srcdir_pattern}
    do 
      if test -d $d
      then
        echo "Deleting $d" | tee ${logdir}/copy.log
        rm -rf $d
      fi
    done
    echo "Deleting files matching ${SOURCE_ARCHIVE_PATTERN}" | tee ${logdir}/copy.log
    for f in ${SOURCE_ARCHIVE_PATTERN}
    do 
      if test -f $f
      then
        echo "Deleting $f" | tee ${logdir}/copy.log
        rm -f $f
      fi
    done
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee "${logdir}/copy.log"
    exit 2
  fi
  
  echo Cleaning up src, inst and build directories | tee -a "${logdir}/copy.log"
  rm -rf "${blddir}" "${instdir}" "${srcdir}" 2>&1 | tee -a "${logdir}/copy.log" && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd "${pkgbuilddir}" && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Copying from $repodir to $srcdir" | tee -a "${logdir}/copy.log"
  cp -rf "$repodir" "$srcdir" | tee -a "${logdir}/copy.log"
  pushd .
  echo cd ${srcdir} && cd "${srcdir}" && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  tar cJv --hard-dereference -f "${pkgbuilddir}/${SOURCE_ARCHIVE}" --exclude=.git * 2>&1 | tee -a "${logdir}/copy.log" && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  popd
  echo "Done copying"
fi

patch_list=
if test "x$do_patch" == "x1"
then
  rm -f "${logdir}/patch.log"
  echo cd "${srcdir}"
  cd "${srcdir}" && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test ! "x${patchfiles}" == "x"
  then
    echo "Patching in `pwd` from patchfiles ${patchfiles}" | tee -a ${logdir}/patch.log
  fi
  for patchfile in ${patchfiles}
  do
    if test ! "x${patchfiles}" == "x"
    then
      echo "Applying ${patchfile}" | tee -a ${logdir}/patch.log
      patch -p1 -i "${patchfile}" 2>&1 | tee -a ${logdir}/patch.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    patch_list="$patch_list $patchfile"
  done
  echo "Done patching"
fi

mkdir -p ${blddir} || fail $LINENO

if test "x$do_reconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Reconfiguring in `pwd` | tee -a ${logdir}/reconfigure.log
  CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" ${srcdir}/bootstrap --prefix=${prefix} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
elif test "x$do_preconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Patching in `pwd` from ${preconf_patches} | tee -a ${logdir}/reconfigure.log
  for patchfile in ${pkgbuilddir}/preconf-patches/*${subsys}.patch
  do
    patch -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  done
fi

if test "x$do_configure" == "x1"
then
  rm -f ${logdir}/configure.log
  cd ${blddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  eval "install_dirs=\"$install_dirs\""

  build_host_target=
  if test ! "x${buildalias}" == "x"
  then
    build_host_target+=" --build=${buildalias}"
  fi
  if test ! "x${hostalias}" == "x"
  then
    build_host_target+=" --host=${hostalias}"
  fi
  if test ! "x${targetalias}" == "x"
  then
    build_host_target+=" --target=${targetalias}"
  fi

  #lndir ${srcdir} . 2>&1 | tee -a ${logdir}/configure.log
  echo Configuring in `pwd` | tee -a ${logdir}/configure.log
  ../$(basename ${srcdir})/configure \
    --enable-shared \
    --prefix=${prefix} \
    ${install_dirs} \
    ${build_host_target} \
    STRIP=true CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" \
    2>&1 | tee -a ${logdir}/configure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_make" == "x1"
then
  rm -f ${logdir}/make.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Making
  make $MAKEFLAGS 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_install" == "x1"
then
  rm -f ${logdir}/install.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Installing into ${instdir} from `pwd` | tee ${logdir}/install.log
  make install DESTDIR=${instdir} $MAKEFLAGS 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${pkgbuilddir}
  if test ! "x${scriptfiles}" == "x"
  then
    mkdir -p ${instdir}${prefix}/var/lib/mingw-get/scripts
  fi
  for scriptfile in ${scriptfiles}
  do
    if test ! "x${scriptfile}" == "x"
    then
      cp ${scriptfile} ${instdir}${prefix}/var/lib/mingw-get/scripts/$(echo ${scriptfile} | sed -e 's/'${badname}'-/'${badname}'-'${ver}'-'${rev}'-/')
    fi
  done
fi

if test "x$do_check" == "x1"
then
  rm -f ${logdir}/test.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Testing" | tee -a ${logdir}/test.log
  make -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi


if test "x$do_fixinstall" == "x1"
then
  rm -f ${logdir}/fixinstall.log
  echo Fixing the installation | tee ${logdir}/fixinstall.log
  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  for f in ${docfiles} ${licfiles}
  do
    if test -f ${instdir}${prefix}/${docdir}/$(basename ${f})
    then
      cp -r ${f} ${instdir}${prefix}/${docdir}/${f}
    else
      cp -r ${f} ${instdir}${prefix}/${docdir}/$(basename ${f})
    fi
  done
  mkdir -p ${instdir}${prefix}/share/doc/${capname} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cp ${pkgbuilddir}/${subsys}-${badname}.RELEASE_NOTES \
    ${instdir}${prefix}/share/doc/${capname}/${badname}-${ver}-${rev}-${subsys}.RELEASE_NOTES.txt 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  /mingw/bin/laremove.py $(cd ${instdir}; pwd -W) && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  /mingw/bin/split-debug.py $(cd ${instdir}; pwd -W) --target=$buildalias && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  echo Packing | tee -a ${logdir}/pack.log
  cd ${instdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  pkgtmp=${ver}-${rev}-${subsys}_${arch}

  tar cv --xz -f ${pkgbuilddir}/liba52-0-${pkgtmp}-dll.tar.xz \
$p1/bin/liba52-0.dll \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/liba52-0-${pkgtmp}-dbg.tar.xz \
$p1/bin/liba52-0.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/liba52-${pkgtmp}-dev.tar.xz \
$p1/include \
$p1/lib \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/liba52-utils-${pkgtmp}-bin.tar.xz \
$p1/bin/*.exe \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/liba52-utils-${pkgtmp}-dbg.tar.xz \
$p1/bin/*.exe.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-doc.tar.xz \
$p1/share/man \
$p1/${docdir} \
$p1/share/doc/${capname} \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  BLD="\
pkgbuild.sh \
${subsys}-${badname}.RELEASE_NOTES \
$(if test -d patches; then echo patches; fi) \
${scriptfiles} \
"

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bld.tar.xz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-src.tar.xz ${SOURCE_ARCHIVE} $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_clean" == "x1"
then

cd ${pkgbuilddir}
  rm -rf ${blddir} ${instdir}
  if test -d "${srcdir}" -a ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    rm -rf ${srcdir}
  fi
fi

echo "Done"
exit 0
