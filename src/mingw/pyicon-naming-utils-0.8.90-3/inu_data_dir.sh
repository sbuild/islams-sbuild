#!/bin/sh
# Setup the INU_DATA_DIR environment variable.
if test -z "$INU_DATA_DIR";
then
  if test "x$MSYSTEM" == "xMINGW32";
  then
    INU_DATA_DIR=$(cd /mingw ;pwd -W)/share/icon-naming-utils
  else
    INU_DATA_DIR=/usr/share/icon-naming-utils
  fi
fi
export INU_DATA_DIR
