#!/bin/sh

gcat=/mingw/etc/xml/catalog
tcat=/mingw/etc/xml/docbook-xsl.xml

if test -f ${tcat}
then
  xmlcatalog --noout --del "http://docbook.sourceforge.net/release/xsl/" ${tcat}
  xmlcatalog --noout --del "http://docbook.sourceforge.net/release/xsl/" ${tcat}
  if test $(cat ${tcat} | wc -l) -eq $(xmlcatalog --create | wc -l)
  then
    rm ${tcat}
  fi
fi

en=$(echo -en "\n\b")

uris=\
"http://docbook.sourceforge.net/release/xsl/"$en\
"http://docbook.sourceforge.net/release/xsl/"

if test -f ${gcat}
then
  normal_IFS=$IFS
  IFS=$en
  for pid in $uris
  do
    IFS=$normal_IFS
    remove_catalog=0
    if test ! -f ${tcat}
    then
      remove_catalog=1
    else
      if test $(cat ${tcat} | grep --regexp=\""${pid}.*"\" | wc -l) == 0
      then
        remove_catalog=1
      fi
    fi
    if test $remove_catalog == 1
    then
      echo "Removing reference \"$pid\" to catalog ${tcat}"
      xmlcatalog --noout --del "$pid" ${gcat}
    fi
    IFS=$en
  done
  IFS=$normal_IFS
  if test $(cat ${gcat} | wc -l) -eq $(xmlcatalog --create | wc -l)
  then
    rm ${gcat}
  fi
fi
