#!/bin/sh
mkdir -p /mingw/etc/xml

if ! test -f /mingw/etc/xml/catalog
then
  xmlcatalog --noout --create /mingw/etc/xml/catalog
fi
xmlcatalog --noout --add delegateURI "http://docbook.sourceforge.net/release/xsl/" "./docbook-xsl.xml" /mingw/etc/xml/catalog
xmlcatalog --noout --add delegateSystem "http://docbook.sourceforge.net/release/xsl/" "./docbook-xsl.xml" /mingw/etc/xml/catalog

if ! test -f /mingw/etc/xml/docbook-xsl.xml
then
  xmlcatalog --noout --create /mingw/etc/xml/docbook-xsl.xml
fi
xmlcatalog --noout --add delegateURI "http://docbook.sourceforge.net/release/xsl/" "../../share/xml/docbook/stylesheet/docbook-xsl/catalog.xml" /mingw/etc/xml/docbook-xsl.xml
xmlcatalog --noout --add delegateSystem "http://docbook.sourceforge.net/release/xsl/" "../../share/xml/docbook/stylesheet/docbook-xsl/catalog.xml" /mingw/etc/xml/docbook-xsl.xml
