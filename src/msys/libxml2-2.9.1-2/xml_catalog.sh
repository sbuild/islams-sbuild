#!/bin/sh
# Setup the XML_CATALOG_FILES environment variable.
if test -z "$XML_CATALOG_FILES";
then
  #if test "x$MSYSTEM" == "xMINGW32";
  #then
  #  XML_CATALOG_FILES=$(cd /mingw ;pwd -W)/etc/xml/catalog
  #else
    XML_CATALOG_FILES=/etc/xml/catalog
  #fi
fi
export XML_CATALOG_FILES
