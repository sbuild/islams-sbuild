#!/bin/sh
mkdir -p /etc/xml

if test ! -f /etc/xml/catalog
then
  xmlcatalog --create --noout /etc/xml/catalog
fi
