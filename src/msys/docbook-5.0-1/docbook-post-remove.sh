#!/bin/sh

gcat=/etc/xml/catalog
tcat=/etc/xml/docbook5-xml.xml

if test -f ${tcat}
then
  xmlcatalog --noout --del "-//OASIS//DTD DocBook XML 5//EN" ${tcat}
  xmlcatalog --noout --del "-//OASIS//DTD DocBook XML 5.0//EN" ${tcat}
  xmlcatalog --noout --del "http://docbook.org/xml/5.0/" ${tcat}
  xmlcatalog --noout --del "http://www.oasis-open.org/docbook/xml/5.0/" ${tcat}
  if test $(cat ${tcat} | wc -l) -eq $(xmlcatalog --create | wc -l)
  then
    rm ${tcat}
  fi
fi

en=$(echo -en "\n\b")

pids=\
"-//OASIS//DTD DocBook XML 5"$en\

uris=\
"http://docbook.org/xml/5"$en\
"http://www.oasis-open.org/docbook/xml/5"$en\

if test -f ${gcat}
then
  normal_IFS=$IFS
  IFS=$en
  for pid in $pids $uris
  do
    IFS=$normal_IFS
    remove_catalog=0
    if test ! -f ${tcat}
    then
      remove_catalog=1
    else
      if test $(cat ${tcat} | grep --regexp=\""${pid}.*"\" | wc -l) == 0
      then
        remove_catalog=1
      fi
    fi
    if test $remove_catalog == 1
    then
      echo "Removing reference \"$pid\" to catalog ${tcat}"
      xmlcatalog --noout --del "$pid" ${gcat}
    fi
    IFS=$en
  done
  IFS=$normal_IFS
  if test $(cat ${gcat} | wc -l) -eq $(xmlcatalog --create | wc -l)
  then
    rm ${gcat}
  fi
fi
