#!/bin/sh
mkdir -p /etc/xml

xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/5" "./docbook5-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/5" "./docbook5-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML 5" "./docbook5-xml.xml" /etc/xml/catalog

if ! test -f /etc/xml/docbook5-xml.xml
then
  xmlcatalog --create --noout /etc/xml/docbook5-xml.xml
fi
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML 5//EN" "/usr/share/xml/docbook/schema/catalog-docbook5.xml" /etc/xml/docbook5-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML 5.0//EN" "/usr/share/xml/docbook/schema/catalog-docbook5.xml" /etc/xml/docbook5-xml.xml
xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/5.0/" "/usr/share/xml/docbook/schema/catalog-docbook5.xml" /etc/xml/docbook5-xml.xml
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/5.0/" "/usr/share/xml/docbook/schema/catalog-docbook5.xml" /etc/xml/docbook5-xml.xml
