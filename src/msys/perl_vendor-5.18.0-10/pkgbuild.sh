#! /bin/bash

name=perl_vendor
badname=perl_vendor
ver=5.18.0
ver1=5
ver2=5.18
dllver=0
rev=10
shortver=$(echo "${ver}${rev}" | sed 's/-.*//')
libperl="msys-perl${ver2/./_}.dll"
subsys=msys2
srcname=${name}-${ver}
prefix=/usr
CFLAGS="-g -O2"
docdir=share/doc/${badname}/${ver}
SOURCE_ARCHIVE_FORMAT=.tar.bz2
SOURCE_ARCHIVE=${name}-${ver}${SOURCE_ARCHIVE_FORMAT}
preconf_patches=
docfiles="
README \
Changes*
INSTALL"
licfiles="Artistic Copying"
rebasebase=0x58000000

#if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=/etc --localstatedir=/var"; fi
if test "x${install_dirs}" == "x"; then install_dirs="--datadir=\${prefix}/share --infodir=\${prefix}/share/info --mandir=\${prefix}/share/man --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=\${prefix}/etc --localstatedir=/var"; fi


reposrc=""

declare -a archives

i=0
archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Pod-Escapes/Pod-Escapes-1.04.tar.gz/00ea2e0d2e84ed98517a4616708b68d3/Pod-Escapes-1.04.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Pod-Simple/Pod-Simple-3.28.tar.gz/ee65094e29924948ae02fe33229cc5e4/Pod-Simple-3.28.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Test-Pod/Test-Pod-1.45.tar.gz/089c8f272931df82f6c4d11a74f04628/Test-Pod-1.45.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Devel-Symdump/Devel-Symdump-2.08.tar.gz/68e3a2f2f989bff295ee63aed5a2a1e5/Devel-Symdump-2.08.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Pod-Coverage/Pod-Coverage-0.22.tar.gz/6cf04053968db85c355a740ab170aaf5/Pod-Coverage-0.22.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Test-Pod-Coverage/Test-Pod-Coverage-1.08.tar.gz/33405cca7c75b7b89c06ba30eea66692/Test-Pod-Coverage-1.08.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Compress-Bzip2/Compress-Bzip2-2.17.tar.gz/d8b809f6efe3edb33f81d608c932b2ef/Compress-Bzip2-2.17.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-IO-String/IO-String-1.08.tar.gz/250e5424f290299fc3d6b5d1e9da3835/IO-String-1.08.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Archive-Zip/Archive-Zip-1.30.tar.gz/40153666e7538b410e001aa8a810e702/Archive-Zip-1.30.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-TermReadKey/TermReadKey-2.30.tar.gz/f0ef2cea8acfbcc58d865c05b0c7e1ff/TermReadKey-2.30.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Term-ReadLine-Perl/Term-ReadLine-Perl-1.0303.tar.gz/a77ecf4921cc714820fef898c6cb0eaf/Term-ReadLine-Perl-1.0303.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Term-ReadLine-Gnu/Term-ReadLine-Gnu-1.24.tar.gz/43db7380970a0674a601c042e9486a9a/Term-ReadLine-Gnu-1.24.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-XML-NamespaceSupport/XML-NamespaceSupport-1.11.tar.gz/222cca76161cd956d724286d36b607da/XML-NamespaceSupport-1.11.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-XML-SAX-Base/XML-SAX-Base-1.08.tar.gz/38c8c3247dfd080712596118d70dbe32/XML-SAX-Base-1.08.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-XML-SAX/XML-SAX-0.99.tar.gz/290f5375ae87fdebfdb5bc3854019f24/XML-SAX-0.99.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-XML-LibXML/XML-LibXML-2.0019.tar.gz/0873ac0bae4a4bf7ba7b2d970876e4ba/XML-LibXML-2.0019.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-XML-Parser/XML-Parser-2.41.tar.gz/c320d2ffa459e6cdc6f9f59c1185855e/XML-Parser-2.41.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Proc-ProcessTable/Proc-ProcessTable-0.48.tar.gz/ddc2c67cd1184ddb0ba1f84e89b90e2a/Proc-ProcessTable-0.48.tar.gz
i=$(( $i + 1 ))

#archives[$i]=http://search.cpan.org/CPAN/authors/id/I/IN/INGY/YAML-0.80.tar.gz
archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-YAML/YAML-0.84.tar.gz/3644f03e3da5d99158963c6613f5ff92/YAML-0.84.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Config-Tiny/Config-Tiny-2.14.tar.gz/498b8fd37a190a214cddd5506be77720/Config-Tiny-2.14.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-File-Copy-Recursive/File-Copy-Recursive-0.38.tar.gz/e76dc75ab456510d67c6c3a95183f72f/File-Copy-Recursive-0.38.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-IPC-Run3/IPC-Run3-0.045.tar.gz/e8913c03a8a6c6297a6e622d656e343a/IPC-Run3-0.045.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Probe-Perl/Probe-Perl-0.03.tar.gz/8876e1b99c531800a86b383702e0ab73/Probe-Perl-0.03.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://ftp.tw.freebsd.org/pub/FreeBSD/ports/distfiles/Tee-0.14.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-IO-CaptureOutput/IO-CaptureOutput-1.1102.tar.gz/367b3c0eb33a4b600de4009b0445ae0d/IO-CaptureOutput-1.1102.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-File-pushd/File-pushd-1.002.tar.gz/eac693b6968d99e0a3da89cd7babec11/File-pushd-1.002.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-File-Which/File-Which-1.09.tar.gz/b9429edaad7f45caafa4d458afcfd8af/File-Which-1.09.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-File-HomeDir/File-HomeDir-0.99.tar.gz/cb058c68393631344c2b906026091016/File-HomeDir-0.99.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Digest-SHA/Digest-SHA-5.71.tar.gz/f40aeb91b6fe62ed4bf47ceb86382305/Digest-SHA-5.71.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-IPC-Run/IPC-Run-0.92.tar.gz/9d209c8d4bda50bea5f5a09e03db173e/IPC-Run-0.92.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Module-Signature/Module-Signature-0.68.tar.gz/c63c0b5c4e7162fc0c44512e1f832e5e/Module-Signature-0.68.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-URI/URI-1.60.tar.gz/70f739be8ce28b8baba7c5920ffee4dc/URI-1.60.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTML-Tagset/HTML-Tagset-3.20.tar.gz/d2bfa18fe1904df7f683e96611e87437/HTML-Tagset-3.20.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTML-Parser/HTML-Parser-3.69.tar.gz/d22cc6468ce670a56034be907e4e7c54/HTML-Parser-3.69.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTTP-Date/HTTP-Date-6.02.tar.gz/52b7a0d5982d61be1edb217751d7daba/HTTP-Date-6.02.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Encode-Locale/Encode-Locale-1.03.tar.gz/de8422d068634e7c1068dab4e18b452f/Encode-Locale-1.03.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-LWP-MediaTypes/LWP-MediaTypes-6.02.tar.gz/8c5f25fb64b974d22aff424476ba13c9/LWP-MediaTypes-6.02.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTTP-Message/HTTP-Message-6.03.tar.gz/d41b22c7c01b974f2b4e85d401a6b552/HTTP-Message-6.03.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-File-Listing/File-Listing-6.04.tar.gz/83f636b477741f3a014585bb9cc079a6/File-Listing-6.04.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTTP-Daemon/HTTP-Daemon-6.01.tar.gz/ed0ae02d25d7f1e89456d4d69732adc2/HTTP-Daemon-6.01.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-WWW-RobotRules/WWW-RobotRules-6.02.tar.gz/b7186e8b8b3701e70c22abf430742403/WWW-RobotRules-6.02.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTTP-Negotiate/HTTP-Negotiate-6.01.tar.gz/1236195250e264d7436e7bb02031671b/HTTP-Negotiate-6.01.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-HTTP-Cookies/HTTP-Cookies-6.01.tar.gz/ecfd0eeb88512033352c2f13c9580f03/HTTP-Cookies-6.01.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Net-HTTP/Net-HTTP-6.03.tar.gz/86957940d96649ca782b686686b82e7b/Net-HTTP-6.03.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-libwww-perl/libwww-perl-6.04.tar.gz/24acf2fe33b2295f048f8859e9665ee3/libwww-perl-6.04.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Net-IP/Net-IP-1.26.tar.gz/3a98e3ac45d69ea38a63a7e678bd716d/Net-IP-1.26.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Digest-HMAC/Digest-HMAC-1.03.tar.gz/e6a5d6f552da16eacb5157ea4369ff9d/Digest-HMAC-1.03.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Net-DNS/Net-DNS-0.68.tar.gz/05c0f6955747758bb5c9578d9bc6c3a5/Net-DNS-0.68.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Test-Reporter/Test-Reporter-1.58.tar.gz/52ef5471e28ea441c0239606b1bb93e6/Test-Reporter-1.58.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Crypt-SSLeay/Crypt-SSLeay-0.58.tar.gz/fbf3d12e58462cee00ea63239c0b13c7/Crypt-SSLeay-0.58.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://search.cpan.org/CPAN/authors/id/M/ML/MLEHMANN/common-sense-3.72.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-JSON-XS/JSON-XS-3.01.tar.gz/b7be65295baf6dd9233c6494782c1153/JSON-XS-3.01.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-JSON/JSON-2.53.tar.gz/7db1be00d44414c4962eeac222395a76/JSON-2.53.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://search.cpan.org/CPAN/authors/id/D/DA/DAGOLDEN/Metabase-Client-Simple-0.008.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Sub-Install/Sub-Install-0.926.tar.gz/89a7f82dd840bc2401f281b5f24732b9/Sub-Install-0.926.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Data-UUID/Data-UUID-1.219.tar.gz/8ca1f802b40d9b563f4de26968677097/Data-UUID-1.219.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Params-Util/Params-Util-1.07.tar.gz/02db120c0eef87aae1830cc62bdec37b/Params-Util-1.07.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Data-OptList/Data-OptList-0.107.tar.gz/17177b3cfb8941780a8736f9b9b30421/Data-OptList-0.107.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Sub-Exporter/Sub-Exporter-0.984.tar.gz/cfa19c5f09bc08f49174856c0bfd7849/Sub-Exporter-0.984.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Data-GUID/Data-GUID-0.048.tar.gz/8709af7609214edcad1ec99ecd62f048/Data-GUID-0.048.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-CPAN-DistnameInfo/CPAN-DistnameInfo-0.12.tar.gz/06bc803c0e4fb7735ddc7282163f1cc3/CPAN-DistnameInfo-0.12.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://backpan.perl.org/authors/id/D/DA/DAGOLDEN/Metabase-Fact-0.021.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Test-Tester/Test-Tester-0.108.tar.gz/939d4fb9ad04b8b5a746cdd09040ea0b/Test-Tester-0.108.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Test-NoWarnings/Test-NoWarnings-1.04.tar.gz/682ed043f7d3e38f3dfd8745fd21c49a/Test-NoWarnings-1.04.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://search.cpan.org/CPAN/authors/id/H/HM/HMBRAND/Config-Perl-V-0.20.tgz
i=$(( $i + 1 ))

archives[$i]=http://search.cpan.org/CPAN/authors/id/D/DA/DAGOLDEN/CPAN-Testers-Report-1.999001.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://search.cpan.org/CPAN/authors/id/D/DA/DAGOLDEN/Test-Reporter-Transport-Metabase-1.999008.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Capture-Tiny/Capture-Tiny-0.18.tar.gz/74760c1f86824080ae4059d6f471a092/Capture-Tiny-0.18.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Devel-Autoflush/Devel-Autoflush-0.05.tar.gz/071061e12ce7ca4f3d62a2e329db0503/Devel-Autoflush-0.05.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-IPC-Cmd/IPC-Cmd-0.80.tar.gz/2f4c74a1c91e67dd80ef6dc5c5ebaed4/IPC-Cmd-0.80.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://ftp.tw.freebsd.org/pub/FreeBSD/ports/distfiles/CPAN-Reporter-1.2006.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Module-ScanDeps/Module-ScanDeps-1.13.tar.gz/15fe01997c556f36d1a1b22722e4bd79/Module-ScanDeps-1.13.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-PAR-Dist/PAR-Dist-0.49.tar.gz/bd852113974544f3c8c107ab4055cf8c/PAR-Dist-0.49.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Socket6/Socket6-0.23.tar.gz/2c02adb13c449d48d232bb704ddbd492/Socket6-0.23.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-IO-Socket-INET6/IO-Socket-INET6-2.69.tar.gz/12a80a5164a775294a9bf9c812fc3257/IO-Socket-INET6-2.69.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://search.cpan.org/CPAN/authors/id/R/RU/RURBAN/B-Generate-1.48.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-PadWalker/PadWalker-1.98.tar.gz/c4487293af8afd84b6739be296610e8f/PadWalker-1.98.tar.gz
i=$(( $i + 1 ))

archives[$i]=http://pkgs.fedoraproject.org/repo/pkgs/perl-Data-Alias/Data-Alias-1.16.tar.gz/f53a4f654d57671fe0bd2cdcd6974e41/Data-Alias-1.16.tar.gz
i=$(( $i + 1 ))

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys2"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw32"
then
  echo "You must be in an MINGW shell to build a mingw32 package"
  exit 5
fi

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/bld
logdir=${pkgbuilddir}/logs
srcdir=${pkgbuilddir}/${name}-${ver}

echo The source directory is ${srcdir}

srcdirname=$(basename ${srcdir})

capname=Unknown
if test "x${subsys}" == "xmingw32"
then
capname=MinGW
elif test "x${subsys}" == "xmsys2"
then
capname=MSYS
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}

if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi
archname=${arch}-msys-threads-64int
archver=$shortver

p1=${prefix:1}

do_download=1
do_unpack=1
do_patch=0
do_preconfigure=0
do_reconfigure=0
do_configure=0
do_cpan=0
do_check=0
do_install=1
do_fixinstall=0
do_pack=1
do_clean=1

if test ! "x$reposrc" = "x"
then
  srcdir="$reposrc"
  do_download=0
  do_unpack=0
  do_reconfigure=1
fi

zeroall() {
      do_download=0
      do_unpack=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --download) do_download=1 ; shift 1 ;;
    --unpack) do_unpack=1 ; shift 1 ;;
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --download-only) zeroall; do_download=1 ; shift 1 ;;
    --unpack-only) zeroall; do_unpack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_download" == "x1"
then
  rm -f ${logdir}/download.log
  cd ${pkgbuilddir}/vendor/
  for i in ${!archives[*]}
  do
    SOURCE_ARCHIVE=$(basename ${archives[$i]})
    if test ! -f ${SOURCE_ARCHIVE}
    then
      echo "Downloading ${SOURCE_ARCHIVE} ..." 2>&1 | tee ${logdir}/download.log
      wget ${archives[$i]} 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi

    if test -f ${SOURCE_ARCHIVE}.sha512
    then
      echo "Verifying sha512 sum ..." | tee -a ${logdir}/download.log
      sha512sum -c ${SOURCE_ARCHIVE}.sha512 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    else
      echo "Calculating sha512 sum ..." | tee -a ${logdir}/download.log
      sha512sum ${SOURCE_ARCHIVE} >${SOURCE_ARCHIVE}.sha512 2>>${logdir}/download.log || fail $LINENO
    fi
  done
  echo "Done downloading"
fi

if test "x$do_unpack" == "x1"
then
  rm -f ${logdir}/unpack.log

  echo Cleaning up inst and build directories | tee -a ${logdir}/unpack.log
  rm -rf ${instdir} 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Done unpacking"
fi

source ${pkgbuilddir}/vendor/modules

if test "x$do_cpan" == "x1"
then
  cd ${pkgbuilddir}/vendor
    for m in $modules; do
      if [ $m != YAML ]; then # is broken, endless EUMM cycle
        echo perl -MCPAN -e"get \"$m\""
        perl -MCPAN -e"get \"$m\""
      fi
    done
fi

if test "x$do_install" == "x1"
then
  rm -f ${logdir}/install.log
  cd ${pkgbuilddir}/vendor

  echo Installing into ${instdir} from `pwd` | tee ${logdir}/install.log

  instvdir=$instdir

  # CPAN, libXML and more
  for i in $exts
  do
    test -n ${i} || fail $LINENO
    if [ -d ${i} ]; then
      echo rm -rf ${i} 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      rm -rf ${i} 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    echo tar xvzf ${i}.tar.gz  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    if [ ! -e ${i}.tar.gz -a ! -e ${i}.tgz ]; then echo "${i}.tar.gz and ${i}.tgz does not exist"  2>&1 | tee -a ${logdir}/install.log ; fail $LINENO ; fi
    if [ -e ${i}.tar.gz ]; then aname=${i}.tar.gz; else aname=${i}.tgz; fi
    tar xvzf ${aname}  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    if [ ! -d ${i} ]; then echo "${i} not created. Aborted"  2>&1 | tee -a ${logdir}/install.log ; fail $LINENO ; fi
    echo cd ${i}  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

    cd ${i}

    if test -f ../$i.patch; then
      echo "${i}: patch -b -f -p1 ../$i.patch"  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      patch -b -f -p1 < ../$i.patch  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      find -name \*.orig -exec chmod 0444 \{\} \;  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    if [ -f Makefile.PL ]; then
      if test ! -e Makefile.PL
      then
        chmod +x Makefile.PL
      fi
      echo "${i}: perl Makefile.PL"  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      opts=
      if [ ${i#Crypt-SSLeay} != $i ]; then opts="-default -lib=/usr -live"; fi
      perl Makefile.PL $opts  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      echo "${i}: make" 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      make -j1  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      if [ $? -gt 0 ]; then
        echo "make exitcode $?. Aborted" 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
        fail $LINENO
      fi
      #XXX Some not to vendor. Most do it automatically
      if [ ${i#ExtUtils-ParseXS} != $i -o ${i#ExtUtils-CBuilder}  != $i ]; then
        echo "${i}: make install DESTDIR=${instvdir}"  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
        make -j1 install DESTDIR=${instvdir}  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      else
        echo "${i}: make install_vendor DESTDIR=${instvdir}"  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
        make -j1 install_vendor DESTDIR=${instvdir}  2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      fi
    else
      if test ! -e Build.PL
      then
        chmod +x Build.PL
      fi
      echo "${i}: perl Build.PL --destdir=${instvdir} --installdirs=vendor" 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      perl Build.PL --destdir=${instvdir} --installdirs=vendor 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      echo "${i}: Build" 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      ./Build 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      echo "${i}: Build install" 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
      ./Build install 2>&1 | tee -a ${logdir}/install.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    cd ..
    rm -rf ./${i}
  done
  find ${instvdir} -name .packlist -exec sed -i -e"s,${instdir},," \{\} \;

  #for taint
  chmod -R g-w ${instvdir}${prefix}/lib/perl5/vendor_perl/

  /mingw/bin/split-debug.py $(cd ${instdir}; pwd -W) --target=$buildalias && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi





  if test ! "x${scriptfiles}" == "x"
  then
    mkdir -p ${instdir}/var/lib/mingw-get/scripts
  fi
  for scriptfile in ${scriptfiles}
  do
    if test ! "x${scriptfile}" == "x"
    then
      cp ${scriptfile} ${instdir}/var/lib/mingw-get/scripts/$(echo $(basename ${scriptfile}) | sed -e 's/'${badname}'-/'${badname}'-'${ver}'-'${rev}'-/')
    fi
  done
fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  cd ${instdir}
  echo Packing | tee -a ${logdir}/pack.log

  pkgtmp=${ver}-${rev}-${subsys}_${arch}

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bin.tar.xz \
--exclude=*.dbg \
$p1/bin \
$p1/lib \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-dbg.tar.xz \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/B/Generate/Generate.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Compress/Bzip2/Bzip2.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Crypt/SSLeay/SSLeay.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Data/Alias/Alias.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Data/UUID/UUID.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Digest/SHA/SHA.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/HTML/Parser/Parser.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/JSON/XS/XS.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Net/DNS/DNS.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/PadWalker/PadWalker.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Proc/ProcessTable/ProcessTable.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Socket6/Socket6.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Term/ReadKey/ReadKey.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/Term/ReadLine/Gnu/Gnu.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/XML/LibXML/LibXML.dll.dbg \
$p1/lib/perl5/vendor_perl/${ver}*/${archname}/auto/XML/Parser/Expat/Expat.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-doc.tar.xz \
$p1/share/man \
 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir}

  BLD="\
pkgbuild.sh \
$(if test -d patches; then echo patches; fi) \
vendor \
"

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bld.tar.xz --exclude=vendor/*.gz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-src.tar.xz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

fi

if test "x$do_clean" == "x1"
then

cd ${pkgbuilddir}
  rm -rf ${blddir} ${instdir}
  if test -d "${srcdir}" -a ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    rm -rf ${srcdir}
  fi
fi

echo "Done"
exit 0
