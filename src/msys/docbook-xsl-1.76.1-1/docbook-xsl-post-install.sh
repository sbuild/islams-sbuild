#!/bin/sh
mkdir -p /etc/xml

xmlcatalog --noout --add delegateURI "http://docbook.sourceforge.net/release/xsl/" "./docbook-xsl.xml" /etc/xml/catalog
xmlcatalog --noout --add delegateSystem "http://docbook.sourceforge.net/release/xsl/" "./docbook-xsl.xml" /etc/xml/catalog

if ! test -f /etc/xml/docbook-xsl.xml
then
  xmlcatalog --noout --create /etc/xml/docbook-xsl.xml
fi
xmlcatalog --noout --add delegateURI "http://docbook.sourceforge.net/release/xsl/" "/usr/share/xml/docbook/stylesheet/docbook-xsl/catalog.xml" /etc/xml/docbook-xsl.xml
xmlcatalog --noout --add delegateSystem "http://docbook.sourceforge.net/release/xsl/" "/usr/share/xml/docbook/stylesheet/docbook-xsl/catalog.xml" /etc/xml/docbook-xsl.xml
