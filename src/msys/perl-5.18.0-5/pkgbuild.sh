#! /bin/bash

name=perl
badname=perl
ver=5.18.0
ver1=5
ver2=5.18
dllver=0
rev=5
shortver=$(echo "${ver}${rev}" | sed 's/-.*//')
libperl="msys-perl${ver2/./_}.dll"
subsys=msys2
srcname=${name}-${ver}
prefix=/usr
CFLAGS="-g -O2"
docdir=share/doc/${badname}/${ver}
SOURCE_ARCHIVE_FORMAT=.tar.bz2
SOURCE_ARCHIVE=${name}-${ver}${SOURCE_ARCHIVE_FORMAT}
preconf_patches=
docfiles="
README \
Changes*
INSTALL"
licfiles="Artistic Copying"
rebasebase=0x58000000

#if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=/etc --localstatedir=/var"; fi
if test "x${install_dirs}" == "x"; then install_dirs="--datadir=\${prefix}/share --infodir=\${prefix}/share/info --mandir=\${prefix}/share/man --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=\${prefix}/etc --localstatedir=/var"; fi


reposrc=""

url=http://www.cpan.org/src/5.0/${SOURCE_ARCHIVE}

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys2"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw32"
then
  echo "You must be in an MINGW shell to build a mingw32 package"
  exit 5
fi

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/bld
logdir=${pkgbuilddir}/logs
srcdir=${pkgbuilddir}/${name}-${ver}

echo The source directory is ${srcdir}

srcdirname=$(basename ${srcdir})

capname=Unknown
if test "x${subsys}" == "xmingw32"
then
capname=MinGW
elif test "x${subsys}" == "xmsys2"
then
capname=MSYS
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}

if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi
archname=${arch}-msys-threads-64int
archver=$shortver

p1=${prefix:1}

do_download=1
do_unpack=1
do_patch=1
do_preconfigure=0
do_reconfigure=0
do_configure=1
do_make=1
do_check=0
do_install=1
do_fixinstall=1
do_pack=1
do_clean=1

if test ! "x$reposrc" = "x"
then
  srcdir="$reposrc"
  do_download=0
  do_unpack=0
  do_reconfigure=1
fi

zeroall() {
      do_download=0
      do_unpack=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --download) do_download=1 ; shift 1 ;;
    --unpack) do_unpack=1 ; shift 1 ;;
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --download-only) zeroall; do_download=1 ; shift 1 ;;
    --unpack-only) zeroall; do_unpack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_download" == "x1"
then
  rm -f ${logdir}/download.log
  test -f ${pkgbuilddir}/${SOURCE_ARCHIVE} || {
    echo "Downloading ${SOURCE_ARCHIVE} ..." 2>&1 | tee ${logdir}/download.log
    wget -P ${pkgbuilddir} $url 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    b=$(basename $url)
    if test ! "x$b" = "x${SOURCE_ARCHIVE}"
    then
      mv -f $b ${SOURCE_ARCHIVE}
    fi
  }

  if test -f ${SOURCE_ARCHIVE}.sha512
  then
    echo "Verifying sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum -c ${SOURCE_ARCHIVE}.sha512 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  else
    echo "Calculating sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum ${SOURCE_ARCHIVE} >${SOURCE_ARCHIVE}.sha512 2>>${logdir}/download.log || fail $LINENO
  fi
  echo "Done downloading"
fi

if test "x$do_unpack" == "x1"
then
  rm -f ${logdir}/unpack.log
  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/unpack.log
    rm -rf ${srcdir}/* 2>&1 | tee -a ${logdir}/unpack.log
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee ${logdir}/unpack.log
    exit 2
  fi

  echo Cleaning up inst and build directories | tee -a ${logdir}/unpack.log
  rm -rf ${blddir} ${instdir} 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Unpacking $SOURCE_ARCHIVE in `pwd`" | tee -a ${logdir}/unpack.log
  case "$SOURCE_ARCHIVE" in
  *.tar.bz2 ) tar xjf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.gz  ) tar xzf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.zip     ) unzip -q $SOURCE_ARCHIVE  2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.lzma) tar --lzma -xf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.xz) tar xJf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  esac
  if test $(ls ${srcdir} -l | wc -l) -ge 2
  then
    echo "Unpacked into ${srcdir} successfully!" | tee -a ${logdir}/unpack.log
  else
    echo "${srcdir} is empty - did not unpack into it?" | tee -a ${logdir}/unpack.log
    exit 3
  fi
  echo "Done unpacking"
fi

patch_list=
if test "x$do_patch" == "x1"
then
  rm -f ${logdir}/patch.log
  echo cd ${srcdir}
  cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test ! "x${patchfiles}" == "x"
  then
    echo "Patching in `pwd` from patchfiles ${patchfiles}" | tee -a ${logdir}/patch.log
  fi
  for patchfile in ${patchfiles}
  do
    if test ! "x${patchfiles}" == "x"
    then
      echo "Applying ${patchfile}" | tee -a ${logdir}/patch.log
      patch -p1 -N -i ${patchfile} 2>&1 | tee -a ${logdir}/patch.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    patch_list="$patch_list $patchfile"
  done
  find . -name \*.orig -exec chmod 0444 \{\} \;
  find . -name \*.orig -print | sed 's,.orig,,' | xargs chmod u+rw
  echo "Done patching"
fi

if test "x$do_reconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Reconfiguring in `pwd` | tee -a ${logdir}/reconfigure.log
  autoreconf -fi 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
elif test "x$do_preconfigure" == "x1"
then
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Patching in `pwd` from ${preconf_patches} | tee -a ${logdir}/reconfigure.log
  for patchfile in ${pkgbuilddir}/preconf-patches/*${subsys}.patch
  do
    patch -p0 -i ${patchfile} 2>&1 | tee -a ${logdir}/reconfigure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  done
fi

mkdir -p ${blddir} || fail $LINENO

if test "x$do_configure" == "x1"
then
  rm -f ${logdir}/configure.log
  cd ${blddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  eval "install_dirs=\"$install_dirs\""

  build_host_target=
  if test ! "x${buildalias}" == "x"
  then
    build_host_target+=" --build=${buildalias}"
  fi
  if test ! "x${hostalias}" == "x"
  then
    build_host_target+=" --host=${hostalias}"
  fi
  if test ! "x${targetalias}" == "x"
  then
    build_host_target+=" --target=${targetalias}"
  fi

  cat > ${blddir}/Policy.sh <<EOF
#!/bin/sh
# Auto-generated msys Policy file for ${name}-${ver}-${rev}
# ${archname}

# Where things will be.
prefix='${prefix}'
siteprefix='${prefix}'
vendorprefix='${prefix}'
vendorman1dir='${prefix}/share/man/man1'
vendorman3dir='${prefix}/share/man/man3'
html1dir='${docdir}/html/html1'
html3dir='${docdir}/html/html3'
sitehtml1dir='${docdir}/html/html1'
sitehtml3dir='${docdir}/html/html3'
vendorhtml1dir='${docdir}/html/html1'
vendorhtml3dir='${docdir}/html/html3'

archname='${archname}'
privlib='${prefix}/lib/perl5/${archver}'
sitelib='${prefix}/lib/perl5/site_perl/${archver}'
vendorlib='${prefix}/lib/perl5/vendor_perl/${archver}'
archlib='${prefix}/lib/perl5/${archver}/${archname}'
sitearch='${prefix}/lib/perl5/site_perl/${archver}/${archname}'
vendorarch='${prefix}/lib/perl5/vendor_perl/${archver}/${archname}'

sitebin='/usr/local/bin'
sitescript='/usr/local/bin'

# The maintainer.
mydomain='.no-ip.info'
myhostname='lrn'
perladmin='lrn1986@gmail.com'
cf_by='LRN'
cf_email='lrn1986@gmail.com'

EOF

  CONF_ARGS="-de -Dlibperl=$libperl -Dcc=gcc -Dld=g++ -Darchname=${archname}"
  test -s ../Policy.local && cat ../Policy.local >> Policy.sh
  test -s ../conf_args.local && . ../conf_args.local

  CONF_ARGS="$CONF_ARGS -Dmksymlinks"
  CONF_ARGS="$CONF_ARGS -Dusethreads"

  # -g for strip_debug
  # tricky: only -DEBUGGING=-g lets us generate debuginfo without -DDEBUGGING
  # fedora fell into that trap
  CONF_ARGS="$CONF_ARGS -Accflags=-g"

  echo Configuring in `pwd` | tee -a ${logdir}/configure.log
  ../$(basename ${srcdir})/Configure $CONF_ARGS 2>&1 | tee -a ${logdir}/configure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  #../$(basename ${srcdir})/configure \
  #  --prefix=${prefix} \
  #  --program-suffix=-${ver2} \
  #  ${build_host_target} \
  #  ${install_dirs} \
  #  2>&1 | tee -a ${logdir}/configure.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  #
fi

if test "x$do_make" == "x1"
then
  rm -f ${logdir}/make.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Making
  make -j1 all libperl.a 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

#  echo "./perl.exe" > rebase.lst
#  find -type f -name \*.exe | grep -v "perl.exe" >> rebase.lst
#  peflags -t1 -T rebase.lst
#
#  find -type f -name \*.dll > rebase.lst
#  peflags -d1 -T $dir/rebase.lst
#  cat $dir/rebase.lst | rebase -v -b $rebasebase -T -

  find lib -name \*.dll -exec chmod +rx \{\} \;
fi

if test "x$do_install" == "x1"
then
  rm -f ${logdir}/install.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Installing into ${instdir} from `pwd` | tee ${logdir}/install.log


  make install DESTDIR=${instdir} -j1 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  if test ! "x${scriptfiles}" == "x"
  then
    mkdir -p ${instdir}/var/lib/mingw-get/scripts
  fi
  for scriptfile in ${scriptfiles}
  do
    if test ! "x${scriptfile}" == "x"
    then
      cp ${scriptfile} ${instdir}/var/lib/mingw-get/scripts/$(echo $(basename ${scriptfile}) | sed -e 's/'${badname}'-/'${badname}'-'${ver}'-'${rev}'-/')
    fi
  done
fi

if test "x$do_check" == "x1"
then
  rm -f ${logdir}/test.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Testing" | tee -a ${logdir}/test.log
  make -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi


if test "x$do_fixinstall" == "x1"
then
  rm -f ${logdir}/fixinstall.log
  echo Fixing the installation | tee ${logdir}/fixinstall.log
  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  for f in ${docfiles} ${licfiles}
  do
    if test -f ${instdir}${prefix}/${docdir}/$(basename ${f})
    then
      cp -r ${f} ${instdir}${prefix}/${docdir}/${f}
    else
      cp -r ${f} ${instdir}${prefix}/${docdir}/$(basename ${f})
    fi
  done
  mkdir -p ${instdir}${prefix}/share/doc/${capname} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cp ${pkgbuilddir}/${subsys}-${badname}.RELEASE_NOTES \
    ${instdir}${prefix}/share/doc/${capname}/${badname}-${ver}-${rev}-${subsys}.RELEASE_NOTES.txt 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi


  if [ ! -e ${instdir}${prefix}/lib/perl5/$archver/$archname/auto/Win32CORE/Win32CORE.a ]
  then
	cp ${blddir}/lib/auto/Win32CORE/Win32CORE.a ${instdir}${prefix}/lib/perl5/$archver/$archname/auto/Win32CORE/Win32CORE.a
  fi
  if [ ! -e ${instdir}${prefix}/lib/perl5/$archver/$archname/auto/Cwd/Cwd.a ]
  then
	cp ${blddir}/lib/auto/Cwd/Cwd.a ${instdir}${prefix}/lib/perl5/$ver/$archname/auto/Cwd/Cwd.a
  fi
  rm ${instdir}${prefix}/lib/perl5/$archver/$archname/auto/Cwd/Cwd.dll
  rm ${blddir}/lib/auto/Cwd/Cwd.dll
  mkdir -p ${instdir}${prefix}/lib/perl5/site_perl/$archver


  find ${instdir} -name .packlist -exec sed -i -e"s,${instdir},," \{\} \;
  #faclfixdir "${instdir}"
  chmod -R g-w ${instdir}${prefix}/bin ${instdir}${prefix}/lib/perl5
  chmod +x ${instdir}${prefix}/bin

  echo "symlink bin/msys-perl dll to CORE/"
  ln -s ../../../../bin/${libperl} ${instdir}${prefix}/lib/perl5/${archver}/${archname}/CORE

  echo "symlink libperl[.dll].a to /usr/lib/"
  ln -s perl5/${archver}/${archname}/CORE/libperl.a ${instdir}${prefix}/lib/libperl.a
  ln -s perl5/${archver}/${archname}/CORE/libperl.dll.a ${instdir}${prefix}/lib/libperl.dll.a

  cd ${instdir}${prefix}/bin
  chmod +x *
  if [ -e scandeps.pl ]; then mv scandeps.pl scandeps; fi
  if [ -e "a2p${ver}.exe" -a ! -e "a2p.exe" ]; then # installperl -v (usedevel)
	for f in $(ls | grep -v "${archver}"); do
	    mv $f $f${ver}
	done
	for x in *$ver; do r=$(basename $x $ver); ln -s $x $r; done
	for x in *$ver.exe; do r=$(basename $x $ver); ln -s $x $r; done
  fi

  cd ${pkgbuilddir}
  /usr/bin/install -m 755 perlrebase ${instdir}${prefix}/bin
  #pod2man perlrebase.pod | gzip -c > ${instdir}${prefix}/share/man/man1/perlrebase.1.gz
  popd

  /mingw/bin/split-debug.py $(cd ${instdir}; pwd -W) --target=$buildalias && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi




fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  cd ${instdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  echo Packing | tee -a ${logdir}/pack.log

  pkgtmp=${ver}-${rev}-${subsys}_${arch}

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bin.tar.xz \
--exclude=*.dbg \
$p1/bin/a2p.exe \
$p1/bin/c2ph \
$p1/bin/config_data \
$p1/bin/corelist \
$p1/bin/cpan \
$p1/bin/cpan2dist \
$p1/bin/cpanp \
$p1/bin/cpanp-run-perl \
$p1/bin/enc2xs \
$p1/bin/find2perl \
$p1/bin/h2ph \
$p1/bin/h2xs \
$p1/bin/instmodsh \
$p1/bin/json_pp \
$p1/bin/libnetcfg \
$p1/bin/${libperl} \
$p1/bin/perl.exe \
$p1/bin/perl5.18.0.exe \
$p1/bin/perlbug \
$p1/bin/perldoc \
$p1/bin/perlivp \
$p1/bin/perlrebase \
$p1/bin/perlthanks \
$p1/bin/piconv \
$p1/bin/pl2pm \
$p1/bin/pod2html \
$p1/bin/pod2latex \
$p1/bin/pod2man \
$p1/bin/pod2text \
$p1/bin/pod2usage \
$p1/bin/podchecker \
$p1/bin/podselect \
$p1/bin/prove \
$p1/bin/psed \
$p1/bin/pstruct \
$p1/bin/ptar \
$p1/bin/ptardiff \
$p1/bin/ptargrep \
$p1/bin/s2p \
$p1/bin/shasum \
$p1/bin/splain \
$p1/bin/xsubpp \
$p1/bin/zipdetails \
$p1/lib/perl5/${archver} \
var \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-dbg.tar.xz \
$p1/bin/*.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/arybase/arybase.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/attributes/attributes.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/B/B.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Compress/Raw/Bzip2/Bzip2.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Compress/Raw/Zlib/Zlib.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Data/Dumper/Dumper.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Devel/Peek/Peek.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Devel/PPPort/PPPort.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Digest/MD5/MD5.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Digest/SHA/SHA.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/Byte/Byte.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/CN/CN.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/EBCDIC/EBCDIC.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/Encode.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/JP/JP.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/KR/KR.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/Symbol/Symbol.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/TW/TW.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Encode/Unicode/Unicode.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Fcntl/Fcntl.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/File/DosGlob/DosGlob.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/File/Glob/Glob.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Filter/Util/Call/Call.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/GDBM_File/GDBM_File.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Hash/Util/FieldHash/FieldHash.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Hash/Util/Util.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/I18N/Langinfo/Langinfo.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/IO/IO.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/IPC/SysV/SysV.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/List/Util/Util.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Math/BigInt/FastCalc/FastCalc.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/MIME/Base64/Base64.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/mro/mro.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Opcode/Opcode.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/PerlIO/encoding/encoding.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/PerlIO/mmap/mmap.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/PerlIO/scalar/scalar.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/PerlIO/via/via.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/POSIX/POSIX.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/re/re.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/SDBM_File/SDBM_File.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Socket/Socket.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Storable/Storable.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Sys/Hostname/Hostname.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Sys/Syslog/Syslog.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Text/Soundex/Soundex.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/threads/shared/shared.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/threads/threads.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Tie/Hash/NamedCapture/NamedCapture.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Time/HiRes/HiRes.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Time/Piece/Piece.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Unicode/Collate/Collate.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Unicode/Normalize/Normalize.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Win32/Win32.dll.dbg \
$p1/lib/perl5/${archver}/${archname}/auto/Win32API/File/File.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}_vendor-${pkgtmp}-bin.tar.xz \
--exclude=*.dbg \
$p1/lib/perl5/site_perl \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-doc.tar.xz \
$p1/share/man \
$p1/${docdir} \
$p1/share/doc/${capname} \
 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  BLD="\
pkgbuild.sh \
${subsys}-${badname}.RELEASE_NOTES \
$(if test -d patches; then echo patches; fi) \
${scriptfiles} \
${SOURCE_ARCHIVE}.sha512 \
"

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bld.tar.xz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-src.tar.xz ${SOURCE_ARCHIVE} $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_clean" == "x1"
then

cd ${pkgbuilddir}
  rm -rf ${blddir} ${instdir}
  if test -d "${srcdir}" -a ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    rm -rf ${srcdir}
  fi
fi

echo "Done"
exit 0
