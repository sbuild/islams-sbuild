#! /bin/bash

name=tcp-wrappers
badname=tcp-wrappers
ver=7.6
ver1=7
ver2=7.6
dllver=0
rev=2
subsys=msys2
srcname=${name}-${ver}
prefix=/usr
CFLAGS="-g -O2"
docdir=share/doc/${badname}/${ver}
SOURCE_ARCHIVE_FORMAT=.tar.gz
SOURCE_ARCHIVE=tcp_wrappers_${ver}${SOURCE_ARCHIVE_FORMAT}
preconf_patches=
docfiles="BLURB CHANGES README.IRIX README.NIS CYGWIN-PATCHES/README.Debian"
licfiles="DISCLAIMER"

if test "x${install_dirs}" == "x"; then install_dirs="--datarootdir=\${prefix}/share --docdir=\${prefix}/\${docdir} --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=/etc --localstatedir=/var"; fi
#if test "x${install_dirs}" == "x"; then install_dirs="--datadir=\${prefix}/share --infodir=\${prefix}/share/info --mandir=\${prefix}/share/man --sbindir=\${prefix}/sbin --libexecdir=\${prefix}/libexec --sysconfdir=\${prefix}/etc --localstatedir=/var"; fi


reposrc=""

url=ftp://ftp.porcupine.org/pub/security/${SOURCE_ARCHIVE}

if test ! "$MSYSTEM" == "MSYS" -a "$subsys" == "msys2"
then
  echo "You must be in an MSYS shell to build a msys package"
  exit 4
fi

if test ! "$MSYSTEM" == "MINGW32" -a "$subsys" == "mingw32"
then
  echo "You must be in an MINGW shell to build a mingw32 package"
  exit 5
fi

pkgbuilddir=`pwd` || fail $LINENO
echo Acting from the directory ${pkgbuilddir}

patchfiles=`for i in ${pkgbuilddir}/patches/*.${subsys}.patch ${pkgbuilddir}/patches/*.all.patch; do if test $(expr index "$i" "*") -ge 1; then : ;else echo $i; fi; done | sort`
x=${pkgbuilddir}/${badname}
scriptfiles=`for i in $x-pre-install.sh $x-post-install.sh $x-pre-remove.sh $x-post-remove.sh; do if test ! -f $i; then : ;else echo $(basename $i); fi; done | sort`

instdir=${pkgbuilddir}/inst
blddir=${pkgbuilddir}/tcp_wrappers_${ver}
logdir=${pkgbuilddir}/logs
srcdir=${pkgbuilddir}/tcp_wrappers_${ver}

echo The source directory is ${srcdir}

srcdirname=$(basename ${srcdir})

capname=Unknown
if test "x${subsys}" == "xmingw32"
then
capname=MinGW
elif test "x${subsys}" == "xmsys2"
then
capname=MSYS
fi

_sysver=$(uname -r)
export SYSVER=${_sysver%%\(*}

if test "x$arch" == "x" -a ! "x$buildalias" == "x"
then
  arch_index=$(expr index $buildalias -)
  arch_up_to=$((arch_index - 1))
  arch=${buildalias::${arch_up_to}}
fi
if test "x$arch" == "x"
then
  echo "Can't determine architecture."
  echo "Set 'arch' or 'buildalias' environment variable."
  exit 1
fi

p1=${prefix:1}

do_download=1
do_unpack=1
do_patch=1
do_preconfigure=0
do_reconfigure=0
do_configure=0
do_make=1
do_check=0
do_install=0
do_fixinstall=1
do_pack=1
do_clean=1

if test ! "x$reposrc" = "x"
then
  srcdir="$reposrc"
  do_download=0
  do_unpack=0
  do_reconfigure=1
fi

zeroall() {
      do_download=0
      do_unpack=0
      do_patch=0
      do_preconfigure=0
      do_reconfigure=0
      do_configure=0
      do_make=0
      do_check=0
      do_install=0
      do_fixinstall=0
      do_pack=0
      do_clean=0
}

while [ $# -gt 0 ]
do
  case $1 in
    --download) do_download=1 ; shift 1 ;;
    --unpack) do_unpack=1 ; shift 1 ;;
    --patch) do_patch=1 ; shift 1 ;;
    --preconfigure) do_preconfigure=1 ; shift 1 ;;
    --reconfigure) do_reconfigure=1 ; shift 1 ;;
    --configure) do_configure=1 ; shift 1 ;;
    --make) do_make=1 ; shift 1 ;;
    --check) do_check=1 ; shift 1 ;;
    --install) do_install=1 ; shift 1 ;;
    --fixinstall) do_fixinstall=1 ; shift 1 ;;
    --pack) do_pack=1 ; shift 1 ;;
    --download-only) zeroall; do_download=1 ; shift 1 ;;
    --unpack-only) zeroall; do_unpack=1 ; shift 1 ;;
    --patch-only) zeroall; do_patch=1 ; shift 1 ;;
    --preconfigure-only) zeroall; do_preconfigure=1 ; shift 1 ;;
    --reconfigure-only) zeroall; do_reconfigure=1 ; shift 1 ;;
    --configure-only) zeroall; do_configure=1 ; shift 1 ;;
    --make-only) zeroall; do_make=1 ; shift 1 ;;
    --check-only) zeroall; do_check=1 ; shift 1 ;;
    --install-only) zeroall; do_install=1 ; shift 1 ;;
    --fixinstall-only) zeroall; do_fixinstall=1 ; shift 1 ;;
    --pack-only) zeroall; do_pack=1 ; shift 1 ;;
    --dont-clean) do_clean=0 ; shift 1 ;;
    *) shift 1 ;;
  esac
done


fail() {
  echo "failure at line $1"
  exit 1
}

if test ! -d ${logdir}
then
  mkdir -p ${logdir} || fail $LINENO
fi

if test "x$do_download" == "x1"
then
  rm -f ${logdir}/download.log
  test -f ${pkgbuilddir}/${SOURCE_ARCHIVE} || {
    echo "Downloading ${SOURCE_ARCHIVE} ..." 2>&1 | tee ${logdir}/download.log
    wget -P ${pkgbuilddir} $url 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    b=$(basename $url)
    if test ! "x$b" = "x${SOURCE_ARCHIVE}"
    then
      mv -f $b ${SOURCE_ARCHIVE}
    fi
  }

  if test -f ${SOURCE_ARCHIVE}.sha512
  then
    echo "Verifying sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum -c ${SOURCE_ARCHIVE}.sha512 2>&1 | tee -a ${logdir}/download.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  else
    echo "Calculating sha512 sum ..." | tee -a ${logdir}/download.log
    sha512sum ${SOURCE_ARCHIVE} >${SOURCE_ARCHIVE}.sha512 2>>${logdir}/download.log || fail $LINENO
  fi
  echo "Done downloading"
fi

if test "x$do_unpack" == "x1"
then
  rm -f ${logdir}/unpack.log
  if test ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    echo "Deleting ${srcdir} contents" | tee ${logdir}/unpack.log
    rm -rf ${srcdir}/* 2>&1 | tee -a ${logdir}/unpack.log
  else
    echo "I think it is unsafe to delete ${srcdir}" | tee ${logdir}/unpack.log
    exit 2
  fi

  echo Cleaning up inst and build directories | tee -a ${logdir}/unpack.log
  rm -rf ${blddir} ${instdir} 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Unpacking $SOURCE_ARCHIVE in `pwd`" | tee -a ${logdir}/unpack.log
  case "$SOURCE_ARCHIVE" in
  *.tar.bz2 ) tar xjf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.gz  ) tar xzf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.zip     ) unzip -q $SOURCE_ARCHIVE  2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.lzma) tar --lzma -xf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  *.tar.xz) tar xJf $SOURCE_ARCHIVE 2>&1 | tee -a ${logdir}/unpack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi;;
  esac
  if test $(ls ${srcdir} -l | wc -l) -ge 2
  then
    echo "Unpacked into ${srcdir} successfully!" | tee -a ${logdir}/unpack.log
  else
    echo "${srcdir} is empty - did not unpack into it?" | tee -a ${logdir}/unpack.log
    exit 3
  fi
  echo "Done unpacking"
fi

patch_list=
if test "x$do_patch" == "x1"
then
  rm -f ${logdir}/patch.log
  echo cd ${srcdir}
  cd ${srcdir} && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  if test ! "x${patchfiles}" == "x"
  then
    echo "Patching in `pwd` from patchfiles ${patchfiles}" | tee -a ${logdir}/patch.log
  fi
  for patchfile in ${patchfiles}
  do
    if test ! "x${patchfiles}" == "x"
    then
      echo "Applying ${patchfile}" | tee -a ${logdir}/patch.log
      patch -p1 -i ${patchfile} 2>&1 | tee -a ${logdir}/patch.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
    fi
    patch_list="$patch_list $patchfile"
  done
  echo "Done patching"
fi

mkdir -p ${blddir} || fail $LINENO

if test "x$do_make" == "x1"
then
  rm -f ${logdir}/make.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo Making
  make $MAKEFLAGS -j1 cygwin CC=gcc 2>&1 | tee -a ${logdir}/make.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_check" == "x1"
then
  rm -f ${logdir}/test.log
  cd ${blddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  echo "Testing" | tee -a ${logdir}/test.log
  make -k check 2>&1 | tee -a ${logdir}/test.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi


if test "x$do_fixinstall" == "x1"
then
  rm -f ${logdir}/fixinstall.log
  echo Fixing the installation | tee ${logdir}/fixinstall.log
  mkdir -p ${instdir}${prefix}/share/doc/${badname}/${ver} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cd ${srcdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  for f in ${docfiles} ${licfiles}
  do
    if test -f ${instdir}${prefix}/${docdir}/$(basename ${f})
    then
      cp -r ${f} ${instdir}${prefix}/${docdir}/${f}
    else
      cp -r ${f} ${instdir}${prefix}/${docdir}/$(basename ${f})
    fi
  done
  mkdir -p ${instdir}${prefix}/share/doc/${capname} 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  cp ${pkgbuilddir}/${subsys}-${badname}.RELEASE_NOTES \
    ${instdir}${prefix}/share/doc/${capname}/${badname}-${ver}-${rev}-${subsys}.RELEASE_NOTES.txt 2>&1 | tee -a ${logdir}/fixinstall.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  /usr/bin/install -d ${instdir}${prefix}/{s,}bin ${instdir}${prefix}/lib ${instdir}${prefix}/include ${instdir}${prefix}/share/man/man3 ${instdir}${prefix}/share/man/man5 ${instdir}${prefix}/share/man/man8 ${instdir}/etc/defaults/etc
  /usr/bin/install -t ${instdir}${prefix}/bin ${blddir}/shared/*.dll
  /usr/bin/install -t ${instdir}${prefix}/sbin ${blddir}/*.exe
  /usr/bin/install -t ${instdir}${prefix}/lib ${blddir}/shared/*.a ${blddir}/*.a
  /usr/bin/install -t ${instdir}${prefix}/include ${blddir}/tcpd.h
  /usr/bin/install -t ${instdir}${prefix}/include ${blddir}/tcpd.h
  /usr/bin/install -t ${instdir}${prefix}/share/man/man3 ${blddir}/*.3
  /usr/bin/install -t ${instdir}${prefix}/share/man/man5 ${blddir}/*.5
  /usr/bin/install -t ${instdir}${prefix}/share/man/man8 ${blddir}/*.8
  /usr/bin/install -t ${instdir}/etc/defaults/etc ${blddir}/CYGWIN-PATCHES/hosts.*

  /mingw/bin/laremove.py $(cd ${instdir}; pwd -W) && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  /mingw/bin/split-debug.py $(cd ${instdir}; pwd -W) --target=$buildalias && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi




fi

if test "x$do_pack" == "x1"
then
  rm -f ${logdir}/pack.log
  cd ${instdir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
  echo Packing | tee -a ${logdir}/pack.log

  pkgtmp=${ver}-${rev}-${subsys}_${arch}

  tar cv --xz -f ../${badname}-${pkgtmp}-bin.tar.xz \
$p1/sbin/*.exe \
etc \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ../${badname}-${pkgtmp}-dbg.tar.xz \
$p1/sbin/*.exe.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ../libwrap0-${pkgtmp}-dll.tar.xz \
$p1/bin/*.dll \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ../libwrap0-${pkgtmp}-dbg.tar.xz \
$p1/bin/*.dll.dbg \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ../libwrap-${pkgtmp}-dev.tar.xz \
$p1/include \
$p1/lib \
  2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-doc.tar.xz \
$p1/share/man \
$p1/${docdir} \
$p1/share/doc/${capname} \
 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  cd ${pkgbuilddir}  && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  BLD="\
pkgbuild.sh \
${subsys}-${badname}.RELEASE_NOTES \
$(if test -d patches; then echo patches; fi) \
${scriptfiles} \
${SOURCE_ARCHIVE}.sha512 \
"

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-bld.tar.xz $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi

  tar cv --xz -f ${pkgbuilddir}/${badname}-${pkgtmp}-src.tar.xz ${SOURCE_ARCHIVE} $BLD 2>&1 | tee -a ${logdir}/pack.log && if ! test "x${PIPESTATUS[0]}" == "x0"; then fail $LINENO; fi
fi

if test "x$do_clean" == "x1"
then

cd ${pkgbuilddir}
  rm -rf ${blddir} ${instdir}
  if test -d "${srcdir}" -a ! "x${srcdir}" == "x/" -a ! "x${srcdir}" == "x" -a ! "x${srcdir}" == "x${pkgbuilddir}"
  then
    rm -rf ${srcdir}
  fi
fi

echo "Done"
exit 0
