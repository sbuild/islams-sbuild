#!/bin/sh
mkdir -p /etc/xml

xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/" "./docbook-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/" "./docbook-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook" "./docbook-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML" "./docbook-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD XML Exchange Table Model 19990315" "./docbook-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook" "./docbook-xml.xml" /etc/xml/catalog
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook CALS Table Model" "./docbook-xml.xml" /etc/xml/catalog

if ! test -f /etc/xml/docbook-xml.xml
then
  xmlcatalog --noout --create /etc/xml/docbook-xml.xml
fi
xmlcatalog --noout --add delegateSystem "http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegateSystem "http://docbook.org/xml/4.2/docbookx.dtd" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook Information Pool V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook Additional General Entities V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook Notations V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ENTITIES DocBook Character Entities V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook XML HTML Tables V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook XML V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//ELEMENTS DocBook Document Hierarchy V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
xmlcatalog --noout --add delegatePublic "-//OASIS//DTD DocBook CALS Table Model V4.2//EN" "/usr/share/xml/docbook/schema/dtd/4.2/catalog.xml" /etc/xml/docbook-xml.xml
