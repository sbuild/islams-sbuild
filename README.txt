﻿S[mart|tupid] Build

Semi-automatically assembles a MinGW/MSys2 installation, builds lots of extra packages.

Instructions:

1) Download sbuild, unpack into sbuild directory. Make sure you have more than 6GiB of free space.
   ***IMPORTANT*** sbuild absolute directory name should be as SHORT as possible. "c:/sb" or something like that.
   No paths with spaces, and CERTAINLY no long names! This should be less of a problem now, since gcc is cross-compiled, but it's still a sound advice.
2) Install latest Python 2.7.x
   Pick an installation directory that has no spaces in it. I.e. "C:\Python27", not "C:\Program Files (x86)\Python".
   Make sure you associate .py files with that Python installation (or use command line to invoke it directly)
   Use 32-bit version of Python (i.e x86, not x86_64), because compiler suite used by sbuild at the moment is not 64-bit-capable.
3.1) Install win32api Python package from http://sourceforge.net/projects/pywin32/files/pywin32/ (pick the latest version for python 2.7; pick x86, not amd64)
4) Run windows shell (cmd.exe) as Administrator (Start->Accessoires->Command prompt (right click, run as Administrator)).
   Issue the following commands:
   cd /D "c:\sb"
   c:\python27\python.exe buildall.py >build.log 2>&1
   (replace "c:\python27\" with the path to Python 2.7 on your machine, and "c:\sb" with the path to sbuild directory).
   Wait for it to complete.
   You can watch its progress by looking at the .log file in sbuild directory
5) If something fails, examine the log, fix the problem, then re-run buildall.py like this:
   buildall.py --start-at-action=<action that failed>
   replace "<action that failed>" with the name of the action that failed the list time you ran buildall.py (see `buildall.py --list-actions' for the list of default actions; or just read the buildall.py file)
   So, if you've failed to unpack basic msys installation, then you can re-attempt that with:
   buildall.py --start-at-action=unpack_msys_base
   If you've failed to build gtk+-2.0-2.24.20-1, re-attempt that by running:
   buildall.py --start-at-action=build_mingw_packages --mingw-start-from=gtk+-2.0-2.24.20-1

What sbuild does:
1) Downloads and unpacks MSYS2 packages that were cross-compiled from Debian (msys2 core, bash, sed, patch, coreutils et cetera).
   There's a separate branch with the scripts for cross-compilation, you can use them.
   Sbuild already comes with a set of essential msys binaries (wget, tar, xz and their dependencies) which are used to download and unpack MSYS2 packages
2) Builds the rest of the MSYS2 packages that will be needed
3) Downloads and unpacks MinGW packages that were cross-compiled from Debian (binutils, mingw-w64, gcc and its dependencies).
   Same as with MSYS2, the scripts for cross-compilation are in a separate branch.
4) Builds MinGW packages

Troubleshooting:
* Run buildall.py from a command prompt that runs under the Administrator account.
  Always.
  There's no other way. If you don't run as Administrator, mklink will fail, even if you give yourself appropriate privilege. This is due to the way UAC works.
* Issue this command before running buildall.py:
  set NUMBER_OF_PROCESSORS=1
  That would disable parallel building (makes the process of building stuff slower, but more stable).
  Symptoms of parallel building going wrong: `make' reports absence of rules to make missing files
* Check if you have anything on the BLODA [1]. If so - remove it.
  Symptoms of BLODA interference: `make' crashes (produces a dump), fails to fork
* Check if you have BrowserProtect service (whatever that is). If so - disable or remove it.
  Symptoms of BrowserProtect interference: OS slowly runs out of physical memory, memory is only freed by rebooting

Tips:
* Specify different name for log file ("build.log" in the example above) every time you re-run buildall.
  Logs are really useful for troubleshooting.
* mingwinst.py will unpack archives into your temporary directory. Make sure you have some space there (a few hundreds of megabytes).
  If your temporary directory is on a SSD, lots of writes may reduce its lifetime. Consider setting the TEMP environment variable
  before running buildall.py to point somewhere else (use ImDisk to create a RAMDrive, if you have lots of RAM)
* sbuild uses symbolic links extensively. These require NT 6.x (Vista and newer).
  Be careful when deleting symbolic links - some tools might delete linked files/directories instead of the links themselves.
* If you think that sbuild takes too much of your CPU, try to set NUMBER_OF_PROCESSORS (see above) to a value lower than the number of cores you have.
* Some programs and configure tests use network in a way that triggers firewalls. Be prepared to deal with that in some way, such as:
  A) supervise the build process and allow applications to use network
  B) allow anything in sbuild directory hierarchy to use newtork without restrictions
  C) disable the firewall
  Running as Administrator does not make you exempt from firewall filters on your machine, at least as far as built-in Windows firewall is concerned.

Sbuild is licensed under GNU GPLv3 or later (see COPYING in the root of the source tree).
Packages are licensed under their appropriate licenses.
Normally attribution of the patches used by sbuild is not tracked. If your patch is used in sbuild, and you want it to be attributed to you, contact the developer.

Package types:
-bin      - command line (and GUI) applications, some scripts. May include configuration and data files.
-dll      - shared libraries. Also plugins, if the package provides them.
-dev      - development files (headers, import libraries, static libraries, m4 macros and such)
-doc      - documentation
-lang     - language files
-bld      - sbuild files for building the package (buildscript, release notes, patches, sha512 sum of the tarball etc).
            Does not contain a source tarball (except when source tarball can't be downloaded anywhere).
            mingwinst will not install it.
            Not available for some msys packages
-src      - same as -bld, but also contains the source tarball.
            mingwinst will not install it
            Not available for some msys packages
-dbg      - debug symbols for similarly-named -bin and -dll packages.
-patches  - patches against upstream that were used to build the package.
            Provided for some msys packages instead of -bld package types.
            The script that drives the build process (and makes use of the patches) is a single piece and is available at gitorious in a separate branch.
            There are no -src package types for such packages.

-bin or -dll may also include pre- and post-installation scripts. mingwinst will run these scripts on installation/removal.
Scripts are run by Bash and require shebangs.

[1] http://cygwin.com/faq/faq.html#faq.using.bloda
