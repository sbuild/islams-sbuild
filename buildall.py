#!/mingw/bin/python
from __future__ import print_function
import sys
import os
import hashlib
import zipfile
import subprocess
import shutil
import argparse
import datetime
import re
import tempfile
import urlparse

is2 = sys.version_info.major == 2

if is2:
  from urllib import urlretrieve
else:
  from urllib.request import urlretrieve

if is2:
  from _winreg import QueryValue, CreateKey, HKEY_LOCAL_MACHINE
else:
  from winreg import QueryValue, CreateKey, HKEY_LOCAL_MACHINE

os.environ['LANG'] = 'en_US'
os.environ['MSYS'] = "winsymlinks:nativestrict"
os.environ['SHELLOPTS'] = 'igncr'

clean_tarballs=False

msys_base_archives = """
base-cygwin/3.3-1-2/base-cygwin-3.3-1-2-msys2_i686-bin.tar.xz
base-files/4.2-2-1/base-files-4.2-2-1-msys2_i686-bin.tar.xz
bash/4.3-1/bash-4.3-1-msys2_i686-bin.tar.xz
bash/4.3-1/bash-4.3-1-msys2_i686-lang.tar.xz
binutils/2.24-1/binutils-2.24-1-msys2_i686-bin.tar.xz
binutils/2.24-1/binutils-2.24-1-msys2_i686-dev.tar.xz
binutils/2.24-1/binutils-2.24-1-msys2_i686-lang.tar.xz
bison/3.0.2-1/bison-3.0.2-1-msys2_i686-bin.tar.xz
bison/3.0.2-1/bison-3.0.2-1-msys2_i686-dev.tar.xz
bzip2/1.0.6-2/bzip2-1.0.6-2-msys2_i686-bin.tar.xz
bzip2/1.0.6-2/libbz2-1.0-1.0.6-2-msys2_i686-dll.tar.xz
bzip2/1.0.6-2/libbz2-1.0.6-2-msys2_i686-dev.tar.xz
ca-certificates/1.97-1/ca-certificates-1.97-1-msys2_i686-bin.tar.xz
cloog/0.18.1-1/cloog-0.18.1-1-msys2_i686-bin.tar.xz
cloog/0.18.1-1/cloog-0.18.1-1-msys2_i686-dev.tar.xz
cloog/0.18.1-1/libcloog-isl-0.18.1-1-msys2_i686-dev.tar.xz
cloog/0.18.1-1/libisl-0.18.1-1-msys2_i686-dev.tar.xz
cloog/0.18.1-1/libcloog-isl4-0.18.1-1-msys2_i686-dll.tar.xz
cloog/0.18.1-1/libisl10-0.18.1-1-msys2_i686-dll.tar.xz
coreutils/8.22-1/coreutils-8.22-1-msys2_i686-bin.tar.xz
crypt/20131121-1/crypt-20131121-1-msys2_i686-bin.tar.xz
crypt/20131121-1/libcrypt-20131121-1-msys2_i686-dev.tar.xz
crypt/20131121-1/libcrypt0-20131121-1-msys2_i686-dll.tar.xz
diffutils/3.3-2/diffutils-3.3-2-msys2_i686-bin.tar.xz
expat/2.1.0-2/libexpat-2.1.0-2-msys2_i686-dev.tar.xz
expat/2.1.0-2/libexpat1-2.1.0-2-msys2_i686-dll.tar.xz
expat/2.1.0-2/xmlwf-2.1.0-2-msys2_i686-bin.tar.xz
file/5.18-1/file-5.18-1-msys2_i686-bin.tar.xz
file/5.18-1/libmagic-5.18-1-msys2_i686-dev.tar.xz
file/5.18-1/libmagic1-5.18-1-msys2_i686-dll.tar.xz
findutils/4.5.12-2/findutils-4.5.12-2-msys2_i686-bin.tar.xz
flex/2.5.39-1/flex-2.5.39-1-msys2_i686-bin.tar.xz
flex/2.5.39-1/flex-2.5.39-1-msys2_i686-dev.tar.xz
gawk/4.1.1-1/gawk-4.1.1-1-msys2_i686-bin.tar.xz
gawk/4.1.1-1/gawk-4.1.1-1-msys2_i686-dev.tar.xz
gcc/4.8.2-1/cpp-4.8.2-1-msys2_i686-bin.tar.xz
gcc/4.8.2-1/cpp-4.8.2-1-msys2_i686-dev.tar.xz
gcc/4.8.2-1/fixincludes-4.8.2-1-msys2_i686-bin.tar.xz
gcc/4.8.2-1/g++-4.8.2-1-msys2_i686-bin.tar.xz
gcc/4.8.2-1/gcc-4.8.2-1-msys2_i686-bin.tar.xz
gcc/4.8.2-1/gcc-4.8.2-1-msys2_i686-lang.tar.xz
gcc/4.8.2-1/gfortran-4.8.2-1-msys2_i686-bin.tar.xz
gcc/4.8.2-1/libatomic1-4.8.2-1-msys2_i686-dll.tar.xz
gcc/4.8.2-1/libgcc-4.8.2-1-msys2_i686-dev.tar.xz
gcc/4.8.2-1/libgcc1-4.8.2-1-msys2_i686-dll.tar.xz
gcc/4.8.2-1/libgfortran-4.8.2-1-msys2_i686-dev.tar.xz
gcc/4.8.2-1/libgfortran3-4.8.2-1-msys2_i686-dll.tar.xz
gcc/4.8.2-1/libgomp1-4.8.2-1-msys2_i686-dll.tar.xz
gcc/4.8.2-1/libobjc-4.8.2-1-msys2_i686-dev.tar.xz
gcc/4.8.2-1/libquadmath0-4.8.2-1-msys2_i686-dll.tar.xz
gcc/4.8.2-1/libssp0-4.8.2-1-msys2_i686-dll.tar.xz
gcc/4.8.2-1/libstdc++-4.8.2-1-msys2_i686-dev.tar.xz
gcc/4.8.2-1/libstdc++6-4.8.2-1-msys2_i686-dll.tar.xz
gettext/0.18.3.2-1/gettext-0.18.3.2-1-msys2_i686-bin.tar.xz
gettext/0.18.3.2-1/gettext-0.18.3.2-1-msys2_i686-dev.tar.xz
gettext/0.18.3.2-1/gettext-base-0.18.3.2-1-msys2_i686-bin.tar.xz
gettext/0.18.3.2-1/libasprintf-0.18.3.2-1-msys2_i686-dev.tar.xz
gettext/0.18.3.2-1/libasprintf0-0.18.3.2-1-msys2_i686-bin.tar.xz
gettext/0.18.3.2-1/libgettextpo-0.18.3.2-1-msys2_i686-dev.tar.xz
gettext/0.18.3.2-1/libgettextpo0-0.18.3.2-1-msys2_i686-bin.tar.xz
gettext/0.18.3.2-1/libintl-0.18.3.2-1-msys2_i686-dev.tar.xz
gettext/0.18.3.2-1/libintl8-0.18.3.2-1-msys2_i686-dll.tar.xz
gettext/0.18.3.2-1/gettext-base-0.18.3.2-1-msys2_i686-lang.tar.xz
gettext/0.18.3.2-1/gettext-0.18.3.2-1-msys2_i686-lang.tar.xz
gmp/6.0.0-1/libgmp-6.0.0-1-msys2_i686-dev.tar.xz
gmp/6.0.0-1/libgmp10-6.0.0-1-msys2_i686-dll.tar.xz
gmp/6.0.0-1/libgmpxx-6.0.0-1-msys2_i686-dev.tar.xz
gmp/6.0.0-1/libgmpxx4-6.0.0-1-msys2_i686-dll.tar.xz
grep/2.18-1/grep-2.18-1-msys2_i686-bin.tar.xz
groff/1.22.2-2/groff-1.22.2-2-msys2_i686-bin.tar.xz
groff/1.22.2-2/groff-base-1.22.2-2-msys2_i686-bin.tar.xz
gzip/1.6-3/gzip-1.6-3-msys2_i686-bin.tar.xz
less/458-1/less-458-1-msys2_i686-bin.tar.xz
libiconv/1.14-2/iconv-1.14-2-msys2_i686-bin.tar.xz
libiconv/1.14-2/libcharset-1.14-2-msys2_i686-dev.tar.xz
libiconv/1.14-2/libcharset1-1.14-2-msys2_i686-dll.tar.xz
libiconv/1.14-2/libiconv-1.14-2-msys2_i686-dev.tar.xz
libiconv/1.14-2/libiconv2-1.14-2-msys2_i686-dll.tar.xz
libtool/2.4.2-2/libltdl-2.4.2-2-msys2_i686-dev.tar.xz
libtool/2.4.2-2/libltdl7-2.4.2-2-msys2_i686-dll.tar.xz
libtool/2.4.2-2/libtool-2.4.2-2-msys2_i686-bin.tar.xz
m4/1.4.17-1/m4-1.4.17-1-msys2_i686-bin.tar.xz
make/git-85047eb9044d4b72d50e6620c505c675d55ab98b-1/make-git-85047eb9044d4b72d50e6620c505c675d55ab98b-1-msys2_i686-bin.tar.xz
make/git-85047eb9044d4b72d50e6620c505c675d55ab98b-1/make-git-85047eb9044d4b72d50e6620c505c675d55ab98b-1-msys2_i686-dev.tar.xz
make/git-85047eb9044d4b72d50e6620c505c675d55ab98b-1/make-git-85047eb9044d4b72d50e6620c505c675d55ab98b-1-msys2_i686-lang.tar.xz
mpc/1.0.2-1/libmpc-1.0.2-1-msys2_i686-dev.tar.xz
mpc/1.0.2-1/libmpc3-1.0.2-1-msys2_i686-dll.tar.xz
mpfr/3.1.2-2/libmpfr-3.1.2-2-msys2_i686-dev.tar.xz
mpfr/3.1.2-2/libmpfr4-3.1.2-2-msys2_i686-dll.tar.xz
msys2/git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1/cyglsa-git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1-msys2_i686-dll.tar.xz
msys2/git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1/libmsys-git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1-msys2_i686-dev.tar.xz
msys2/git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1/libmsys2.0-git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1-msys2_i686-dll.tar.xz
msys2/git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1/msys2-runtime-git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1-msys2_i686-bin.tar.xz
ncurses/5.9+20140118-1/libncurses-5.9+20140118-1-msys2_i686-dev.tar.xz
ncurses/5.9+20140118-1/libncurses6-5.9+20140118-1-msys2_i686-dll.tar.xz
ncurses/5.9+20140118-1/libtic6-5.9+20140118-1-msys2_i686-dll.tar.xz
ncurses/5.9+20140118-1/ncurses-5.9+20140118-1-msys2_i686-bin.tar.xz
ncurses/5.9+20140118-1/nurses-base-5.9+20140118-1-msys2_i686-bin.tar.xz
openssl/1.0.1g-1/libssl-1.0.1g-1-msys2_i686-dev.tar.xz
openssl/1.0.1g-1/libssl1.0.0-1.0.1g-1-msys2_i686-dll.tar.xz
openssl/1.0.1g-1/openssl-1.0.1g-1-msys2_i686-bin.tar.xz
patch/2.7.1-3/patch-2.7.1-3-msys2_i686-bin.tar.xz
pcre/8.35-1/libpcre-1-8.35-1-msys2_i686-dll.tar.xz
pcre/8.35-1/libpcre-8.35-1-msys2_i686-dev.tar.xz
pcre/8.35-1/libpcre16-0-8.35-1-msys2_i686-dll.tar.xz
pcre/8.35-1/libpcre32-0-8.35-1-msys2_i686-dll.tar.xz
pcre/8.35-1/libpcrecpp-0-8.35-1-msys2_i686-dll.tar.xz
pcre/8.35-1/libpcrecpp-8.35-1-msys2_i686-dev.tar.xz
pcre/8.35-1/libpcreposix-0-8.35-1-msys2_i686-dll.tar.xz
pcre/8.35-1/libpcreposix-8.35-1-msys2_i686-dev.tar.xz
pcre/8.35-1/pcregrep-8.35-1-msys2_i686-bin.tar.xz
pcre/8.35-1/pcretest-8.35-1-msys2_i686-bin.tar.xz
readline/6.3-1/libreadline-6.3-1-msys2_i686-dev.tar.xz
readline/6.3-1/libreadline6-6.3-1-msys2_i686-dll.tar.xz
rebase/4.4.1-1/mingw-rebase-4.4.1-1-msys2_i686-bin.tar.xz
sed/4.2.2-2/sed-4.2.2-2-msys2_i686-bin.tar.xz
tar/1.27.1-1/tar-1.27.1-1-msys2_i686-bin.tar.xz
w32api/svn-r6478-1/w32api-svn-r6478-1-msys2_i686-dev.tar.xz
wget/1.15-1/wget-1.15-1-msys2_i686-bin.tar.xz
xz/5.0.5-2/liblzma-5.0.5-2-msys2_i686-dev.tar.xz
xz/5.0.5-2/liblzma5-5.0.5-2-msys2_i686-dll.tar.xz
xz/5.0.5-2/xz-5.0.5-2-msys2_i686-bin.tar.xz
xz/5.0.5-2/xz-5.0.5-2-msys2_i686-lang.tar.xz
zlib/1.2.8-2/libz-1.2.8-2-msys2_i686-dev.tar.xz
zlib/1.2.8-2/libz-1.2.8-2-msys2_i686-dll.tar.xz
""".split ()

msys_base_archives_dbg = """
bash/4.3-1/bash-4.3-1-msys2_i686-dbg.tar.xz
binutils/2.24-1/binutils-2.24-1-msys2_i686-dbg.tar.xz
bison/3.0.2-1/bison-3.0.2-1-msys2_i686-dbg.tar.xz
bzip2/1.0.6-2/bzip2-1.0.6-2-msys2_i686-dbg.tar.xz
bzip2/1.0.6-2/libbz2-1.0-1.0.6-2-msys2_i686-dbg.tar.xz
cloog/0.18.1-1/cloog-0.18.1-1-msys2_i686-dbg.tar.xz
cloog/0.18.1-1/libcloog-isl4-0.18.1-1-msys2_i686-dbg.tar.xz
cloog/0.18.1-1/libisl10-0.18.1-1-msys2_i686-dbg.tar.xz
coreutils/8.22-1/coreutils-8.22-1-msys2_i686-dbg.tar.xz
crypt/20131121-1/crypt-20131121-1-msys2_i686-dbg.tar.xz
crypt/20131121-1/libcrypt0-20131121-1-msys2_i686-dbg.tar.xz
diffutils/3.3-2/diffutils-3.3-2-msys2_i686-dbg.tar.xz
expat/2.1.0-2/libexpat1-2.1.0-2-msys2_i686-dbg.tar.xz
expat/2.1.0-2/xmlwf-2.1.0-2-msys2_i686-dbg.tar.xz
file/5.18-1/file-5.18-1-msys2_i686-dbg.tar.xz
file/5.18-1/libmagic1-5.18-1-msys2_i686-dbg.tar.xz
findutils/4.5.12-2/findutils-4.5.12-2-msys2_i686-dbg.tar.xz
flex/2.5.39-1/flex-2.5.39-1-msys2_i686-dbg.tar.xz
gawk/4.1.1-1/gawk-4.1.1-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/cpp-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/fixincludes-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/g++-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/gcc-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/gfortran-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libatomic1-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libgcc1-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libgfortran3-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libgomp1-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libquadmath0-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libssp0-4.8.2-1-msys2_i686-dbg.tar.xz
gcc/4.8.2-1/libstdc++6-4.8.2-1-msys2_i686-dbg.tar.xz
gettext/0.18.3.2-1/gettext-0.18.3.2-1-msys2_i686-dbg.tar.xz
gettext/0.18.3.2-1/gettext-base-0.18.3.2-1-msys2_i686-dbg.tar.xz
gettext/0.18.3.2-1/libasprintf0-0.18.3.2-1-msys2_i686-dbg.tar.xz
gettext/0.18.3.2-1/libgettextpo0-0.18.3.2-1-msys2_i686-dbg.tar.xz
gettext/0.18.3.2-1/libintl8-0.18.3.2-1-msys2_i686-dbg.tar.xz
gmp/6.0.0-1/libgmp10-6.0.0-1-msys2_i686-dbg.tar.xz
gmp/6.0.0-1/libgmpxx4-6.0.0-1-msys2_i686-dbg.tar.xz
grep/2.18-1/grep-2.18-1-msys2_i686-dbg.tar.xz
groff/1.22.2-2/groff-1.22.2-2-msys2_i686-dbg.tar.xz
groff/1.22.2-2/groff-base-1.22.2-2-msys2_i686-dbg.tar.xz
gzip/1.6-3/gzip-1.6-3-msys2_i686-dbg.tar.xz
less/458-1/less-458-1-msys2_i686-dbg.tar.xz
libiconv/1.14-2/iconv-1.14-2-msys2_i686-dbg.tar.xz
libiconv/1.14-2/libcharset1-1.14-2-msys2_i686-dbg.tar.xz
libiconv/1.14-2/libiconv2-1.14-2-msys2_i686-dbg.tar.xz
libtool/2.4.2-2/libltdl7-2.4.2-2-msys2_i686-dbg.tar.xz
m4/1.4.17-1/m4-1.4.17-1-msys2_i686-dbg.tar.xz
make/git-85047eb9044d4b72d50e6620c505c675d55ab98b-1/make-git-85047eb9044d4b72d50e6620c505c675d55ab98b-1-msys2_i686-dbg.tar.xz
mpc/1.0.2-1/libmpc3-1.0.2-1-msys2_i686-dbg.tar.xz
mpfr/3.1.2-2/libmpfr4-3.1.2-2-msys2_i686-dbg.tar.xz
msys2/git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1/libmsys2.0-git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1-msys2_i686-dbg.tar.xz
msys2/git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1/msys2-runtime-git-6e95f9f5eab763ba36c8d5c62a22a4db9cca1d7f-1-msys2_i686-dbg.tar.xz
ncurses/5.9+20140118-1/libncurses6-5.9+20140118-1-msys2_i686-dbg.tar.xz
ncurses/5.9+20140118-1/libtic6-5.9+20140118-1-msys2_i686-dbg.tar.xz
ncurses/5.9+20140118-1/ncurses-5.9+20140118-1-msys2_i686-dbg.tar.xz
openssl/1.0.1g-1/libssl1.0.0-1.0.1g-1-msys2_i686-dbg.tar.xz
openssl/1.0.1g-1/openssl-1.0.1g-1-msys2_i686-dbg.tar.xz
patch/2.7.1-3/patch-2.7.1-3-msys2_i686-dbg.tar.xz
pcre/8.35-1/libpcre-1-8.35-1-msys2_i686-dbg.tar.xz
pcre/8.35-1/libpcre16-0-8.35-1-msys2_i686-dbg.tar.xz
pcre/8.35-1/libpcre32-0-8.35-1-msys2_i686-dbg.tar.xz
pcre/8.35-1/libpcrecpp-0-8.35-1-msys2_i686-dbg.tar.xz
pcre/8.35-1/libpcreposix-0-8.35-1-msys2_i686-dbg.tar.xz
pcre/8.35-1/pcregrep-8.35-1-msys2_i686-dbg.tar.xz
pcre/8.35-1/pcretest-8.35-1-msys2_i686-dbg.tar.xz
readline/6.3-1/libreadline6-6.3-1-msys2_i686-dbg.tar.xz
sed/4.2.2-2/sed-4.2.2-2-msys2_i686-dbg.tar.xz
tar/1.27.1-1/tar-1.27.1-1-msys2_i686-dbg.tar.xz
wget/1.15-1/wget-1.15-1-msys2_i686-dbg.tar.xz
xz/5.0.5-2/liblzma5-5.0.5-2-msys2_i686-dbg.tar.xz
xz/5.0.5-2/xz-5.0.5-2-msys2_i686-dbg.tar.xz
zlib/1.2.8-2/libz-1.2.8-2-msys2_i686-dbg.tar.xz
""".split ()

msys_base_url_template = "http://lrn.no-ip.info/packages/i686-pc-msys/"

msys_packages = [
    ('rebase-4.4.1-1', None, None, None),
    ('dash-0.5.7-2', None, None, None),
    ('unzip-6.0-3', None, None, None),
    ('gdbm-1.10-2', None, None, None),
    ('perl-5.18.0-5', None, None, None),
    ('autoconf-13.1-1', None, None, None),
    ('autoconf2.1-2.13-1', None, None, None),
    ('autoconf2.5-2.64-1', None, None, None),
    ('autoconf2.5-2.67-1', None, None, None),
    ('autoconf2.5-2.69-1', None, None, None),
    ('automake-9.1-1', None, None, None),
    ('automake1.10-1.10.3-1', None, None, None),
    ('automake1.11-1.11.6-1', None, None, None),
    ('automake1.12-1.12.6-1', None, None, None),
    ('automake1.13-1.13.4-1', None, None, None),
    ('automake1.14-1.14-1', None, None, None),
    ('pkg-config-0.28-2', None, None, None),
    ('libffi-3.0.13-2', None, None, None),
    ('which-2.20-2', None, None, None),
    ('help2man-1.44.1-1', None, None, None),
    ('texinfo-5.2-2', None, None, None),
    ('libtasn1-3.4-1', None, None, None),
    ('p11-kit-0.20.2-2', None, None, None),
    ('icu4c-52.1-3', None, None, None),
    ('sqlite3-3.8.2-1', None, None, None),
    ('db4.8-4.8.30-2', None, None, None),
    ('python2.7-2.7.6-2', None, None, None),
    ('libxml2-2.9.1-2', None, None, None),
    ('gnupg-1.4.16-1', None, None, None),
    ('perl_vendor-5.18.0-10', None, None, None),
    ('dos2unix-6.0.4-1', None, None, None),
    ('libidn-1.28-2', None, None, None),
    ('libmetalink-0.1.2-2', None, None, None),
    ('tcp-wrappers-7.6-2', None, None, None),
    ('libedit-20130712-2', None, None, None),
    ('openssh-6.5p1-1', None, None, None),
    ('apr-1-1.5.0-1', None, None, None),
    ('apr-util-1-1.5.3-1', {'apr_src': '/src/msys/apr-1-1.5.0-1/apr-1.5.0'}, None, None),
    ('swig-2.0.11-2', None, None, None),
    ('getopt-1.1.5-2', None, None, None),
    ('xmlto-0.0.25-1', None, None, None),
    ('xorg-util-macros-1.18.0-1', None, None, None),
    ('xproto-7.0.25-1', None, None, None),
    ('lndir-1.0.3-2', None, None, None),
    ('scons-2.3.0-1', None, None, None),
    ('serf-1-1.3.3-1', None, None, None),
    ('subversion-1.8.5-1', None, None, None),
    ('libxslt-1.1.28-3', None, None, None),
    ('asciidoc-8.6.9-1', None, None, None),
    ('libssh2-1.4.3-2', None, None, None),
    ('curl-7.35.0-1', None, None, None),
    ('docbook-5.0-1', None, None, None),
    ('docbook-xml4.1-4.1.2-2', None, None, None),
    ('docbook-xml4.2-4.2-2', None, None, None),
    ('docbook-xml4.3-4.3-2', None, None, None),
    ('docbook-xml4.4-4.4-2', None, None, None),
    ('docbook-xml4.5-4.5-2', None, None, None),
    ('docbook-xsl-1.76.1-1', None, None, None),
    ('git-1.8.5.3-1', None, None, None),
    ('bash-completion-2.1-1', None, None, None),
    ('libunistring-0.9.3-2', None, None, None),
    ('libatomic_ops-7.4.2-1', None, None, None),
    ('bdw-gc-7.4.0-2', None, None, None),
    ('guile-2.0.9-2', None, None, None),
    ("intltool-0.50.2-1", None, None, None),
    ("gnome-doc-utils-0.20.10-2", None, None, None),
    ('gperf-3.0.4-2', None, None, None),
    ('vim-7.4-2', None, None, None),
]

mingw_base_archives_dbg = """
winpthreads/git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1/libwinpthread1-git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1-mingw_i686-dbg.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/binutils-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dbg.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/libbfd-2-24-51-20140801-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dbg.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/libopcodes-2-24-51-20140801-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/cpp-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/fixincludes-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/g++-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/gcc-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/gfortran-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/gnat-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/gobjc++-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/gobjc-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libatomic1-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libasan1-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libgcc1-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libgfortran3-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libgnat4.9-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libgomp1-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libitm1-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libobjc4-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libquadmath0-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libssp0-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libstdc++6-4.9.1-1-mingw_i686-dbg.tar.xz
gcc/4.9.1-1/libubsan0-4.9.1-1-mingw_i686-dbg.tar.xz
cloog/0.18.1-4/cloog-0.18.1-4-mingw_i686-dbg.tar.xz
cloog/0.18.1-4/libcloog-isl4-0.18.1-4-mingw_i686-dbg.tar.xz
cloog/0.18.1-4/libisl10-0.18.1-4-mingw_i686-dbg.tar.xz
gmp/6.0.0a-2/libgmp10-6.0.0a-2-mingw_i686-dbg.tar.xz
gmp/6.0.0a-2/libgmpxx4-6.0.0a-2-mingw_i686-dbg.tar.xz
libiconv/1.14-5/essential-libcharset1-1.14-5-mingw_i686-dbg.tar.xz
libiconv/1.14-5/essential-libiconv2-1.14-5-mingw_i686-dbg.tar.xz
mpc/1.0.2-3/libmpc3-1.0.2-3-mingw_i686-dbg.tar.xz
mpfr/3.1.2-8/libmpfr4-3.1.2-8-mingw_i686-dbg.tar.xz
""".split ()

mingw_base_archives = """
mingw-w64-headers/git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1/mingw-w64-headers-git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1-mingw_i686-dev.tar.xz
mingw-w64-crt/git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1/mingw-w64-crt-git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1-mingw_i686-dev.tar.xz
winpthreads/git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1/libwinpthread-git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1-mingw_i686-dev.tar.xz
winpthreads/git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1/libwinpthread1-git-09564a0bef4cf9da72f2d4c4eb778909a696cc54-1-mingw_i686-dll.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/binutils-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-bin.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/binutils-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-lang.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/libbfd-2-24-51-20140801-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dll.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/libbfd-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dev.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/libopcodes-2-24-51-20140801-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dll.tar.xz
binutils/git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1/libopcodes-git-90debf20f4cc4ddd7cfb6356fe0d3876a18604a6-1-mingw_i686-dev.tar.xz
gcc/4.9.1-1/cpp-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/cpp-4.9.1-1-mingw_i686-dev.tar.xz
gcc/4.9.1-1/fixincludes-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/g++-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/gcc-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/gcc-4.9.1-1-mingw_i686-lang.tar.xz
gcc/4.9.1-1/gfortran-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/gnat-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/gobjc++-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/gobjc-4.9.1-1-mingw_i686-bin.tar.xz
gcc/4.9.1-1/libatomic1-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libasan1-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libgcc-4.9.1-1-mingw_i686-dev.tar.xz
gcc/4.9.1-1/libgcc1-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libgfortran-4.9.1-1-mingw_i686-dev.tar.xz
gcc/4.9.1-1/libgfortran3-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libgnat4.9-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libgomp1-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libitm1-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libobjc-4.9.1-1-mingw_i686-dev.tar.xz
gcc/4.9.1-1/libobjc4-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libquadmath0-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libssp0-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libstdc++-4.9.1-1-mingw_i686-dev.tar.xz
gcc/4.9.1-1/libstdc++6-4.9.1-1-mingw_i686-dll.tar.xz
gcc/4.9.1-1/libubsan0-4.9.1-1-mingw_i686-dll.tar.xz
cloog/0.18.1-4/cloog-0.18.1-4-mingw_i686-bin.tar.xz
cloog/0.18.1-4/cloog-0.18.1-4-mingw_i686-dev.tar.xz
cloog/0.18.1-4/libcloog-isl-0.18.1-4-mingw_i686-dev.tar.xz
cloog/0.18.1-4/libcloog-isl4-0.18.1-4-mingw_i686-dll.tar.xz
cloog/0.18.1-4/libisl-0.18.1-4-mingw_i686-dev.tar.xz
cloog/0.18.1-4/libisl10-0.18.1-4-mingw_i686-dll.tar.xz
gmp/6.0.0a-2/libgmp-6.0.0a-2-mingw_i686-dev.tar.xz
gmp/6.0.0a-2/libgmp10-6.0.0a-2-mingw_i686-dll.tar.xz
gmp/6.0.0a-2/libgmpxx-6.0.0a-2-mingw_i686-dev.tar.xz
gmp/6.0.0a-2/libgmpxx4-6.0.0a-2-mingw_i686-dll.tar.xz
libiconv/1.14-5/essential-libcharset-1.14-5-mingw_i686-dev.tar.xz
libiconv/1.14-5/essential-libcharset1-1.14-5-mingw_i686-dll.tar.xz
libiconv/1.14-5/essential-libiconv-1.14-5-mingw_i686-dev.tar.xz
libiconv/1.14-5/essential-libiconv2-1.14-5-mingw_i686-dll.tar.xz
mpc/1.0.2-3/libmpc-1.0.2-3-mingw_i686-dev.tar.xz
mpc/1.0.2-3/libmpc3-1.0.2-3-mingw_i686-dll.tar.xz
mpfr/3.1.2-8/libmpfr-3.1.2-8-mingw_i686-dev.tar.xz
mpfr/3.1.2-8/libmpfr4-3.1.2-8-mingw_i686-dll.tar.xz
""".split ()

mingw_base_url_template = "http://lrn.no-ip.info/packages/i686-w64-mingw/"

mingw_packages = [
    ("autoconf2.5-2.69-2", None, None, None),
    ("autoconf2.5-2.64-3", None, None, None),
    ("autoconf2.5-2.67-2", None, None, None),
    ("autoconf2.1-2.13-1", None, None, None),
    ("autoconf-12.1-2", None, None, None),
    ("automake1.11-1.11.6-5", None, None, None),
    ("automake1.12-1.12.6-3", None, None, None),
    ("automake1.13-1.13.1-2", None, None, None),
    ("automake1.14-1.14-1", None, None, None),
    ("automake-9.1-1", None, None, None),
    ("libtool-2.4.2-4", None, None, None),
    ("pexports-0.46-5", None, None, None),
    ("w32-cpython2.7-lib-1.0-4", None, None, None),
    ("libgnurx-2.5.1-6", None, None, None),
    ("dlfcn-win32-r19-8", None, None, None),
    ("zlib-1.2.8-4", None, None, None),
    ("bzip2-1.0.6-4", None, None, None),
    ("gettext-0.19.2-1", None, None, None),
    ("libiconv-1.14-5", None, None, None),
    ("expat-2.1.0-4", None, None, None),
    ("gdb-7.7.1-1", None, None, None),
    ("pkg-config-0.28-4", None, None, None),
    ("tcl8.6-8.6.1-3", None, None, None),
    ("tk8.6-8.6.1-3", None, None, None),
    ("mercurial-2.9-2", None, None, None),
    ("libunistring-0.9.3-6", None, None, None),
    ("libffi-3.1-1", None, None, None),
    ("libxml2-2.9.1-4", None, None, None),
    ("libgpg-error-1.13-1", None, None, None),
    ("libgcrypt-1.6.1-2", None, None, None),
    ("libksba-1.3.0-1", None, None, None),
    ("libassuan-2.1.1-1", None, None, None),
    ("pyicon-naming-utils-0.8.90-3", None, None, None),
    ("libxslt-1.1.28-7", None, None, None),
    ("intltool-0.50.2-3", None, None, None),
    ("libatomic_ops-7.4.2-1", None, None, None),
    ("bdw-gc-7.4.0-3", None, None, None),
    ("guile-2.0.9-6", None, None, None),
    ("gtk-doc-1.21-1", None, None, None),
    ("nettle-2.7.1-4", None, None, None),
    ("libtasn1-4.0-1", None, None, None),
    ("p11-kit-0.20.3-1", None, None, None),
    ("glib-2.0-2.41.2-2", None, None, None),
    ("dbus-1-1.8.6-1", None, None, None),
    ("libcroco-0.6-0.6.8-2", None, None, None),
    ("gnutls-3.2.16-1", None, None, None),
    ("libidn-1.28-4", None, None, None),
    ("rtmpdump-git-a1900c3e152085406ecb87c1962c55ec9c6e4016-1", None, None, None),
    ("curl-7.37.1-1", None, None, None),
    ("gnurl-7.37.1-1", None, None, None),
    ("ragel-6.8-4", None, None, None),
    ("glpk-4.54-1", None, None, None),
    ("libpng16-1.6.12-1", None, None, None),
    ("freetype2-2.5.3-1", {'build_essential': '1'}, None, "-n essential-libfreetype -n essential-libfreetype6"),
    ("fontconfig-2.11.1-2", None, None, None),
    ("glew-1.10.0-5", None, None, None),
    ("xz-5.0.5-4", None, None, None),
    ("libarchive-3.1.2-4", None, None, None),
    ("pixman-1-0.32.4-2", None, None, None),
    ("cairo-1.12.16-3", None, None, None),
    ("python-markupsafe-0.23-1", None, None, None),
    ("python-mako-1.0.0-1", None, None, None),
    ("gobject-introspection-1.0-1.41.4-1", {'glib_src': '/src/mingw/glib-2.0-2.41.2-2/glib-2.41.2'}, None, None),
    ("gsettings-desktop-schemas-3.0-3.13.2-1", None, None, None),
    ("json-glib-1.0-1.0.2-1", None, None, None),
    ("icu4c-52.1-3", None, None, None),
    ("harfbuzz-0.9.33-1", None, None, None),
    ("freetype2-2.5.3-1", None, None, "-n libfreetype -n libfreetype6 -n freetype2"),
    ("pango-1.0-1.36.5-1", None, None, None),
    ("gnome-common-3.12.0-1", None, None, None),
    ("atk-1.0-2.13.3-1", None, None, None),
    ("yasm-1.2.0-6", None, None, None),
    ("libjpeg-turbo-1.3.1-1", None, None, None),
    ("freeglut-2.8.1-4", None, None, None),
    ("jbigkit-2.1-1", None, None, None),
    ("tiff-4.0.3-5", None, None, None),
    ("lcms2-2.6-1", None, None, None),
    ("cmake-3.0.0-1", None, None, None),
    ("openjpeg-2.1.0-1", None, None, None),
    ("jasper-1.900.1-7", None, None, None),
    ("gdk-pixbuf-2.0-2.31.0-1", None, None, None),
    ("gtk+-2.0-2.24.24-1", None, None, None),
    ("gtk+-3.0-3.13.5-1", None, None, None),
    ("librsvg-2.40.2-2", None, None, None),
    ("gnome-icon-theme-3.12.0-2", None, None, None),
    ("gnome-icon-theme-symbolic-3.12.0-1", None, None, None),
    ("hicolor-icon-theme-0.13-1", None, None, None),
    ("adwaita-icon-theme-3.13.4-1", None, None, None),
    ("glade3.8-3.8.5-1", None, None, None),
    ("glade-3.18.3-1", None, None, None),
    ("gtksourceview-3.13.2-1", None, None, None),
    ("py2cairo-1.10.0-2", None, None, None),
    ("pygobject-3.13.3-1", None, None, None),
    ("libpeas-1.0-1.10.1-1", None, None, None),
    ("libgxps-0.2.2-1", None, None, None),
    ("djvulibre-3.5.25.3-1", None, None, None),
    ("poppler-0.26.3-1", None, None, None),
    ("libsecret-1-0.18-1", None, None, None),
    ("libplibc-svn-r156-1", None, None, None),
    ("aspell-0.60.7-20131207-2", None, None, None),
    ("aspell6-en-7.1-0-1", None, None, None),
    ("hunspell-1.3.3-1", None, None, None),
    ("hunspell-en-us-20070829-1", None, None, None),
    ("hunspell-ru-yo-0.3.3-1", None, None, None),
    ("enchant-1.6.0-4", None, None, None),
    ("iso-codes-3.55-1", None, None, None),
    ("gtkspell3-3.0.6-1", None, None, None),
    ("gedit-3.13.4-1", None, None, None),
    ("evince-3.13.3-1", None, None, None),
    ("libogg-1.3.2-1", None, None, None),
    ("libvorbis-1.3.4-2", None, None, None),
    ("libmicrohttpd-0.9.37-1", None, None, None),
    ("libtheora-1.2.0alpha1-6", None, None, None),
    ("orc-0.4-0.4.21-1", None, None, None),
    ("libvorbisidec-svn-r18910-4", None, None, None),
    ("ruby2.0-2.1.2-1", None, None, None),
    ("libcaca-0.99.beta19-1", None, None, None),
    ("flac-1.3.0-4", None, None, None),
    ("speex-1.2rc1-6", None, None, None),
    ("libshout-2.3.1-5", None, None, None),
    ("glib-networking-2.41.4-2", None, None, None),
    ("sqlite3-3.8.5.0-1", None, None, None),
    ("libsoup-2.4-2.47.4-1", None, None, None),
    ("wavpack-4.70.0-2", None, None, None),
    ("taglib-1.9.1-3", None, None, None),
    ("liba52-svn-r613-6", None, None, None),
    ("opencore-amr-0.1.3-6", None, None, None),
    ("libcddb-1.3.2-8", None, None, None),
    ("libcdio-git-d8e32350a6dd7c1ecca11d705ae4ba9973db1f55-1", None, None, None),
    ("libdvdread-4.9.9-1", None, None, None),
    ("libdvdnav-4.2.1-2", None, None, None),
    ("libid3tag-0.15.1b-8", None, None, None),
    ("libmad-0.15.1b-7", None, None, None),
    ("lame-3.99.5-7", None, None, None),
    ("libsndfile-1.0.25-6", None, None, None),
    ("twolame-0.3.13-6", None, None, None),
    ("libmpeg2-svn-r1206-6", None, None, None),
    ("x264-20140729-2245-stable-1", None, None, None),
    ("fribidi-0.19.6-2", None, None, None),
    ("libass-0.11.2-1", None, None, None),
    ("opus-1.1-2", None, None, None),
    ("libdca-svn-r91-6", None, None, None),
    ("gsm-1.0.13-6", None, None, None),
    ("libmodplug-0.8.8.5-1", None, None, None),
    ("mjpegtools-2.1.0-3", None, None, None),
    ("zbar-0.10-6", None, None, None),
    ("libvpx-1.3.0-2", None, None, None),
    ("libvisual-0.4-0.4.0-7", None, None, None),
    ("schroedinger-1.0-1.0.11-7", None, None, None),
    ("soundtouch-1.8.0-2", None, None, None),
    ("vo-aacenc-0.1.3-4", None, None, None),
    ("vo-amrwbenc-0.1.3-4", None, None, None),
    ("SDL-1.2.15-6", None, None, None),
    ("qrencode-3.4.4-1", None, None, None),
    ("faad2-2.7-4", None, None, None),
    ("mpg123-1.20.1-1", None, None, None),
    ("libmimic-1.0.4-4", None, None, None),
    ("celt0-0.11.1-5", None, None, None),
    ("check-0.9.14-1", None, None, None),
    ("daala-git-9e3345cdaa141951f8b54d47d6dd0cdb1e67216a-1", None, None, None),
    ("giflib-5.1.0-1", None, None, None),
    ("libwebp-0.4.0-2", None, None, None),
    ("fluidsynth-1.1.6-2", None, None, None),
    ("libmms-0.6.4-1", None, None, None),
    ("openal-soft-1.15.1-1", None, None, None),
    ("gstreamer-1.0-1.4.0-1", None, None, None),
    ("gst-plugins-base-1.0-1.4.0-1", None, None, None),
    ("gst-plugins-good-1.0-1.4.0-1", None, None, None),
    ("gst-plugins-ugly-1.0-1.4.0-1", None, None, None),
    ("gst-plugins-bad-1.0-1.4.0-1", None, None, None),
    ("gst-libav-1.0-1.4.0-1", None, None, None),
    ("libilbc-git-b5f9b10b1a0e32d969d3e10e54be21454c526d75-4", None, None, None),
    ("libav-10.2-1", None, None, None),
    ("exiv2-0.24-2", None, None, None),
    ("tidy-cvs-20091223-8", None, None, None),
    ("libgsf-1-1.14.30-1", None, None, None),
    ("libsmf-1.3-5", None, None, None),
    ("file-5.19-1", None, None, None),
    ("bsdiff-4.3-2", None, None, None),
    ("libextractor-svn-r34102-1", None, None, None),
    ("gnunet-svn-r34102-1", None, None, None),
    ("gnunet-gtk-svn-r34102-1", None, None, None),
]

default_actions = [
      'unpack_msys_base',
      'initial_rebase_msys',
      'make_msys_fstab',
      'init_passwd_and_group',
      'chown_sbuild_files',
      'chmod_sbuild_files',
      'link_w32python_to_mingw',
      'run_msys_postinstall',
      'build_msys_packages',
      'unpack_mingw_base',
      'build_mingw_packages',
    ]

class ClearActionsAction (argparse.Action):
  def __call__ (self, parser, namespace, values, option_string=None):
    setattr (namespace, 'actions', [])

class AddActionAction (argparse.Action):
  def __call__ (self, parser, namespace, values, option_string=None):
    namespace.actions = getattr (namespace, 'actions', default_actions)
    namespace.actions.extend (values)

class RemoveActionAction (argparse.Action):
  def __call__ (self, parser, namespace, values, option_string=None):
    namespace.actions = getattr (namespace, 'actions', default_actions)
    namespace.actions.remove (values[0])

def main ():
  parser = argparse.ArgumentParser (description='Top-level sbuild script.')
  parser.add_argument ('--link-syswow64-python', action='store', dest='link_syswow64_python', default="Auto", help='Whether to get Python location from SysWOW64 registry subtree or not. "Auto" for automatic detection (default).')
  parser.add_argument ('--no-action', action=ClearActionsAction, nargs=0, help='clear the action list.')
  parser.add_argument ('--list-actions', action='store_const', const=True, dest='list_actions', default=None, help='Show a list of available actions, then quit.')
  parser.add_argument ('--start-at-action', action='store', dest='start_at_action', default=None, help='Start following the action list at the given action.')
  parser.add_argument ('--add-action',              action=AddActionAction, nargs=1, help="Add an action to the list.")
  parser.add_argument ('--remove-action',           action=RemoveActionAction, nargs=1, help="Remove an action from the list.")
  parser.add_argument ('--mingw-start-from', default=None, action='store', dest='mingw_start_from', help='start building mingw packages from given package.')
  parser.add_argument ('--msys-start-from', default=None, action='store', dest='msys_start_from', help='start building msys packages from given package.')
  args = parser.parse_args ()
  args.actions = getattr (args, 'actions', default_actions)
  if args.list_actions:
    print ("Available actions:")
    for a in default_actions:
      print ("{}".format (a))
    sys.exit (0)

  if args.msys_start_from is not None and args.msys_start_from not in [x[0] for x in msys_packages]:
    print ("Unknown msys package {}".format (args.msys_start_from))
    sys.exit (1)

  if args.mingw_start_from is not None and args.mingw_start_from not in [x[0] for x in mingw_packages]:
    print ("Unknown mingw package {}".format (args.mingw_start_from))
    sys.exit (1)

  if args.start_at_action is not None:
    if args.start_at_action not in default_actions:
      print ("Unknown action {}".format (args.start_at_action))
      sys.exit (1)
    if args.start_at_action not in args.actions:
      print ("Asked to start executing actions from {}, but that action is not in the list".format (args.start_at_action))
      print ("Current action list:")
      for a in args.actions:
        print ("{}".format (a))
      sys.exit (1)
    actions = []
    add = False
    for i in default_actions:
      if i == args.start_at_action:
        add = True
      if add:
        actions.append (i)
    args.actions = actions

  cd = os.getcwd ()

  tmp = os.environ['TEMP']
  if not os.path.exists (tmp):
    tmp = os.environ['TMP']
  if not os.path.exists (tmp):
    tmp = os.path.join (cd, 'msys', 'tmp')

  os.environ['PATH'] = os.pathsep.join ([os.path.join (os.environ['SystemRoot'], 'System32'), os.environ['SystemRoot'], os.path.join (os.environ['SystemRoot'], 'System32', 'Wbem')])

  cdrv = os.path.splitdrive (os.getcwd ())[0] + os.sep
  mingwdir = os.path.join (cdrv, 'mingw')

  msys_bin = os.path.join (cd, 'msys', 'bin')
  sh_exe = os.path.join (msys_bin, 'bash.exe')

# Since i've patched gcc, this turns from an error into a warning
  if os.path.exists (mingwdir + '\\lib') or os.path.exists (mingwdir + '\\include'):
    print ("WARNING: Directory {} exists, some versions of gcc (but not the one that sbuild installs) will not work properly. Consider removing that directory.".format (mingwdir))

  if 'unpack_msys_base' in args.actions:
    for relarch in msys_base_archives:
      basearc = os.path.basename (relarch)
      url = msys_base_url_template + relarch
      wgetandcheck (os.path.join (cd, 'downloads', basearc), url, msys_bin)
      retry = 10
      while True:
        print ("Unpacking {}".format (basearc))
        sys.stdout.flush ()
        untar = subprocess.Popen ([os.path.join (msys_bin, 'tar.exe'), '--use-compress-program=/usr/bin/xz', '-xf', basearc, '-C', '/'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=os.path.join (cd, 'downloads'))
        o, e = untar.communicate ()
        if untar.returncode == 0:
          break
        print ("e is: {}".format (e))
        print ("Checking for symlink failures")
        if ": Cannot create symlink to " in e and retry > 0:
          print ("Symlink creation failure, trying again")
          retry -= 1
        else:
          print ("Failed to unpack {}:\n{}".format (basearc, e))
          sys.exit (untar.returncode)

  if 'make_msys_fstab' in args.actions:
    make_fstab (cd, tmp)

  if 'initial_rebase_msys' in args.actions:
    msysdir = os.path.join (cd, 'msys')
    rebaseall (msysdir, True)
    rebaseall (msysdir)

  if 'chown_sbuild_files' in args.actions:
    gid = subprocess.Popen ([os.path.join (msys_bin, "id.exe"), "-g"], stdout=subprocess.PIPE)
    o, e = gid.communicate ()
    if gid.returncode != 0:
      sys.exit (gid.returncode)
    gid = o.rstrip ('\n')
    uid = subprocess.Popen ([os.path.join (msys_bin, "id.exe"), "-u"], stdout=subprocess.PIPE)
    o, e = uid.communicate ()
    if uid.returncode != 0:
      sys.exit (uid.returncode)
    uid = o.rstrip ('\n')
    for i in [
        "/mingw",
        "/src",
    ]:
      sys.stdout.flush ()
      r = subprocess.call ([os.path.join (msys_bin, 'chown.exe'), "-R", uid + ":" + gid, i])
      if r != 0:
        print ("Failed to chown file {}".format (i))
        sys.exit (r)

  if 'chmod_sbuild_files' in args.actions:
    sys.stdout.flush ()
    for i in [
        "/mingw",
        "/src",
    ]:
      r = subprocess.call ([os.path.join (msys_bin, 'chmod.exe'), "-R", '755', i])
      if r != 0:
        print ("Failed to chmod file {}".format (i))
        sys.exit (r)

  if 'link_w32python_to_mingw' in args.actions:
    relarch = "w32-cpython2.7-integration/1.0-1/w32-cpython2.7-integration-1.0-1-mingw_i686-bin.tar.xz"
    basearc = os.path.basename (relarch)
    url = mingw_base_url_template + relarch
    wgetandcheck (os.path.join (cd, 'downloads', basearc), url, msys_bin)
    retry = 10
    while True:
      print ("Unpacking {}".format (basearc))
      sys.stdout.flush ()
      untar = subprocess.Popen ([os.path.join (msys_bin, 'tar.exe'), '--use-compress-program=/usr/bin/xz', '-xf', basearc, '-C', '/'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=os.path.join (cd, 'downloads'))
      o, e = untar.communicate ()
      if untar.returncode == 0:
        break
      if ": Cannot create symlink to " in e and retry > 0:
        print ("Symlink creation failure, trying again")
        retry -= 1
      else:
        print ("Failed to unpack {}:\n{}".format (basearc, e))
        sys.exit (untar.returncode)

  if 'run_msys_postinstall' in args.actions:
    posts = [
      "/etc/postinstall/000-cygwin-post-install.sh",
      "/etc/postinstall/base-files-profile.sh",
      "/etc/postinstall/base-files-mketc.sh",
      "/etc/postinstall/terminfogen.sh",
      "/mingw/var/lib/mingw-get/scripts/w32-cpython2.7-integration-1.0-1-post-install.sh",
    ]
    for post in posts:
      sys.stdout.flush ()
      r = subprocess.call ([sh_exe, '--login', '-c', "PATH=/usr/bin:$PATH {}".format (post)])
      if r != 0:
        print ("Failed to run postinstall script {}".format (post))
        sys.exit (r)
    
  if 'init_passwd_and_group' in args.actions:
    sys.stdout.flush ()
    r = subprocess.call ([sh_exe, '--login', '-c', "mkpasswd -l >/etc/passwd && mkgroup -l > /etc/group"])
    if r != 0:
      print ("Failed to init passwd and group")
      sys.exit (r)

  if 'build_msys_packages' in args.actions:
    build_packages ('MSYS', 'i686-pc-msys', os.path.join (cd, 'src', 'msys'), os.path.join (cd, 'msys'), os.path.join (cd, 'mingw', 'bin', 'mingwinst.py'), sh_exe, msys_packages, start_from=args.msys_start_from)

  if 'unpack_mingw_base' in args.actions:
    for relarch in mingw_base_archives:
      basearc = os.path.basename (relarch)
      url = mingw_base_url_template + relarch
      wgetandcheck (os.path.join (cd, 'downloads', basearc), url, msys_bin)
      retry = 10
      while True:
        print ("Unpacking {}".format (basearc))
        sys.stdout.flush ()
        untar = subprocess.Popen ([os.path.join (msys_bin, 'tar.exe'), '--use-compress-program=/usr/bin/xz', '-xf', basearc, '-C', '/'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=os.path.join (cd, 'downloads'))
        o, e = untar.communicate ()
        if untar.returncode == 0:
          break
        if ": Cannot create symlink to " in e and retry > 0:
          print ("Symlink creation failure, trying again")
          retry -= 1
        else:
          print ("Failed to unpack {}:\n{}".format (basearc, e))
          sys.exit (untar.returncode)

  if 'build_mingw_packages' in args.actions:
    build_packages ('MINGW32', 'i686-w64-mingw32', os.path.join (cd, 'src', 'mingw'), os.path.join (cd, 'mingw'), os.path.join (cd, 'mingw', 'bin', 'mingwinst.py'), sh_exe, mingw_packages, start_from=args.mingw_start_from)

  print ("Done building everything")
  print ("Run bash_mingw.bat to start a shell for building Windows programs (this is what you usually need)")
  print ("Run bash_msys.bat to start a shell for building MSYS2 programs")
  print ("Read relocation.txt to learn how to move MinGW/MSYS2 around")

    
def progress_report (transferred_blocks, block_size, total_size):
  tr_size = transferred_blocks * block_size
  if total_size != -1:
    perc = tr_size * 100 / total_size
    sys.stdout.write ("\r{:3}% {}/{}".format (int (perc), tr_size, total_size).encode ('utf-8'))
  else:
    sys.stdout.write ("\r{}/??".format (tr_size).encode ('utf-8'))
  sys.stdout.flush ()


def getandcheck (file, uri):
  ok = False
  if os.path.exists (file):
    ok = hashcheck (file, unsure=True)
  if not ok:
    print ("Downloading {} to {}".format (uri, file))
    urlretrieve (uri, file, progress_report)
    print ("")
    hashcheck (file)

def hashcheck (file, expected=None, unsure=False):
  if os.path.exists (file + '.sha512'):
    with open (file + '.sha512', 'rb') as r:
      expected = r.read ()
  elif unsure:
    return False
  print ("Checking {}: ".format (file), end='')
  with open (file, 'rb') as r:
    r.seek (0, 2)
    size = r.tell ()
    r.seek (0, 0)
    sha512 = hashlib.sha512 ()
    c = 0
    while True:
      d = r.read (1024 * 16)
      if not d:
        break
      c += len (d)
      perc = c * 100 / size
      sys.stdout.write ('\rChecking {}: {:3}% {}/{}'.format (file, int (perc), c, size))
      sys.stdout.flush ()
      sha512.update (d)
    print ("")
    hsum = sha512.hexdigest ()
    if expected is None:
      with open (file + '.sha512', 'wb') as w:
        w.write (hsum)
    else:
      if hsum != expected:
        if not unsure:
          print ("""File `{}' hash sum mismatch (calculated / expected):
{} /
{}""".format (hsum, expected))
          sys.exit (2)
        else:
          return False
    return True

def wgetandcheck (file, uri, msys_bin):
  ok = False
  if os.path.exists (file):
    ok = shahashcheck (file, msys_bin, unsure=True)
  if not ok:
    r = subprocess.call ([os.path.join (msys_bin, 'wget.exe'), uri, '-O', file], cwd=os.path.dirname (file))
    if r != 0:
      print ("Failed to download {} from {}".format (file, uri))
      sys.exit (r)
    shahashcheck (file, msys_bin)

def shahashcheck (file, msys_bin, unsure=False):
  if not os.path.exists (file + '.sha512') and unsure:
    return False
  if not os.path.exists (file + '.sha512'):
    with open (file + '.sha512', 'wb') as w:
      r = subprocess.call ([os.path.join (msys_bin, 'sha512sum.exe'), os.path.basename (file)], cwd=os.path.dirname (file), stdout=w)
      if r != 0:
        print ("Checksum calculation for {} failed".format (file))
        sys.exit (r)
  else:
    r = subprocess.call ([os.path.join (msys_bin, 'sha512sum.exe'), '-c', os.path.basename (file) + '.sha512'], cwd=os.path.dirname (file))
    if r != 0:
      if not unsure:
        print ("Checksum check for {} failed".format (file))
        sys.exit (r)
      else:
        return False
  return True

def get_python_dir (link_syswow64_python):
  systemroot = os.environ['SystemRoot']
  if not os.path.exists (systemroot):
    sys.exit (7)
  if link_syswow64_python != 'Never' and os.path.exists (os.path.join (systemroot, 'SysWOW64')):
    key = "SOFTWARE\\Wow6432Node\\Python\\PythonCore\\2.7\\InstallPath"
  else:
    key = "SOFTWARE\\Python\\PythonCore\\2.7\\InstallPath"
  pythondir = QueryValue (HKEY_LOCAL_MACHINE, key)
  return pythondir

def make_fstab (cd, tmp):
  with open (os.path.join (cd, 'msys', 'etc', 'fstab'), 'wb') as fstab:
    cds = cd.replace ('\\', '/')
    tmps = tmp.replace ('\\', '/')
    print (
"""none / cygdrive binary,posix=0,user 0 0
{cd}/src   /src ntfs binary 0 0
{cd}       /sbuild ntfs binary 0 0
{cd}/mingw /mingw ntfs binary 0 0
{tmp}      /tmp ntfs binary 0 0
""".format (cd=cds, tmp=tmps), file=fstab)

def build_packages (MSYSTEM, buildalias, src_dir, rootdir, mingwinst, sh, packages, start_from=None):
  os.environ['MSYSTEM'] = MSYSTEM

  startfrom = start_from
  srcdir = src_dir
  print ("packages = {}".format (packages))
  for i, p in enumerate (packages):
    print ("Considering package {}: {}".format (i, p[0]))
    if startfrom is not None:
      if p[0] != startfrom:
        print ("Skipping {}".format (p[0]))
        continue
      print ("Will build")
      startfrom = None
    print ("Building package {}: {}".format (i, p[0]))
    pdir = os.path.join (srcdir, p[0])
    if not os.path.exists (os.path.join (pdir, 'pkgbuild.sh')):
      sys.stdout.flush ()
      r = subprocess.call ([sh, '--login', '-c', 'cd {}; tar --verbose --xz -m -xf ./{}-*bld.tar.xz'.format (pdir.replace ('\\', '/'), p[0])])
      if r != 0:
        print ("Unpacking {} failed : {}".format (p[0], r))
        sys.exit (r)
    env = dict (os.environ).copy ()
    env['buildalias'] = buildalias
    env['hostalias'] = buildalias
    if p[1] is not None:
      env.update (p[1])
    env['MAKEFLAGS'] = '-j{}'.format (os.environ.get ('NUMBER_OF_PROCESSORS', '1'))
    sys.stdout.flush ()
    t_from = datetime.datetime.now ()
    r = subprocess.call ([sh, '--login', '-c', 'unset PWD; cd {}; ./pkgbuild.sh'.format (pdir.replace ('\\', '/'))], env=env)
    t_to = datetime.datetime.now ()
    if r != 0:
      print ("pkgbuild.sh for {} failed: {}".format (p[0], r))
      sys.exit (r)
    print ("Building package {} took {} seconds".format (p[0], (t_to - t_from).seconds))
    arguments = [sh, '--login', '-c',
        'cd {pwd}; {python} {mingwinst} {args}'.format (
            pwd=pdir.replace ('\\', '/'),
            python=sys.executable.replace ('\\', '/'),
            mingwinst=mingwinst.replace ('\\', '/'),
            args=p[3] if p[3] else '')]
    sys.stdout.flush ()
    r = subprocess.call (arguments)
    if r != 0:
      print ("mingwinst.py for {} failed: {}".format (p[0], r))
      sys.exit (r)
    print ("Done installing {}".format (p[0]))
    if 'MSYS' in MSYSTEM:
      rebaseall (rootdir)
    print ("Done deploying {}".format (p[0]))
    if clean_tarballs:
      r = subprocess.call ([sh, '--login', '-c', 'cd {pwd}; rm *-msys2_*.tar.xz'.format (pwd=pdir.replace ('\\', '/'))], env=env)
      r = subprocess.call ([sh, '--login', '-c', 'cd {pwd}; rm *-mingw_*.tar.xz'.format (pwd=pdir.replace ('\\', '/'))], env=env)
      print ("Done cleaning tarballs for {}".format (p[0]))

def rebaseall (msysroot, msysonly=False):
  DefaultBaseAddress = "0x70000000"
  db = msysroot + '\\etc\\rebase.db.i386'
  have_db = os.path.isfile (db)
  mingw_rebase = os.path.join (msysroot, 'bin', 'mingw-rebase.exe')
  nt = tempfile.NamedTemporaryFile (delete=False)
  for root, dirs, files in os.walk (msysroot):
    for f in files:
      if not (f.endswith ('.dll') or f.endswith ('.so') or f.endswith ('.oct')):
        continue
      msys = re.match (r'msys-[0-9.]+\.dll', f)
      if msysonly and not msys:
        continue
      if not msysonly:
        msys = False
      cyglsa = re.match (r'cyglsa.*\.dll', f)
      ash = False
      rebase = re.match (r'mingw-rebase\.exe', f)
      peflags = re.match (r'mingw-peflags\.exe', f)
      if msys or cyglsa or ash or rebase or peflags:
        continue
      if nt_is_link (os.path.join (root, f)):
        continue
      nt.write (os.path.join (root, f).encode (sys.getfilesystemencoding()))
      nt.write ('\n')
  n = nt.name
  nt.close ()
  args = [mingw_rebase.replace ('\\','/'), '-v', '-n', '-s', '-T', n.replace ('\\', '/')]
  if not have_db:
    args += ['-b', DefaultBaseAddress]
  if not msysonly:
    args += ['-m']
  print ("running rebase: {}".format (args))
  sys.stdout.flush ()
  r = subprocess.call (args)
  if r != 0:
    print ("rebase for file {} failed: {}".format (os.path.join (root, f), r))
    sys.exit (r)
  os.remove (n)

def nt_is_link (filename):
  bs_filename = filename.replace ('/', '\\')
  dirname = os.path.dirname (bs_filename)
  p = subprocess.Popen ([os.environ['ComSpec'], '/C', 'dir', dirname], stdout=subprocess.PIPE)
  o, e = p.communicate ()
  if p.returncode != 0:
    return True
  bn = os.path.basename (filename)
  if '<SYMLINK>      ' + bn + ' [' in o and ' ' + bn + '\n' not in o:
    return True
  return False
  
if __name__ == "__main__":
  main ()
