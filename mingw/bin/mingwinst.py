#!/mingw/bin/python
# -*- coding: utf-8 -*-
#    mingw-install-manually - manually unpacks mingw.org-compliant packages
#    Copyright © 2012  LRN
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import print_function

import os
import re
import sys
import subprocess
import tempfile
import shutil
import argparse
import types

class StdOut(object):
    def write(self, string):
        if isinstance(string, unicode):
            string = string.encode('utf-8')
        try:
            sys.__stdout__.write(string)
        except:
            pass
    def flush(self):
        sys.__stdout__.flush ()


sys.stdout = StdOut()

def run_and_communicate (args, **kwargs):
  # In 2.7.x subprocess doesn't work well with unicode,
  # work around that by writing a UTF-8-encoded shell
  # script and executing THAT instead

  cmdline = args
  if not isinstance(cmdline, types.StringTypes):
    cmdline = subprocess.list2cmdline(cmdline)
  getcp = subprocess.Popen (['chcp'], shell=True, stdout=subprocess.PIPE)
  o, e = getcp.communicate ()
  o = o.rstrip ('\r\n')
  cp = re.sub (r'^.*: (.+)', r'\1', o, re.DOTALL)
  script = ("@echo off\r\nchcp 65001\r\n" + cmdline + "\r\nexit %ERRORLEVEL%\r\n").encode ('utf-8')
  nt = tempfile.NamedTemporaryFile (mode='w+b', suffix='.cmd', delete=False)
  if 'shell' not in kwargs:
    kwargs['shell'] = True
  nt.write (script)
  print ("Closing {}".format (nt.name))
  n = nt.name
  nt.close ()
  print ("Closed")
  print ("Running {} with kwargs {}".format (nt.name, kwargs))
  p = subprocess.Popen ([nt.name], **kwargs)
  o, e = p.communicate ()
  os.remove (n)
  o = o.decode ('UTF-8')
  e = e.decode ('UTF-8')
  subprocess.call (['chcp', cp], shell=True)
  return o, e, p

mounts = {}
sysroot_override = None
verbose = False

def debug (*args, **kwargs):
  if verbose:
    if 'file' not in kwargs:
      kwargs['file'] = sys.stderr
    print (*args, **kwargs)

def parse_filename (f):
  state = 0
  buffer = []
  s = []
  debug ("Parsing filename {}".format (f))
  if len (f) < 9:
    debug ("Too short ({} < 9), returning [{}]".format (len (f), f))
    return [f]
  
  extra_ver = None

  mingw = f.find ("-mingw_")
  msys2 = f.find ("-msys2_")
  debug ("Position of -mingw_ = {}, position of -msys2_ = {}".format (mingw, msys2))
  if mingw < 0 and msys2 < 0:
    debug ("No distr id found, returning [{}]".format (f))
    return [f]
  subsys_ver = None
  if mingw > 0:
    pre_sys = f[:mingw]
    debug ("pre_sys = {}".format (pre_sys))
    subsys = "mingw"
    post_sys = f[mingw + len ("-mingw_"):]
    debug ("post_sys = {}".format (post_sys))
    dash = post_sys.find ('-')
    debug ("Dash position in post_sys: {}".format (dash))
    if dash == -1:
      debug ("Failed to find a dash, returning [{}]".format (f))
      return [f]
    arch = post_sys[:dash]
    debug ("arch = {}".format (arch))
    post_sys = post_sys[dash + 1:]
    debug ("arch-less post_sys = {}".format (post_sys))
    if post_sys[:7] == 'python-':
      tar = post_sys.find ('.tar')
      debug ("tar position = {}".format (tar))
      extra_ver = post_sys[7:tar]
      debug ("extra_ver = {}".format (extra_ver))
      post_sys = post_sys[:6] + post_sys[tar:]
      debug ("post_sys without extra_ver = {}".format (post_sys))
  elif msys2 > 0:
    pre_sys = f[:msys2]
    debug ("pre_sys = {}".format (pre_sys))
    subsys = "msys2"
    post_sys = f[msys2 + len ("-msys2_"):]
    debug ("post_sys = {}".format (post_sys))
    dash = post_sys.find ('-')
    debug ("Dash position in post_sys: {}".format (dash))
    if dash == -1:
      debug ("Failed to find a dash, returning [{}]".format (f))
      return [f]
    arch = post_sys[:dash]
    debug ("arch = {}".format (arch))
    post_sys = post_sys[dash + 1:]
    debug ("arch-less post_sys = {}".format (post_sys))

  i = 1
  s = []
  debug ("while -{}th char of pre_sys {} ({}) != '-'".format (i, pre_sys, pre_sys[-i]))
  while pre_sys[-i] != '-':
    s.append (pre_sys[-i])
    debug ("Append {} to {}".format (pre_sys[-i], s))
    i += 1
  rev = ''.join (reversed(s))
  debug ("rev = {}".format (rev))
  pre_sys = pre_sys[0:-i]
  debug ("pre_sys without rev = {}".format (pre_sys))

  i = 1
  s = []
  debug ("while -{}th char of pre_sys {} ({}) != '-'".format (i, pre_sys, pre_sys[-i]))
  while pre_sys[-i] != '-':
    s.append (pre_sys[-i])
    debug ("Append {} to {}".format (pre_sys[-i], s))
    i += 1
  ver = ''.join (reversed(s))
  debug ("ver = {}".format (ver))
  pkgname = pre_sys[0:-i]
  debug ("pkgname = {}".format (pkgname))
  if pkgname[-4:] == '-git':
    pkgname = pkgname[:-4]
    debug ("git-less pkgname = {}".format (pkgname))
    ver = 'git-' + ver
    debug ("git-ver = {}".format (ver))
  elif pkgname[-4:] == '-svn':
    pkgname = pkgname[:-4]
    debug ("svn-less pkgname = {}".format (pkgname))
    ver = 'svn-' + ver
    debug ("svn-ver = {}".format (ver))

  lastdot = post_sys.rfind ('.')
  debug ("last dot position = {}".format (lastdot))
  if '.tar' in post_sys[:lastdot]:
    pre_lastdot = post_sys[:lastdot].rfind ('.')
    debug ("pre-last dot position = {}".format (pre_lastdot))
    pkgext = post_sys[pre_lastdot + 1:]
    debug ("pkgext = {}".format (pkgext))
    pkgtype = post_sys[:pre_lastdot]
    debug ("pkgtype = {}".format (pkgtype))
  else:  
    pkgext = post_sys[lastdot + 1:]
    debug ("pkgext = {}".format (pkgext))
    pkgtype = post_sys[:lastdot]
    debug ("pkgtype = {}".format (pkgtype))
  print ("Parsed file {}; name={}; version={}; revision={}; subsystem={}; subsystem_version={}; type={}; extension={}; extra_version={}".format (f, pkgname, ver, rev, subsys, subsys_ver, pkgtype, pkgext, extra_ver))
  return {'name' : pkgname, 'ver' : ver, 'rev' : rev, 'subsys' : subsys, 'subsys_ver' : subsys_ver, 'type' : pkgtype, 'ext' : pkgext, 'extra_ver' : extra_ver}

def find_packages_in (directory):
  packages = {}
  for r, d, fl in os.walk (directory):
    del d[:]
    for f in fl:
      parsed = parse_filename (f)
      if len (parsed) == 8:
        print ("Found {}".format (f))
        if parsed['name'] not in packages:
          packages[parsed['name']] = {}
        verrev = "{}-{}".format (parsed['ver'], parsed['rev'])
        if verrev not in packages[parsed['name']]:
          packages[parsed['name']][verrev] = []
        packages[parsed['name']][verrev].append ((f, parsed))
  return packages

def get_scripts (subsys):
  if sysroot_override is None or 'msys2' in subsys:
    if 'msys2' in subsys:
      d = '/'
    elif 'mingw' in subsys:
      d = '/mingw'
    else:
      print ("Unknown subsystem: {}".format (subsys))
      sys.exit (1)

    if d not in mounts:
      print ("Can't get scripts: Don't know where {} directory is".format (d))
      sys.exit (1)
    scriptdir = mounts[d] + '/var/lib/mingw-get/scripts/'
  else:
    scriptdir = sysroot_override + '/var/lib/mingw-get/scripts/'
  
  scripts = set ()
  if os.path.isdir (scriptdir):
    for root, dirs, files in os.walk (scriptdir):
      dirs[:] = []
      for f in files:
        scripts.add ((scriptdir + f, f, os.stat (scriptdir + f).st_mtime))
  return scripts

def get_msys_mounts ():
  mounts = {}
  p = subprocess.Popen (['sh', '-c', 'mount'], stdout=subprocess.PIPE)
  tmp, _ = p.communicate ()
  for line in tmp.splitlines ():
    m = re.match (r'(.+) on (.+) type.*', line)
    if m:
      mounts[m.group (2)] = m.group (1).replace ('/', '\\')
      print ("Found MSys mount: {} {}".format (m.group (2), m.group (1).replace ('/', '\\')))
  return mounts

def on_rmtree_error (function, path, excinfo):
  print ("Function {} failed on path {}: {}".format (function, path, excinfo))

def map_dest (name, pkg, mounts):
  dospath = False
  warnings = 0
  while name.startswith ('.\\') or name.startswith ('./') or name == '.':
    name = name[2:]
  if name == '':
    return (True, True, None, None, 0)
  if name == 'mingw' or name.startswith ('mingw/') or name.startswith ('mingw\\'):
    mname = name[len ('mingw/'):]
    s = '/mingw'
    if pkg['type'] == 'python':
      if pkg['extra_ver'] is not None:
        pass #TODO: check Python version
      dst = os.path.join (sys.prefix, 'lib')
      dst = s
    else:
      if sysroot_override is not None and 'msys' not in pkg['subsys']:
        s = sysroot_override
        dst = s
      else:
        dst = mounts[s]
    dstname = os.path.join (dst, mname)
  elif name == 'usr' or name.startswith ('usr/') or name.startswith ('usr\\'):
    mname = name[len ('usr/'):]
    s = '/'
    dst = mounts[s]
    dstname = os.path.join (dst, mname)
  else:
    s = '/'
    dst = mounts[s]
    dstname = os.path.join (dst, name)
#  else:
#    print ("WARNING: Unknown subsystem: {}".format (pkg['subsys']))
#    warnings += 1
  if s not in mounts and not dospath:
    print ("ERROR: Don't know where {} is".format (s))
    errors += 1
    return (False, False, None, None, warnings)
  else:
    dst = dst.replace ('/', '\\')
    dstname = dstname.replace ('/', '\\')
    return (True, False, dst, dstname, warnings)


def main ():
  errors = 0
  warnings = 0
  parser = argparse.ArgumentParser (description='Primitive mingw package manipulator.')
  parser.add_argument ('directory', metavar='DIR', default='.', nargs='?', help="directory to work in (defaults to '.').")
  parser.add_argument ('--ver', '-V', metavar='VER', default=None, help='version to install (defaults to latest).')
  parser.add_argument ('--install', '-i', action='store_const', dest='mode', const='install', help='install packages (default).')
  parser.add_argument ('--remove', '-r', action='store_const', dest='mode', const='remove', help='remove packages.')
  parser.add_argument ('--force-sysroot', '-s', default=None, action='store', dest='sysroot', help='use a non-standard mingw sysroot (by default mingw sysroot is /mingw, and depends on fstab).')
  parser.add_argument ('--force-name', '-n', default=None, action='append', dest='name', help='only operate on package named "name" (by default operates on all packages in directory). Specify more than once to operate on packages with multiple names.')
  parser.add_argument ('--verbose', default=False, action='store_true', dest='verbose', help='enable verbose output.')
  args = parser.parse_args()
  global sysroot_override
  sysroot_override = args.sysroot
  if args.mode == None:
    args.mode = 'install'
  global verbose
  verbose = args.verbose

  global mounts;
  mounts = get_msys_mounts ()

  packages = find_packages_in (args.directory)

  scripts = get_scripts ('mingw')
  scripts.update (get_scripts ('msys2'))
  if args.mode == 'install':
    new_scripts = set ()
  elif args.mode == 'remove':
    remove_scripts = set ()

  for pkgname, pkgcontents in packages.items ():
    if args.name is not None and pkgname not in args.name:
      continue
    print ("Processing {}".format (pkgname))
    versions = list (pkgcontents.items ())
    versions = sorted (versions, key=lambda x: x[0])
    latest = versions[-1]
    if args.ver is not None:
      latest = None
      for v in versions:
        if v[0] == args.ver:
          latest = v
          break
      if latest is None:
        print ("Can't find required version {}. Latest is {}".format (args.ver, versions[-1]))
        sys.exit (1)
    for filename, pkg in latest[1]:
      dospath = False
      if pkg['type'] in ['src', 'bld', 'user']:
        continue
      print ("Processing file {}".format (filename))
      if '/tmp' not in mounts:
        print ("ERROR: Don't know where /tmp is")
        errors += 1
      else:
        tmp = mounts['/tmp']
        td = tempfile.mkdtemp ('_pkg', 'mingwinst_{}_'.format (os.path.basename (filename)), dir=tmp)
        td_base = os.path.basename (td)
        unpacked = True
        print ("Unpacking to {}".format (td))
        if pkg['ext'] == 'tar.xz':
          # Concoct a PATH with /usr/bin at the beginning,
          # avoid conflicts when mingw-xz is installed
          mingwmount = mounts.get ('/mingw', None)
          usrmount = mounts.get ('/usr', None)
          env = dict (os.environ)
          if mingwmount and usrmount:
            env['PATH'] = usrmount + '/bin;' + env['PATH']
          retry = 0
          while retry < 10:
            untar = subprocess.Popen (['tar', '--xz', '-m', '-C', '/tmp/' + td_base, '-xf', filename], stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)
            o, e = untar.communicate ()
            print ("Untar output: {}\nUntar errors: {}".format (o, e))
            if 'Cannot create symlink' not in e:
              break
            retry += 1
          del env
        else:
          print ("WARNING: Don't know how to unpack {} packages".format (pkg['ext']))
          warnings += 1
          unpacked = False
        if unpacked:
          if args.mode == 'install':
            for root, dirs, files in os.walk (unicode (td)):
              rps = os.path.relpath (root, td)
              rpd = os.path.relpath (root, td)
              mapped, skip_level, dst, rpd, wn = map_dest (rpd, pkg, mounts)
              warnings += wn
              if not mapped:
                print ("WARNING: failed to map relative destination path {}".format (os.path.relpath (root, td)))
                continue
              rd = []
              print ("Copying from {}".format (rps))
              for d in dirs:
                if skip_level:
                  continue
                full_d = os.path.join (dst, rpd, d)
                print ("Directory {}".format (os.path.join (rps, d)))
                if os.path.exists (full_d) and not os.path.isdir (full_d):
                  print ("WARNING: Can't copy directory {} to {}, because destination exists and is a file".format (os.path.join (root, d), full_d))
                  warnings += 1
                  rd.append (d)
                elif not os.path.exists (full_d) and d != 'usr':
                  print ("Creating new directory {}".format (full_d))
                  # Create a dummy directory to prevent xcopy confusion
                  os.makedirs (full_d)
                  print (u"Copying {} to {}".format (os.path.join (root, d), full_d))
                  o, e, p = run_and_communicate (['xcopy', '/H', '/R', '/K', '/X', '/Y', '/E', '/B', os.path.join (root, d), full_d], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                  rd.append (d)
              for d in rd:
                dirs.remove (d)
              for f in files:
                full_f = os.path.join (dst, rpd, f)
                print (u"File {}".format (os.path.join (rps, f)))
                if not os.path.exists (full_f):
                  print (u"Creating a dummy file {}".format (full_f))
                  # Create a dummy file to prevent xcopy confusion
                  with open (full_f, 'wb') as t:
                    t.write ('x')
                if not os.path.isfile (full_f):
                  print ("WARNING: Can't copy file {} to {}, because destination exists and is a directory".format (os.path.join (root, f), full_f))
                  warnings += 1
                print (u"Copying {} to {}".format (os.path.join (rps, f), full_f))
                o, e, p = run_and_communicate (['xcopy', '/V', '/C', '/F', '/H', '/R', '/K', '/X', '/Y', '/B', os.path.join (root, f), full_f], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                print (u"Copying results: returncode: {}\nstdout:\n{}\nstderr:\n{}".format (p.returncode, o, e))
                if 'Sharing violation' in e:
                  print ("Failed, trying to recover");
                  i = 0
                  failed = True
                  while failed:
                    alt = full_f + '.mingwinstback.' + str (i)
                    try:
                      os.rename (full_f, alt)
                    except OSError as e:
                      i += 1
                      if i > 100:
                        failed = False
                        print ("Ran out of rename indices")
                    except:
                      print ("Can't rename to {}, and it's fatal".format (alt))
                      failed = False
                    else:
                      failed = False
                      print ("Creating (again?) a dummy file {}".format (full_f))
                      # Create a dummy file to prevent xcopy confusion
                      with open (full_f, 'wb') as t:
                        t.write ('x')
                      print ("Copying {} to {} (again)".format (os.path.join (root, f), full_f))
                      p = subprocess.Popen (['xcopy', '/V', '/C', '/F', '/H', '/R', '/K', '/X', '/Y', '/E', '/B', os.path.join (root, f), full_f], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                      o, e = p.communicate ()
                      print ("Copying results: returncode: {}\nstdout:\n{}\nstderr:\n{}".format (p.returncode, o, e))
                  if 'Sharing violation' in e:
                    print ("ERROR: Can't move {} out of the way".format (full_f))
                    errors += 1
                elif p.returncode != 0:
                  errors += 1
          elif args.mode == 'remove':
            rs = '{}-{}-{}-post-remove.sh'.format (pkg['name'], pkg['rev'], pkg['ver'])
            for script in scripts:
              if script[1] == rs:
                remove_scripts[rs] = script
                break
            for root, dirs, files in os.walk (unicode (td), topdown=False):
              rps = os.path.relpath (root, td)
              rpd = os.path.relpath (root, td)
              mapped, skip_level, dst, rpd, wn = map_dest (rpd, pkg, mounts)
              print ("Mapped {} to {} and {}, {}".format (rpd, dst, rpd, "skip one level" if skip_level else "no skipping"))
              warnings += wn
              if not mapped:
                print ("WARNING: failed to map relative destination path {}".format (os.path.relpath (root, td)))
                continue
              print (u"Removing from {}".format (rps))
              for d in dirs:
                if skip_level:
                  print (u"Skipping {}".format (d))
                  continue
                full_d = os.path.join (dst, rpd, d)
                if os.path.exists (full_d) and not os.path.isdir (full_d):
                  print ("WARNING: Can't remove directory {} from {}, because it's not a directory".format (os.path.join (root, d), full_d))
                  warnings += 1
                elif not os.path.exists (full_d):
                  print ("Removing the directory")
                  try:
                    os.remove (full_d);
                  except:
                    print ("Failed to remove it. This is usually OK")
              for f in files:
                full_f = os.path.join (dst, rpd, f)
                if not os.path.exists (full_f):
                  print (u"{} + {} + {} does not exist - skipping".format (dst, rpd, f))
                  continue
                if f == rs:
                  print (u"{} is a post-remove script, skipping".format (os.path.join (rpd, f)))
                  continue
                print (u"Removing {}".format (os.path.join (rps, f)))
                try:
                  os.remove (full_f)
                except OSError as e:
                  i = 0
                  failed = True
                  while failed:
                    alt = full_f + '.mingwinstback.' + str (i)
                    try:
                      os.rename (full_f, alt)
                    except OSError as e:
                      i += 1
                      if i > 100:
                        failed = False
                        print ("Ran out of rename indices")
                    except:
                      print ("Can't rename to {}, and it's fatal".format (alt))
                      errors += 1
                      failed = False
                    else:
                      failed = False
          o, e, p = run_and_communicate (['del', '/F', '/S', '/Q', td], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
          print (u"Tree deletion results: returncode: {}\nstdout:\n{}\nstderr:\n{}".format (p.returncode, o, e))
          shutil.rmtree (td, True, on_rmtree_error)
          if args.mode == 'install':
            scripts2 = get_scripts ('mingw')
            scripts2.update (get_scripts ('msys2'))
            installed_scripts = scripts2.difference (scripts)
            if len (installed_scripts) > 0:
              print ("Found {} new scripts".format (len (installed_scripts)))
            new_scripts.update (installed_scripts)
  if args.mode == 'install':
    for script in new_scripts:
      filename, basename, time = script
      if 'post-install.' in os.path.basename (filename):
        print ("Running post-install script {}".format (filename))
        sys.stdout.flush ()
        subprocess.call (['sh', filename])
  if args.mode == 'remove':
    for script in remove_scripts:
      filename, basename, time = script
      print ("Running post-remove script {}".format (filename))
      sys.stdout.flush ()
      subprocess.call (['sh', filename])
      try:
        os.remove (filename)
      except:
        print ("WARNING: Failed to remove {} after running it".format (filename))
        warnings += 1
    
  if warnings > 0 or errors > 0:
    print ("Had {} warnings and {} errors".format (warnings, errors))

if __name__ == '__main__':
  main ()
