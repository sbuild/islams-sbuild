#!/mingw/bin/python
# -*- coding: utf-8 -*-
#    mingw-install-manually - manually unpacks mingw.org-compliant packages
#    Copyright � 2012  LRN
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# Usage: laremove.py directory
# Scans directory (recursively), and removes all .la files that have
# corresponding .dll.a files
from __future__ import print_function
import sys
import os

def removelas (directory):
  for root, dirs, files in os.walk (directory):
    for f in files:
      if f[-3:] == '.la':
        dlla = f[:-3] + '.dll.a'
        if dlla in files:
          print ("remove {}".format (os.path.join (root, f)))
          os.remove (os.path.join (root, f))
        else:
          print ("keep {}".format (os.path.join (root, f)))
          need_to_fix = False
          with open (os.path.join (root, f), 'rb') as l:
            la_contents = l.read ()
          old_find = "\nold_library=\'"
          lib_find = "\nlibrary_names=\'"
          old_library_pos = la_contents.find (old_find)
          if old_library_pos > 0:
            old_library_end = [-1, -1]
            old_library_end[0] = la_contents[old_library_pos + len (old_find):].find ("'\n")
            old_library_end[1] = la_contents[old_library_pos + len (old_find):].find ("'\r\n")
            if old_library_end[1] != -1 and old_library_end[1] < old_library_end[0]:
              old_library_end = old_library_end[1]
            else:
              old_library_end = old_library_end[0]
            if old_library_end != -1:
              old_library = la_contents[old_library_pos + len (old_find) : old_library_pos + len (old_find) + old_library_end]
              library_names_pos = la_contents.find (lib_find)
              if library_names_pos > 0:
                library_names_end = [-1, -1]
                library_names_end[0] = la_contents[library_names_pos + len (lib_find):].find ("'\n")
                library_names_end[1] = la_contents[library_names_pos + len (lib_find):].find ("'\r\n")
                if library_names_end[1] != -1 and library_names_end[1] < library_names_end[0]:
                  library_names_end = library_names_end[1]
                else:
                  library_names_end = library_names_end[0]
                if library_names_end != -1:
                  lib_name = la_contents[library_names_pos + len (lib_find) : library_names_pos + len (lib_find) + library_names_end]
                  if len (old_library) > 0 and len (lib_name) == 0:
                    print ("need to fix {}".format (os.path.join (root, f)))
                    new_contents = la_contents[:library_names_pos + len (lib_find)] + old_library + la_contents[library_names_pos + len (lib_find) + library_names_end:]
                    with open (os.path.join (root, f), 'wb') as l:
                      l.write (new_contents)
          

def main ():
  if len (sys.argv) != 2:
    return
  removelas (sys.argv[1])

if __name__ == '__main__':
  main ()